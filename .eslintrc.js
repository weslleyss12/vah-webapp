module.exports = {
  extends: ['airbnb', 'prettier', 'prettier/react'],
  plugins: ['prettier'],
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 2016
  },
  env: {
    es6: true,
    browser: true,
    node: true
  },
  rules: {
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'no-underscore-dangle': 'off',
    'react/sort-comp': 'off',
    eqeqeq: 'off',
    'react/prop-types': [2, { ignore: ['children'] }],
    'react/no-array-index-key': 'off',
    camelcase: 'off',
    'no-case-declarations': 'off'
  }
};
