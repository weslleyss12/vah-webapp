import React from 'react';
import { connect } from 'react-redux';

const Example = ({ description }) => (
  <div>
    <h1>Example Redux: {description}</h1>
  </div>
);

const mapStateToProps = state => ({
  description: state.test.description
});

export default connect(mapStateToProps)(Example);
