// TODO: Conferir qual versão do APP devemos utilizar
const APP_VERSION = '1.0.0';

const API_BASE_URL = 'https://api.appvah.com/v1';
const AIR_API_BASE_URL = 'https://api.appvah.com/v1';

const GOOGLE_PLACES_API = 'AIzaSyA0BGGGmPwECN8kzAjXOP9w_OclhSqy6E8';

export default {
  APP_VERSION,
  API_BASE_URL,
  AIR_API_BASE_URL,
  GOOGLE_PLACES_API
};
