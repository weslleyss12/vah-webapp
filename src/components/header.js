import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Icon } from "antd";

import Container from "./container";
import logo from "../assets/images/core/logo.png";
import logoBlue from "../assets/images/core/logo-transition.png";

import icoAereo from "../assets/images/icons/ico-vah-aereo.svg";
import icoCarona from "../assets/images/icons/ico-vah-carona.svg";
import icoLocacao from "../assets/images/icons/ico-vah-locacao.svg";
import icoRodoviario from "../assets/images/icons/ico-vah-rodoviario.svg";
import icoUrbano from "../assets/images/icons/ico-vah-urbano.svg";

import icoBR from "../assets/images/br.png";
import icoES from "../assets/images/esp.png";

import appGoogle from "../assets/images/core/google-play.svg";
import appStore from "../assets/images/core/app-store.svg";

import { FormattedMessage } from "react-intl";

class Header extends Component {
  state = {
    visible: false
  };

  openMenu = () => {
    this.setState({
      visible: true
    });
  };

  closeMenu = () => {
    this.setState({
      visible: false
    });
  };

  HandleLanguage = lang => {
    localStorage.setItem("idioma", lang);
    window.location.reload();
  };

  render() {
    const { visible } = this.state;
    return (
      <header>
        <Container>
          <Link to="/">
            <img src={logo} alt="logo" width="84" />
          </Link>{" "}
          <span className="slogan">Economize tempo e dinheiro</span>
          <div className={"menu-container " + (visible ? "visible" : "")}>
            <div className="top-menu">
              <img src={logoBlue} alt="logo" />

              <Icon
                type="close"
                className="close-menu"
                onClick={this.closeMenu}
              />
            </div>
            <ul className="menu-header">
              {/* <li className="menu-urbano">
                <Link to="/aplicativo-de-transporte">
                  <b>
                    <img alt="" src={icoUrbano} width="50" />
                  </b>
                  Vah{" "}
                  <span>
                    <FormattedMessage id="menuUrbano" />
                  </span>
                </Link>
              </li> */}
              <li className="menu-aereo">
                <Link to="/passagens-aereas">
                  <b>
                    <img alt="" src={icoAereo} width="50" />
                  </b>Vah{" "}
                  <span>
                    <FormattedMessage id="menuAereo" />
                  </span>
                </Link>
              </li>
              <li className="menu-rodoviario">
                <Link to="/passagem-de-onibus">
                  <b>
                    <img alt="" src={icoRodoviario} width="60" />
                  </b>Vah{" "}
                  <span>
                    <FormattedMessage id="menuRodoviario" />
                  </span>
                </Link>
              </li>
              <li className="menu-locacao">
                <Link to="/aluguel-de-carros">
                  <b>
                    <img alt="" src={icoLocacao} width="50" />
                  </b>Vah{" "}
                  <span>
                    <FormattedMessage id="menuLocacao" />
                  </span>
                </Link>
              </li>
              <li className="menu-carona">
                <Link to="/carona-compartilhada">
                  <b>
                    <img alt="" src={icoCarona} width="40" />
                  </b>Vah{" "}
                  <span>
                    <FormattedMessage id="menuCarona" />
                  </span>
                </Link>
              </li>

              <li className="homebtn">
                <a onClick={() => this.HandleLanguage("pt-BR")}>
                  <img src={icoBR} alt="logo" width="30" />
                </a>
              </li>

              <li className="homebtn">
                <a onClick={() => this.HandleLanguage("es-gl")}>
                  <img src={icoES} alt="logo" width="30" />
                </a>
              </li>

              <li className="homebtn">
                <a
                  target="_blank"
                  href="https://itunes.apple.com/br/app/vah-economize-tempo-e-dinheiro/id1165261052?l=en&mt=8"
                >
                  <img src={appStore} alt="" />
                </a>
              </li>
              <li className="homebtn glink">
                <a
                  target="_blank"
                  href="https://play.google.com/store/apps/details?id=com.vah&hl=pt_BR"
                >
                  <img src={appGoogle} alt="" />
                </a>
              </li>
            </ul>
          </div>
          <Icon
            type="menu-fold"
            className="open-menu"
            onClick={this.openMenu}
          />
        </Container>
      </header>
    );
  }
}

export default Header;
