import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";

import Container from "./container";
import logo from "../assets/images/core/logo.png";
import appGoogle from "../assets/images/core/google-play.svg";
import appStore from "../assets/images/core/app-store.svg";

import icoAereo from "../assets/images/icons/ico-vah-aereo-footer.svg";
import icoCarona from "../assets/images/icons/ico-vah-carona-footer.svg";
import icoLocacao from "../assets/images/icons/ico-vah-locacao-footer.png?no-s";
import icoRodoviario from "../assets/images/icons/ico-vah-rodoviario-footer.svg";
import icoUrbano from "../assets/images/icons/ico-vah-urbano-footer.svg";

import icoFacebook from "../assets/images/icons/ico-facebook.svg";
import icoInstagram from "../assets/images/icons/ico-instagram.svg";

const Footer = () => (
  <Fragment>
    <div className="bannerFooter">
      <Container>
        <div className="infoBanner">
          <img src={logo} width="48" />
          <h3>
            <FormattedMessage id="bannerTitulo" />
            <span>
              <FormattedMessage id="bannerTituloB" />
            </span>
          </h3>
          <p>
            <span>
              <FormattedMessage id="bannerDescricao" />{" "}
            </span>
            <a
              rel="noopener noreferrer"
              href="https://play.google.com/store/apps/details?id=com.vah&hl=pt_BR"
              target="_blank"
            >
              <img src={appGoogle} alt="" />
            </a>
            <a
              rel="noopener noreferrer"
              href="https://itunes.apple.com/br/app/vah-economize-tempo-e-dinheiro/id1165261052?l=en&mt=8"
              target="_blank"
            >
              <img src={appStore} alt="" />
            </a>
          </p>
        </div>
      </Container>
    </div>
    <footer className="main-footer">
      <div className="no-mobile">
        <Container>
          <div className="topo-footer">
            <div>
              <img src={logo} alt="logo" width="84" />{" "}
              <span className="slogan">
                <FormattedMessage id="footerSlogan" />
              </span>
            </div>
            <ul className="navigation">
              <li>
                <Link to="/vah">
                  <FormattedMessage id="footerSobre" />
                </Link>
              </li>
              <li>
                <Link to="/vah/termos-de-uso">
                  <FormattedMessage id="footerTermos" />
                </Link>
              </li>
              <li>
                <Link to="/vah/contato">
                  <FormattedMessage id="footerContato" />
                </Link>
              </li>
              <li>
                <a href="https://vahcompare.com/blog" target="_blank">
                  <FormattedMessage id="footerBlog" />
                </a>
              </li>
              <li className="social-links">
                <FormattedMessage id="footerSiga" />{" "}
                <span className="icon-circ">
                  <a
                    href="https://www.facebook.com/appvah/"
                    rel="noopener noreferrer"
                    target="_blank"
                  >
                    <img alt="" src={icoFacebook} width="18" />
                  </a>
                </span>
                <span className="icon-circ">
                  <a
                    href="https://www.instagram.com/appvah/"
                    rel="noopener noreferrer"
                    target="_blank"
                  >
                    <img alt="" src={icoInstagram} width="16" />
                  </a>
                </span>
              </li>
            </ul>
          </div>
          <div className="bottom-footer">
            <ul className="menu-footer">
              {/* <li>
                <Link to="/aplicativo-de-transporte">
                  <span className="icon">
                    <img alt="" src={icoUrbano} width="40" />
                  </span>
                  vah<strong>
                    <FormattedMessage id="menuUrbano" />
                  </strong>
                </Link>
              </li> */}
              <li>
                <Link to="/passagens-aereas">
                  <span className="icon">
                    <img alt="" src={icoAereo} width="40" />
                  </span>
                  vah<strong>
                    <FormattedMessage id="menuAereo" />
                  </strong>
                </Link>
              </li>
              <li>
                <Link to="/passagem-de-onibus">
                  <span className="icon">
                    <img alt="" src={icoRodoviario} width="40" />
                  </span>
                  vah<strong>
                    <FormattedMessage id="menuRodoviario" />
                  </strong>
                </Link>
              </li>
              <li>
                <Link to="/aluguel-de-carros">
                  <span className="icon">
                    <img alt="" src={icoLocacao} width="40" />
                  </span>
                  vah<strong>
                    <FormattedMessage id="menuLocacao" />
                  </strong>
                </Link>
              </li>
              <li>
                <Link to="/carona-compartilhada">
                  <span className="icon">
                    <img alt="" src={icoCarona} width="30" />
                  </span>
                  vah<strong>
                    <FormattedMessage id="menuCarona" />
                  </strong>
                </Link>
              </li>
            </ul>
            <div className="buttons-app">
              <a
                rel="noopener noreferrer"
                href="https://play.google.com/store/apps/details?id=com.vah&hl=pt_BR"
                target="_blank"
              >
                <img src={appGoogle} alt="" />
              </a>
              <a
                rel="noopener noreferrer"
                href="https://itunes.apple.com/br/app/vah-economize-tempo-e-dinheiro/id1165261052?l=en&mt=8"
                target="_blank"
              >
                <img src={appStore} alt="" />
              </a>
            </div>
          </div>
        </Container>
      </div>
      <div className="yes-mobile">
        <Container>
          <div className="topf">
            <img src={logo} alt="logo" width="84" />{" "}
            <span className="slogan">Baixe agora e comece a economizar!</span>
          </div>
          <div className="buttons-app">
            <a
              rel="noopener noreferrer"
              href="https://play.google.com/store/apps/details?id=com.vah&hl=pt_BR"
              target="_blank"
            >
              <img src={appGoogle} alt="" />
            </a>
            <a
              rel="noopener noreferrer"
              href="https://itunes.apple.com/br/app/vah-economize-tempo-e-dinheiro/id1165261052?l=en&mt=8"
              target="_blank"
            >
              <img src={appStore} alt="" />
            </a>
          </div>

          <div className="bottom-footer">
            <ul className="menu-footer">
              <li>
                <Link to="/aplicativo-de-transporte">
                  <span className="icon">
                    <img alt="" src={icoUrbano} width="40" />
                  </span>
                  vah<strong>
                    <FormattedMessage id="menuUrbano" />
                  </strong>
                </Link>
              </li>
              <li>
                <Link to="/passagens-aereas">
                  <span className="icon">
                    <img alt="" src={icoAereo} width="40" />
                  </span>
                  vah<strong>
                    <FormattedMessage id="menuAereo" />
                  </strong>
                </Link>
              </li>
              <li>
                <Link to="/passagem-de-onibus">
                  <span className="icon">
                    <img alt="" src={icoRodoviario} width="40" />
                  </span>
                  vah<strong>
                    <FormattedMessage id="menuRodoviario" />
                  </strong>
                </Link>
              </li>
              <li>
                <Link to="/aluguel-de-carros">
                  <span className="icon">
                    <img alt="" src={icoLocacao} width="40" />
                  </span>
                  vah<strong>
                    <FormattedMessage id="menuLocacao" />
                  </strong>
                </Link>
              </li>
              <li>
                <Link to="/carona-compartilhada">
                  <span className="icon">
                    <img alt="" src={icoCarona} width="30" />
                  </span>
                  vah<strong>
                    <FormattedMessage id="menuCarona" />
                  </strong>
                </Link>
              </li>
            </ul>
          </div>

          <ul className="navigation">
            <li>
              <Link to="/vah">Sobre</Link>
            </li>
            <li>
              <Link to="/vah/termos-de-uso">Termos de uso</Link>
            </li>
            <li>
              <Link to="/vah/contato">Contato</Link>
            </li>
            <li>
              <a href="https://vahcompare.com/blog" target="_blank">
                Blog
              </a>
            </li>
            <li className="social-links">
              siga-nos{" "}
              <span className="icon-circ">
                <a
                  href="https://www.facebook.com/appvah/"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  <img alt="" src={icoFacebook} width="18" />
                </a>
              </span>
              <span className="icon-circ">
                <a
                  href="https://www.instagram.com/appvah/"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  <img alt="" src={icoInstagram} width="16" />
                </a>
              </span>
            </li>
          </ul>
        </Container>
      </div>
    </footer>
  </Fragment>
);

export default Footer;
