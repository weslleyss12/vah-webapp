import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import Container from './container';

const HeaderInfo = styled.div`
  width: 100%;
  height: 80px;
  background: #a2ea71;
  padding: 15px 0;
  p {
    margin: 0;
  }
`;

const HeaderInfoContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const ButtonBack = styled.a`
  font-size: 14px;
  text-transform: uppercase;
  font-family: 'Gilroy-Bold';
  color: #244a88;
  margin-right: 30px;
`;

const ButtonEdit = styled.a`
  width: 190px;
  height: 48px;
  background: #fff;
  text-align: center;
  line-height: 48px;
  border-radius: 5px;
  text-transform: uppercase;
  color: #244a88;
  font-family: 'Gilroy-SemiBold';
`;

const DestinyInfo = styled.div`
  background: #a2ea71;
  display: flex;
  align-items: center;
`;

const InfosAddress = styled.div`
  p {
    font-size: 18px;
    color: #244a88;
    font-family: 'Gilroy-SemiBold';
    position: relative;
    &:before {
      content: '';
      display: inline-block;
      width: 18px;
      height: 18px;
      vertical-align: middle;
      margin-right: 10px;
      border-radius: 50%;
    }
  }
`;
const AddressStart = styled.p`
  &:before {
    background: #fff;
  }
  &:after {
    content: '';
    display: inline-block;
    width: 4px;
    height: 28px;
    background: #fff;
    position: absolute;
    left: 7px;
    top: 10px;
  }
`;

const AddressEnd = styled.p`
  &:before {
    background: #244a88;
  }
`;

const InfoHeader = ({ infos }) => (
  <HeaderInfo>
    <Container>
      <HeaderInfoContainer>
        <ButtonBack>Voltar</ButtonBack>
        <DestinyInfo>
          <InfosAddress>
            <AddressStart>
              {`
                ${decodeURIComponent(infos.start_address)},
                ${decodeURIComponent(infos.start_city)}
              `}
            </AddressStart>
            <AddressEnd>
              {`
                ${decodeURIComponent(infos.end_address)},
                ${decodeURIComponent(infos.end_city)}
              `}
            </AddressEnd>
          </InfosAddress>
        </DestinyInfo>
        <ButtonEdit>Editar sua busca</ButtonEdit>
      </HeaderInfoContainer>
    </Container>
  </HeaderInfo>
);

InfoHeader.propTypes = {
  infos: PropTypes.shape()
};

InfoHeader.defaultProps = {
  infos: {
    start_address: '',
    end_address: '',
    start_city: '',
    end_city: ''
  }
};

export default InfoHeader;
