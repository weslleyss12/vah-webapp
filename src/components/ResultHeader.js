import React from "react";
import { Row, Col } from "antd";
import { Link } from "react-router-dom";
import styled from "styled-components";
import PropTypes from "prop-types";

import { FormattedMessage } from "react-intl";

import Container from "./container";
import voltar_btn from "../assets/images/icons/VOLTAR.png";

const Wrapper = styled.div`
  height: 80px;
  padding: 12px 0;
  background-color: #a2ea71;

  p {
    margin: 0;
  }
`;

const BackCol = styled(Col)`
  @media (max-width: 768px) {
    display: none;
  }
`;

const ButtonBack = styled(Link)`
  font-size: 14px;
  font-weight: 700;
  letter-spacing: 0.45px;
  padding-left: 20px;
  text-transform: uppercase;
  font-family: "Gilroy-Bold";
  color: #244a88;

  &:before {
    content: "";
    width: 14px;
    height: 23px;
    position: absolute;
    left: 0;
    background-image: url(${voltar_btn});
    background-repeat: no-repeat;
  }

  &:hover {
    color: #244a88;
  }
`;

const InfosAddress = styled.div`
  p {
    font-size: 19px;
    letter-spacing: 0.43px;
    color: #244a88;
    font-family: "Gilroy-SemiBold";
    position: relative;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;

    &:before {
      content: "";
      display: inline-block;
      width: 18px;
      height: 18px;
      vertical-align: middle;
      margin-right: 10px;
      border-radius: 50%;
    }
  }

  &:before {
    content: "";
    display: inline-block;
    width: 4px;
    height: 28px;
    background: #fff;
    position: absolute;
    left: 7px;
    top: 10px;
  }
`;

const AddressStart = styled.p`
  &:before {
    background: #fff;
  }
`;

const AddressEnd = styled.p`
  &:before {
    background: #244a88;
  }
`;

const EditCol = styled(Col)`
  text-align: right;

  @media (max-width: 768px) {
    display: none;
  }
`;

const ButtonEdit = styled(Link)`
  width: 190px;
  height: 48px;
  background: #fff;
  text-align: center;
  line-height: 48px;
  border-radius: 5px;
  text-transform: uppercase;
  color: #244a88;
  font-family: "Gilroy-SemiBold";
  display: inline-block;
`;

const ResultHeader = ({ infos, backUrl }) => (
  <div className="info-wrapper">
    <Container>
      <Row type="flex" justify="center" align="middle">
        <Col xs={0} md={5}>
          <Link className="ButtonBack" to={backUrl}>
            <FormattedMessage id="geralBtnVoltar" />
          </Link>
        </Col>

        <Col xs={15} md={12}>
          <div className="InfosAddress">
            <p className="AddressStart">
              {infos.start_address &&
                `
                ${decodeURIComponent(infos.start_address)},
                ${decodeURIComponent(infos.start_city)}
              `}

              {infos.origin_place &&
                `
                ${decodeURIComponent(infos.origin_place)},
                ${decodeURIComponent(infos.origin_state)}
              `}
            </p>
            <p className="AddressEnd">
              {infos.end_city &&
                `
                ${decodeURIComponent(infos.end_address)},
                ${decodeURIComponent(infos.end_city)}
              `}

              {infos.destination_place &&
                `
                ${decodeURIComponent(infos.destination_place)},
                ${decodeURIComponent(infos.destination_state)}
              `}
            </p>
          </div>
        </Col>

        <Col className="EditCol" xs={9} md={7}>
          <Link className="ButtonEdit" to={backUrl}>
            <FormattedMessage id="geralBtnRefazer" />
          </Link>
        </Col>
      </Row>
    </Container>
  </div>
);

ResultHeader.propTypes = {
  infos: PropTypes.shape()
};

ResultHeader.defaultProps = {
  infos: {
    start_address: "",
    end_address: "",
    start_city: "",
    end_city: "",
    origin_place: "",
    origin_state: "",
    destination_place: "",
    destination_state: ""
  }
};

export default ResultHeader;
