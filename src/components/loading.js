import React, { Component } from 'react';
import { Progress } from 'antd';

let interval = null;

class Loading extends Component {
  constructor(props) {
    super(props);

    setTimeout(() => {
      this._loading();
    }, 500);

    this.state = {
      progress: 0
    };
  }

  componentWillUnmount = () => {
    clearInterval(interval);

    interval = null;
  };

  _loading = () => {
    const { averageLoadingTime, onFinish, finishAfter } = this.props;
    const firstPart = averageLoadingTime * 0.85;
    const sencondPart = averageLoadingTime * 0.15;
    const sencondPartSpeed = sencondPart / (firstPart / 2 + sencondPart);
    let p = 0;
    let finishing = false;

    interval = setInterval(() => {
      if (p / 5 >= firstPart) {
        p += sencondPartSpeed;
      } else {
        p += 2;
      }

      // Force finish
      if (!this.props.loading) {
        p = averageLoadingTime * 5;
      }

      // Finish
      if (p / 5 >= averageLoadingTime && !finishing && !this.props.loading) {
        finishing = true;

        // Time to complete loader
        setTimeout(() => {
          // Stop timer
          clearInterval(interval);

          this.setState({ progress: 100 }, () => {
            // Time to call the finish method
            setTimeout(() => {
              onFinish();
            }, 1000);
          });
        }, finishAfter ? finishAfter * 1000 : 0);
      } else {
        const total = parseInt((p / 5 / averageLoadingTime) * 100);
        this.setState({ progress: total > 100 ? 100 : total });
      }
    }, 200);
  };

  render() {
    const { progress } = this.state;

    return (
      <Progress
        percent={progress}
        showInfo={false}
        className="progressLoading"
      />
    );
  }
}

export default Loading;
