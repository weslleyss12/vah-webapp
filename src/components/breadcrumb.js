import React from "react";
import { Link } from "react-router-dom";

import Container from "./container";
import appGoogle from "../assets/images/core/google-play.svg";
import appStore from "../assets/images/core/app-store.svg";

import icoBR from "../assets/images/br.png";
import icoES from "../assets/images/esp.png";

import { FormattedMessage } from "react-intl";

const HandleLanguage = lang => {
  localStorage.setItem("idioma", lang);
  window.location.reload();
};

const Breadcrumb = ({ children }) => (
  <div className="breadcrumb">
    <Container>
      <ul className="menu-breadcrumb">
        <li className="btn-prev">
          <Link to="#">
            <FormattedMessage id="geralBtnVoltar" />
          </Link>
        </li>
        {children}
      </ul>
      <div className="buttons-app">
        <a onClick={() => HandleLanguage("pt-BR")}>
          <img src={icoBR} alt="logo" width="28" />
        </a>
        <a onClick={() => HandleLanguage("es-gl")}>
          <img src={icoES} alt="logo" width="28" />
        </a>
        <a
          rel="noopener noreferrer"
          href="https://play.google.com/store/apps/details?id=com.vah&hl=pt_BR"
          target="_blank"
        >
          <img src={appGoogle} alt="" className="app" />
        </a>
        <a
          rel="noopener noreferrer"
          href="https://itunes.apple.com/br/app/vah-economize-tempo-e-dinheiro/id1165261052?l=en&mt=8"
          target="_blank"
        >
          <img src={appStore} alt="" className="app" />
        </a>
      </div>
    </Container>
  </div>
);

export default Breadcrumb;
