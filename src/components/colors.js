export default {
  primary: '#00498b',
  secundary: '#a2ea71',
  third: '#a2ea71',
  text: '#505050',
  smallButtons: '#B1B1B1'
};
