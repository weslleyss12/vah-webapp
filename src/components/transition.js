import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";

import Container from "../components/container";
import { FormattedMessage } from "react-intl";

let interval = null;
let attempts = 0;

class AddressTransition extends Component {
  static propTypes = {
    activeTab: PropTypes.string
  };

  static defaultProps = {
    activeTab: ""
  };

  state = {};

  componentWillUnmount = () => {
    clearInterval(interval);

    interval = null;
    attempts = 0;
  };

  _onFinish = () => {
    interval = setInterval(this._checkResults, 1000);
    this._checkResults();
  };

  _checkResults = () => {
    const { results, toggleTransition } = this.props;

    attempts += 1;

    if (results) {
      clearInterval(interval);
      toggleTransition();
    } else if (attempts === 3) {
      clearInterval(interval);
      toggleTransition();
    }
  };

  render() {
    return (
      <Fragment>
        <section className="main-transition">
          <Container>
            <div className="loaderMain">
              <div className="loader" />
              <div className="loaderInfo">
                <p className="tit">
                  <FormattedMessage id="geralBtnTransicao" />
                </p>
                <span className="dot">.</span>
                <p className="desc">
                  <FormattedMessage id="geralBtnTransicao2" />
                </p>
              </div>
            </div>
          </Container>
        </section>
      </Fragment>
    );
  }
}

export default AddressTransition;
