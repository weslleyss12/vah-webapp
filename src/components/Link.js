import React from 'react';
import { get } from 'lodash';
import { withRouter, Link as RLinK } from 'react-router-dom';
import PropTypes from 'prop-types';

import generatePath from '../utils/generatePath';

const Link = ({ pattern, params, children, ...rest }) => {
  const { match } = rest;
  const matchParams = get(match, 'params', {});

  const to = generatePath(pattern, { ...matchParams, ...params });

  return <RLinK to={to}>{children}</RLinK>;
};

Link.propTypes = {
  pattern: PropTypes.string.isRequired,
  params: PropTypes.shape()
};

Link.defaultProps = {
  params: {}
};

export default withRouter(Link);
