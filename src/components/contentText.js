import React from "react";
import { Row, Col } from "antd";
import { FormattedMessage } from "react-intl";

import icoWhat from "../assets/images/icons/ico-what.svg";
import icoFree from "../assets/images/icons/ico-free.svg";
import icoMedia from "../assets/images/icons/ico-media.svg";

const ContentText = () => (
  <div>
    <h1>
      <FormattedMessage id="contentTituloA" /> <strong>Vah</strong>
      <FormattedMessage id="contentTituloB" />
    </h1>
    <Row type="flex" justify="center" gutter={40}>
      <Col xs={20} md={7}>
        <span className="icon">
          <img src={icoWhat} alt="" width="70" />
        </span>
        <h3>
          <FormattedMessage id="contentOque" />
        </h3>
        <p>
          <FormattedMessage id="contentOqueText" />
        </p>
      </Col>
      <Col xs={20} md={7}>
        <span className="icon">
          <img src={icoFree} alt="" width="120" />
        </span>
        <h3>
          <FormattedMessage id="contentGratuito" />
        </h3>
        <p>
          <FormattedMessage id="contentGratuitoText" />
        </p>
      </Col>
      <Col xs={20} md={7}>
        <span className="icon">
          <img src={icoMedia} alt="" width="115" />
        </span>
        <h3>
          <FormattedMessage id="contentMidia" />
        </h3>
        <p>
          <FormattedMessage id="contentMidiaTextA" />
          <a
            href="https://exame.abril.com.br/seu-dinheiro/aplicativo-compara-precos-cobrados-pelo-uber-e-seus-concorrentes/"
            target="_blank"
          >
            Exame
          </a>,{" "}
          <a
            href="https://economia.uol.com.br/noticias/redacao/2016/12/28/uber-x-taxis-quer-economizar-mas-esta-perdido-compare-e-veja-opcoes.htm"
            target="_blank"
          >
            UOL
          </a>,{" "}
          <a
            href="https://veja.abril.com.br/economia/aplicativo-compara-precos-do-uber-99-easy-e-cabify/"
            target="_blank"
          >
            Veja
          </a>,{" "}
          <a
            href="https://www1.folha.uol.com.br/sobretudo/vida-pratica/2017/02/1855745-plataforma-compara-preco-de-taxi-uber-e-cabify.shtml"
            target="_blank"
          >
            Folha de S.Paulo
          </a>,{" "}
          <a
            href="https://www.forbes.com/sites/ricardogeromel/2017/09/12/latest-on-brazil-startup-scene-uber-facebook-google-zip-code-from-hell-and-more/#78f94eaa74e1"
            target="_blank"
          >
            Forbes
          </a>,{" "}
          <a
            href="https://brasil.elpais.com/brasil/2018/02/08/politica/1518109016_417940.html"
            target="_blank"
          >
            El País
          </a>,{" "}
          <a
            href="http://cbn.globoradio.globo.com/comentaristas/mais-sao-paulo/2016/12/13/RECURSO-GRATUITO-APONTA-QUAL-O-APLICATIVO-DE-TRANSPORTE-MAIS-BARATO.htm"
            target="_blank"
          >
            CBN
          </a>,{" "}
          <a
            href="http://blogs.opovo.com.br/id/2017/06/14/aplicativos-comparam-precos-de-servicos-de-transporte-individual/"
            target="_blank"
          >
            O Povo
          </a>,{" "}
          <a
            href="https://www.istoedinheiro.com.br/o-buscape-dos-aplicativos-de-transporte/"
            target="_blank"
          >
            Istoé Dinheiro
          </a>,{" "}
          <a
            href="https://videos.band.uol.com.br/16177326/concorrencia-entre-aplicativos-beneficia-passageiros.html"
            target="_blank"
          >
            Band
          </a>,{" "}
          <a
            href="https://revistapegn.globo.com/Startups/noticia/2017/04/empresa-movimenta-r-14-milhoes-com-comparador-de-precos-de-apps-como-uber-e-99.html"
            target="_blank"
          >
            Pequenas Empresas Grandes Negócios
          </a>,{" "}
          <a
            href="http://odia.ig.com.br/rio-de-janeiro/observatorio/2017-05-07/vai-de-cabify-uber-easy-ou-99.html"
            target="_blank"
          >
            O Dia
          </a>,{" "}
          <a
            href="https://www.techtudo.com.br/dicas-e-tutoriais/2017/05/taxi-ou-uber-app-compara-precos-e-mostra-qual-e-o-transporte-mais-barato.ghtml"
            target="_blank"
          >
            TechTudo
          </a>,{" "}
          <a
            href="https://www.otempo.com.br/interessa/tecnologia-e-games/app-compara-pre%C3%A7os-de-corrida-e-economia-pode-chegar-a-30-1.1435307"
            target="_blank"
          >
            O Tempo
          </a>{" "}
          <FormattedMessage id="contentMidiaTextB" />
        </p>
      </Col>
    </Row>
  </div>
);

export default ContentText;
