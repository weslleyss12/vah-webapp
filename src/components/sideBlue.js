import React, { Component } from "react";

class SideBlue extends Component {
  state = {
    sideShow: true,
    btnsShow: false
  };

  componentDidMount() {
    this.props.onRef(this);
    this.setState({ sideShow: window.innerWidth > 992 });
    this.setState({ btnsShow: window.innerWidth < 992 });
  }

  componentWillUnmount() {
    this.props.onRef(null);
  }

  openSide = () => {
    this.setState({ sideShow: true });
  };

  closeSide = () => {
    this.setState({ sideShow: false });
  };

  render() {
    const { sideShow, btnsShow } = this.state;

    return (
      <div
        className="sidebar"
        onRef={ref => {
          this.child = ref;
        }}
      >
        {!sideShow &&
          btnsShow && (
            <p className="btnAction btnOpen" onClick={this.openSide}>
              Toque para refinar sua <br />busca com os filtros
            </p>
          )}

        {sideShow &&
          btnsShow && (
            <p className="btnAction btnClose" onClick={this.closeSide}>
              Fechar
            </p>
          )}

        {sideShow && this.props.children}
      </div>
    );
  }
}

export default SideBlue;
