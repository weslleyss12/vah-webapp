import React from "react";
import { Link } from "react-router-dom";

import icoAereo from "../assets/images/icons/ico-vah-aereo.svg";
import icoCarona from "../assets/images/icons/ico-vah-carona.svg";
import icoLocacao from "../assets/images/icons/ico-vah-locacao.svg";
import icoRodoviario from "../assets/images/icons/ico-vah-rodoviario.svg";
import icoUrbano from "../assets/images/icons/ico-vah-urbano.svg";

import Container from "./container";

const Navigation = ({ active }) => (
  <Container>
    <nav className="main-menu">
      <ul>
        <li>
          <Link
            to="/aplicativo-de-transporte"
            className={active === "urbano" ? "active" : ""}
          >
            <span>
              <img alt="" src={icoUrbano} width="50" />
            </span>
            vah<strong>urbano</strong>
          </Link>
        </li>
        <li>
          <Link
            to="/passagens-aereas"
            className={active === "aereo" ? "active" : ""}
          >
            <span>
              <img alt="" src={icoAereo} width="50" />
            </span>
            vah<strong>aéreo</strong>
          </Link>
        </li>
        <li>
          <Link
            to="/passagem-de-onibus"
            className={active === "rodoviario" ? "active" : ""}
          >
            <span>
              <img alt="" src={icoRodoviario} width="67" />
            </span>
            vah<strong>rodoviário</strong>
          </Link>
        </li>
        <li>
          <Link
            to="/aluguel-de-carros"
            className={active === "aluguel" ? "active" : ""}
          >
            <span>
              <img alt="" src={icoLocacao} width="50" />
            </span>
            vah<strong>locação</strong>
          </Link>
        </li>
        <li>
          <Link
            to="/carona-compartilhada"
            className={active === "carona" ? "active" : ""}
          >
            <span>
              <img alt="" src={icoCarona} width="40" />
            </span>
            vah<strong>carona</strong>
          </Link>
        </li>
      </ul>
    </nav>
  </Container>
);

export default Navigation;
