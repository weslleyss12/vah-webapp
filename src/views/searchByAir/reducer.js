import { timeMoment } from '../../utils/helpers'

import {
  ACTION_SEARCH_BY_AIR_SET_TYPE,
  ACTION_SEARCH_BY_AIR_SET_AIRPORT,
  ACTION_SEARCH_BY_AIR_SET_DATE,
  ACTION_SEARCH_BY_AIR_SET_ADDITIONAL,
  ACTION_SEARCH_BY_AIR_SET_DIRECT,
  ACTION_SEARCH_BY_AIR_REVERSE_AIRPORT,
  ACTION_SEARCH_BY_AIR_FETCH_PENDING,
  ACTION_SEARCH_BY_AIR_FETCH_FULFILLED,
  ACTION_SEARCH_BY_AIR_FETCH_REJECTED,
  ACTION_SEARCH_BY_AIR_AUTOCOMPLETE_FETCH_PENDING,
  ACTION_SEARCH_BY_AIR_AUTOCOMPLETE_FETCH_FULFILLED,
  ACTION_SEARCH_BY_AIR_AUTOCOMPLETE_FETCH_REJECTED,
  ACTION_SEARCH_BY_AIR_CLEAR_ESTIMATES,
  ACTION_SEARCH_BY_AIR_SET_FILTERS
} from '../../main/constants';

const INITIAL_STATE = {
  type: 'round_trip',
  origin: null,
  destination: null,
  adults: 1,
  children: 0,
  infants: 0,
  direct: 0,
  go_date: null,
  return_date: null,
  pending: true,
  error: null,
  results: null,
  full_estimates: [],
  filters: {},
  autocomplete_results: [],
  pending_autocomplete: false
};

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case ACTION_SEARCH_BY_AIR_SET_TYPE:
      return { ...state, type: payload, return_date: (payload === 'one_way' ? null : state.return_date) }

    case ACTION_SEARCH_BY_AIR_SET_AIRPORT:
      return { ...state, [payload.type]: payload.airport, autocomplete_results: [] }

    case ACTION_SEARCH_BY_AIR_SET_DATE:
      return { ...state, [payload.type + '_date']: payload.date }

    case ACTION_SEARCH_BY_AIR_SET_ADDITIONAL:
      return { ...state, [payload.type]: payload.data }

    case ACTION_SEARCH_BY_AIR_SET_DIRECT:
      return { ...state, direct: payload }

    case ACTION_SEARCH_BY_AIR_REVERSE_AIRPORT:
      return { ...state, origin: state.destination, destination: state.origin }

    case ACTION_SEARCH_BY_AIR_FETCH_PENDING:
      return { ...state, pending: true, results: null, full_estimates: [], filters: {} }

    case ACTION_SEARCH_BY_AIR_AUTOCOMPLETE_FETCH_PENDING:
      return { ...state, pending_autocomplete: true, results: null }

    case ACTION_SEARCH_BY_AIR_FETCH_FULFILLED:
      if (payload.status) {
        return { ...state, pending: false, results: payload.data, full_estimates: payload.data.estimates }
      }

      return { ...state, pending: false }

    case ACTION_SEARCH_BY_AIR_AUTOCOMPLETE_FETCH_FULFILLED:
      if (payload.status) {
        return { ...state, pending_autocomplete: false, autocomplete_results: payload.data }
      }

      return { ...state, pending: false }

    case ACTION_SEARCH_BY_AIR_FETCH_REJECTED:
      return { ...state, pending: false, error: payload }

    case ACTION_SEARCH_BY_AIR_AUTOCOMPLETE_FETCH_REJECTED:
      return { ...state, pending_autocomplete: false, error: payload }

    case ACTION_SEARCH_BY_AIR_CLEAR_ESTIMATES:
      return { ...state, results: { ...state.results, estimates: [] }, pending: true }

    case ACTION_SEARCH_BY_AIR_SET_FILTERS:
      const results = filterItems(payload, state)
      return { ...state, filters: payload, results, pending: false }

    default:
      return state;
  }
};

const filterItems = (filters, state, localResults) => {
  const e = state ? state.full_estimates : localResults.estimates
  const r = state ? state.results : localResults

  // Hour
  const validHour = (hour) => (hour !== 'Any time' && hour !== 'Qualquer horário')

  const isAValidMoment = (hour, date) => {
    if (
      ((hour.indexOf('Morning') !== -1 || hour.indexOf('Manhã') !== -1) && timeMoment(date) === 'morning') ||
      ((hour.indexOf('Afternoon') !== -1 || hour.indexOf('Tarde') !== -1) && timeMoment(date) === 'afternoon') ||
      ((hour.indexOf('Night') !== -1 || hour.indexOf('Noite') !== -1) && timeMoment(date) === 'night')
    ) {
      return true
    }

    return false
  }

  // Stops
  const isValidStops = (filters, stops) => {
    let status = false

    if (stops === 0 && filters.indexOf('Voo direto') !== -1) {
      status = true
    }
    else if (stops > 0 && filters.indexOf(`${stops} Escala(s)`) !== -1) {
      status = true
    }

    return status
  }

  const estimates = e.filter(estimate => {
    return (
      (filters.companies ? filters.companies.indexOf(estimate.outbound.carriers[0].name) !== -1 : true) &&
      ((filters.companies && estimate.inbound) ? filters.companies.indexOf(estimate.inbound.carriers[0].name) !== -1 : true) &&
      (filters.stops ? (isValidStops(filters.stops, estimate.outbound.stops) && (estimate.inbound ? isValidStops(filters.stops, estimate.inbound.stops) : true)) : true) &&
      ((filters.hour_outbound && validHour(filters.hour_outbound)) ? isAValidMoment(filters.hour_outbound, estimate.outbound.departure) : true) &&
      (((filters.hour_inbound && estimate.inbound) && validHour(filters.hour_inbound)) ? isAValidMoment(filters.hour_inbound, estimate.inbound.departure) : true) &&
      (filters.price ? (parseInt(estimate.price) <= filters.price) : true)
    )
  })

  return { ...r, estimates }
}
