import { fetchDefaultOptions } from '../../utils/fetchDefaultOptions';
import Config from '../../config';

import {
  ACTION_SEARCH_BY_AIR_SET_TYPE,
  ACTION_SEARCH_BY_AIR_SET_AIRPORT,
  ACTION_SEARCH_BY_AIR_SET_DATE,
  ACTION_SEARCH_BY_AIR_SET_ADDITIONAL,
  ACTION_SEARCH_BY_AIR_REVERSE_AIRPORT,
  ACTION_SEARCH_BY_AIR_AUTOCOMPLETE_FETCH,
  ACTION_SEARCH_BY_AIR_FETCH,
  ACTION_SEARCH_BY_AIR_CLEAR_ESTIMATES,
  ACTION_SEARCH_BY_AIR_SET_FILTERS
} from '../../main/constants';

function actionAutocomplete(term) {
  const url = `${Config.AIR_API_BASE_URL}/air/places?term=${term}`;

  return fetch(url, fetchDefaultOptions())
    .then(response => response.json())
    .then(responseJson => responseJson)
    .catch(error => error.message);
}

function actionSearch(values) {
  const url = `${Config.AIR_API_BASE_URL}/estimates/air`;

  return fetch(url, fetchDefaultOptions('POST', values, true, false))
    .then(response => response.json())
    .then(responseJson => responseJson)
    .catch(error => error.message);
}

export function autocomplete(values) {
  return {
    type: ACTION_SEARCH_BY_AIR_AUTOCOMPLETE_FETCH,
    payload: actionAutocomplete(values)
  };
}

export function searchAir(values) {
  return {
    type: ACTION_SEARCH_BY_AIR_FETCH,
    payload: actionSearch(values)
  };
}

export function setType(type) {
  return {
    type: ACTION_SEARCH_BY_AIR_SET_TYPE,
    payload: type
  };
}

export function setAirport(airport, type) {
  return {
    type: ACTION_SEARCH_BY_AIR_SET_AIRPORT,
    payload: { airport, type }
  };
}

export function setDate(date, type) {
  return {
    type: ACTION_SEARCH_BY_AIR_SET_DATE,
    payload: { date, type }
  };
}

export function setAdditional(data, type) {
  return {
    type: ACTION_SEARCH_BY_AIR_SET_ADDITIONAL,
    payload: { data, type }
  };
}

export function reverseAirports() {
  return {
    type: ACTION_SEARCH_BY_AIR_REVERSE_AIRPORT
  };
}

export function clearEstimates() {
  return {
    type: ACTION_SEARCH_BY_AIR_CLEAR_ESTIMATES
  };
}

export function setFilters(filters) {
  return {
    type: ACTION_SEARCH_BY_AIR_SET_FILTERS,
    payload: filters
  };
}
