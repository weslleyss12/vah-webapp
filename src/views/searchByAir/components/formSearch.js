import React, { Component } from "react";
import { get } from "lodash";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { FormattedMessage } from "react-intl";
import {
  Form,
  Input,
  Button,
  DatePicker,
  Radio,
  AutoComplete,
  Row,
  Col
} from "antd";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import moment from "moment";

import "./formSearch.css";

// Helpers
import { trim, slugify } from "../../../utils/helpers";
import generatePath from "../../../utils/generatePath";
import localeDatepPicker from "../../../utils/localeDatepPicker";

// Actions
import {
  setType,
  autocomplete,
  setAirport,
  setDate,
  setAdditional
} from "../actions";

const FormItem = Form.Item;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const { Option } = AutoComplete;

class FormSearch extends Component {
  static propTypes = {
    form: PropTypes.shape().isRequired,
    setType: PropTypes.func.isRequired,
    autocomplete: PropTypes.func.isRequired,
    setAirport: PropTypes.func.isRequired,
    setDate: PropTypes.func.isRequired,
    setAdditional: PropTypes.func.isRequired,
    origin: PropTypes.shape(),
    destination: PropTypes.shape(),
    type: PropTypes.string.isRequired
  };

  static defaultProps = {
    origin: null,
    destination: null
  };

  state = {
    dataSource: [],
    adults: 1,
    children: 0,
    babies: 0,
    searchTerm: null,
    returnDateOpen: false,
    ida: null,
    volta: null,
    isValidOrigin: false,
    isValidDestination: false,
    showPassengerModal: false
  };

  componentDidMount() {
    this.props.form.validateFields();
  }

  onChangeType = e => {
    const tripType = e.target.value;
    this.props.setType(tripType);
  };

  renderOption = (place, placeType) => (
    <Option
      key={`${placeType}-${place.id}`}
      value={`${place.description} (${place.id})`}
      place={place}
    >
      {place.description} ({place.id})
    </Option>
  );

  handleSelect = (_value, option, placeType) => {
    const {
      props: { place }
    } = option;
    const { origin, destination } = this.props;

    if (
      (placeType === "origin" && destination && place.id === destination.id) ||
      (placeType === "destination" && origin && place.id === origin.id)
    ) {
      alert("A origem e o destino não podem ser os mesmos.");
    } else {
      this.props.setAirport(place, placeType);

      if (placeType === "origin") this.setState({ isValidOrigin: true });
      else this.setState({ isValidDestination: true });
    }
  };

  increaseCounter = type => {
    if (this.props[type] < 9)
      this.props.setAdditional(this.props[type] + 1, type);
  };

  decreaseCounter = type => {
    if (type === "adults" && this.props[type] > 1)
      this.props.setAdditional(this.props[type] - 1, type);

    if (type !== "adults" && this.props[type] > 0)
      this.props.setAdditional(this.props[type] - 1, type);
  };

  _loadAutocomplete = input => {
    if (this.searching) {
      clearTimeout(this.searching);
    }

    this.searching = setTimeout(() => {
      this.props.autocomplete(input);
    }, 300);
  };

  handleSearch = (text, placeType) => {
    const { searchTerm } = this.state;
    const term = trim(text);
    const slugT = slugify(text);

    if (placeType === "origin") this.setState({ isValidOrigin: false });
    else this.setState({ isValidDestination: false });

    if (!term) {
      this.setState({ ...this.state, results: [] });
    } else if (term.length > 2 && slugT !== searchTerm) {
      this.setState({ ...this.state, searchTerm: slugT });
      this._loadAutocomplete(term);
    }
  };

  handleGoDate = (date, dateString) => {
    const formatedDate = date.format("YYYY-MM-DD");

    this.setState({ ida: date, volta: date });
    this.props.form.setFieldsValue({ ida: dateString });
    this.props.setDate(formatedDate, "go");
  };

  handleReturnDate = (date, dateString) => {
    const formatedDate = date.format("YYYY-MM-DD");

    this.setState({ volta: date });
    this.props.form.setFieldsValue({ volta: dateString });
    this.props.setDate(formatedDate, "return");
  };

  handleGoDateOpen = open => !open && this.setState({ returnDateOpen: true });

  handleReturnDateOpen = open => this.setState({ returnDateOpen: open });

  disabledGoDate = date =>
    date && moment(date).endOf("day") < moment().endOf("day");

  disabledReturnDate = date => {
    const { ida } = this.state;

    if (!date || !ida) return moment(date).endOf("day") < moment().endOf("day");

    return date < ida;
  };

  handleSubmit = e => {
    e.preventDefault();
    const {
      origin,
      destination,
      go_date,
      return_date,
      adults,
      children,
      infants,
      direct,
      match,
      history
    } = this.props;
    const matchParams = get(match, "params", {});

    const userLang = navigator.language || navigator.userLanguage;
    const lang = userLang.split("-");

    const latam = {
      AR: "ARS",
      BO: "BOB",
      BR: "BRL",
      CL: "CLP",
      CO: "COP",
      CR: "CRC",
      CU: "CUC",
      EC: "USD",
      SV: "USD",
      GT: "CTQ",
      HT: "HTG",
      HN: "HLN",
      MX: "MXN",
      NI: "NIO",
      PA: "PAB",
      PY: "PYG",
      PE: "PEN",
      DO: "DOP",
      UY: "UYU",
      VE: "VEF"
    };

    const moeda = lang[1];
    let currencyMoney = "";

    if (latam[`${moeda}`]) {
      currencyMoney = latam[`${moeda}`];
    } else {
      currencyMoney = "USD";
    }

    this.props.form.validateFields(err => {
      if (!err) {
        const url = generatePath(
          "/passagens-aereas/comprar/:origin/:destination/:adults/:children/:infants/:direct/:go_date/:return_date?/:country/:language/:currency/",
          {
            ...matchParams,

            origin: origin.id,
            destination: destination.id,
            adults,
            children,
            infants,
            direct,
            go_date,
            return_date,

            country: destination.country ? destination.country : "",
            language: lang[0],
            currency: currencyMoney
          }
        );

        history.push(url);
      }
    });
  };

  passengerSelect = () => {
    return (
      <div className="filter-container">
        <div className="filter-section">
          <div className="passenger-container">
            <div className="passenger-label">
              <span>Adultos</span>
              <p>16+ anos</p>
            </div>
            <div className="passenger-counter">
              <button
                type="button"
                onClick={() => this.decreaseCounter("adults")}
              >
                -
              </button>
              <input type="text" value={this.props.adults} readOnly />
              <button
                type="button"
                onClick={() => this.increaseCounter("adults")}
              >
                +
              </button>
            </div>
          </div>
          <div className="passenger-container">
            <div className="passenger-label">
              <span>Crianças</span>
              <p>1-16 anos</p>
            </div>
            <div className="passenger-counter">
              <button
                type="button"
                onClick={() => this.decreaseCounter("children")}
              >
                -
              </button>
              <input type="text" value={this.props.children} readOnly />
              <button
                type="button"
                onClick={() => this.increaseCounter("children")}
              >
                +
              </button>
            </div>
          </div>
          <div className="passenger-container">
            <div className="passenger-label">
              <span>Bebês</span>
              <p>12- meses</p>
            </div>
            <div className="passenger-counter">
              <button
                type="button"
                onClick={() => this.decreaseCounter("infants")}
              >
                -
              </button>
              <input type="text" value={this.props.infants} readOnly />
              <button
                type="button"
                onClick={() => this.increaseCounter("infants")}
              >
                +
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  };

  hasErrors = fieldsError =>
    Object.keys(fieldsError).some(field => fieldsError[field]);

  render() {
    const {
      type,
      autocomplete_results,
      adults,
      children,
      infants,
      form: { getFieldDecorator, getFieldsError }
    } = this.props;
    const {
      returnDateOpen,
      isValidOrigin,
      isValidDestination,
      ida,
      volta
    } = this.state;

    let lang = localStorage.getItem("idioma");
    let adultos = "";
    let criancas = "";
    let bebes = "";
    let passageiros = "";

    if (!lang || lang == "pt-BR") {
      adultos = "Adulto (12+ anos)";
      criancas = "Criança (2 a 11 anos)";
      bebes = "Bebê (0 a 23 meses)";
      passageiros = "passageiro(s)";
    } else if (lang == "es-gl") {
      adultos = "Adulto (12+ años)";
      criancas = "Niños (2 a 11 años)";
      bebes = "Bebés (0 a 23 meses)";
      passageiros = "pasajero(s)";
    }

    return (
      <Form
        className="search-form form-aereo"
        layout="inline"
        onSubmit={this.handleSubmit}
      >
        <h1>
          <FormattedMessage id="buscaAereoTitle" />
        </h1>

        <FormItem className="field-filtro">
          {getFieldDecorator("radio-button", {
            initialValue: type,
            onChange: this.onChangeType
          })(
            <RadioGroup>
              <RadioButton value="round_trip">
                <FormattedMessage id="buscaAereoFiltro1" />
              </RadioButton>
              <RadioButton value="one_way">
                <FormattedMessage id="buscaAereoFiltro2" />
              </RadioButton>
            </RadioGroup>
          )}
        </FormItem>

        <FormItem validateStatus="" help="" className="field-big">
          {getFieldDecorator("origin", {
            rules: [{ required: true }]
          })(
            <div>
              <label className="label-search">
                <FormattedMessage id="buscaAereoDe" />
              </label>
              <AutoComplete
                value={this.state.origin}
                placeholder={<FormattedMessage id="buscaAereoDe" />}
                dataSource={autocomplete_results.map(place =>
                  this.renderOption(place, "origin")
                )}
                onChange={value => this.setState({ origin: value })}
                onSearch={value => this.handleSearch(value, "origin")}
                onSelect={(value, option) =>
                  this.handleSelect(value, option, "origin")
                }
                optionLabelProp="value"
              />
            </div>
          )}
        </FormItem>

        <FormItem validateStatus="" help="" className="field-big-2">
          {getFieldDecorator("destination", {
            rules: [{ required: true }]
          })(
            <div>
              <label className="label-search">
                <FormattedMessage id="buscaAereoPara" />
              </label>
              <AutoComplete
                value={this.state.destination}
                placeholder={<FormattedMessage id="buscaAereoPara" />}
                dataSource={autocomplete_results.map(place =>
                  this.renderOption(place, "destination")
                )}
                onChange={value => this.setState({ destination: value })}
                onSearch={value => this.handleSearch(value, "destination")}
                onSelect={(value, option) =>
                  this.handleSelect(value, option, "destination")
                }
                optionLabelProp="value"
              />
            </div>
          )}
        </FormItem>

        <FormItem validateStatus="" help="" className="field-mini">
          {getFieldDecorator("ida", {
            rules: [{ required: true }]
          })(
            <div>
              <label className="label-search">
                <FormattedMessage id="buscaAereoDataIda" />
              </label>
              <DatePicker
                locale={localeDatepPicker}
                format="DD/MMM/YY"
                placeholder="Data"
                onChange={this.handleGoDate}
                onOpenChange={this.handleGoDateOpen}
                disabledDate={this.disabledGoDate}
                className="form-input"
                allowClear={false}
              />
            </div>
          )}
        </FormItem>

        {type === "round_trip" ? (
          <FormItem validateStatus="" help="" className="field-mini">
            {getFieldDecorator("volta", {
              rules: [{ required: true }]
            })(
              <div>
                <label className="label-search">
                  <FormattedMessage id="buscaAereoDataVolta" />
                </label>
                <DatePicker
                  locale={localeDatepPicker}
                  format="DD/MMM/YY"
                  placeholder="Data"
                  onChange={this.handleReturnDate}
                  onOpenChange={this.handleReturnDateOpen}
                  disabledDate={this.disabledReturnDate}
                  className="form-input"
                  allowClear={false}
                  open={returnDateOpen}
                  value={volta ? volta : ""}
                />
              </div>
            )}
          </FormItem>
        ) : null}

        <FormItem validateStatus="" help="" className="field-medium">
          <div className="passenger">
            <label className="label-search">
              <FormattedMessage id="buscaAereoPassageiro" />
            </label>
            <div className="passenger__input-wrapper">
              {/* <Popover
                placement="bottomLeft"
                content={this.passengerSelect()}
                trigger="click"
              > */}
              <Input
                className="form-input"
                type="text"
                placeholder="Para"
                value={`${adults + children + infants} ${passageiros}`}
                onClick={() => this.setState({ showPassengerModal: true })}
                readOnly
              />
              {/* </Popover> */}

              {this.state.showPassengerModal && (
                <div className="passenger__input-modal">
                  <Row className="passenger__option-row">
                    <Col span={4}>
                      <button
                        type="button"
                        onClick={() => this.decreaseCounter("adults")}
                      >
                        -
                      </button>
                    </Col>
                    <Col span={16}>
                      <input
                        type="text"
                        value={`${this.props.adults} ${adultos}`}
                        readOnly
                      />
                    </Col>
                    <Col span={4}>
                      <button
                        type="button"
                        onClick={() => this.increaseCounter("adults")}
                      >
                        +
                      </button>
                    </Col>
                  </Row>
                  <Row className="passenger__option-row">
                    <Col span={4}>
                      <button
                        type="button"
                        onClick={() => this.decreaseCounter("children")}
                      >
                        -
                      </button>
                    </Col>
                    <Col span={16}>
                      <input
                        type="text"
                        value={`${this.props.children} ${criancas}`}
                        readOnly
                      />
                    </Col>
                    <Col span={4}>
                      <button
                        type="button"
                        onClick={() => this.increaseCounter("children")}
                      >
                        +
                      </button>
                    </Col>
                  </Row>
                  <Row className="passenger__option-row">
                    <Col span={4}>
                      <button
                        type="button"
                        onClick={() => this.decreaseCounter("infants")}
                      >
                        -
                      </button>
                    </Col>
                    <Col span={16}>
                      <input
                        type="text"
                        value={`${this.props.infants} ${bebes}`}
                        readOnly
                      />
                    </Col>
                    <Col span={4}>
                      <button
                        type="button"
                        onClick={() => this.increaseCounter("infants")}
                      >
                        +
                      </button>
                    </Col>
                  </Row>
                  <Row className="passenger__option-row">
                    <Col span={24}>
                      <p className="text-politica">
                        <small>
                          <FormattedMessage id="buscaAereoPassageiroInfo" />
                        </small>
                      </p>
                    </Col>
                  </Row>
                  <Row className="passenger__option-row">
                    <Col span={24}>
                      <button
                        type="button"
                        className="passenger__option-row__close-btn"
                        onClick={() =>
                          this.setState({ showPassengerModal: false })
                        }
                      >
                        Atualizar
                      </button>
                    </Col>
                  </Row>
                </div>
              )}
            </div>
          </div>
        </FormItem>

        <FormItem className="field-full">
          <Button
            type="primary"
            htmlType="submit"
            className="btn-comparar"
            disabled={
              this.hasErrors(getFieldsError()) ||
              !isValidOrigin ||
              !isValidDestination
            }
          >
            Comparar
          </Button>
        </FormItem>
      </Form>
    );
  }
}

const WrappedFormSearch = Form.create()(FormSearch);

export default withRouter(
  connect(
    state => ({
      ...state.searchByAir
    }),
    dispatch =>
      bindActionCreators(
        {
          setType,
          autocomplete,
          setAirport,
          setDate,
          setAdditional
        },
        dispatch
      )
  )(WrappedFormSearch)
);
