import React, { Component } from "react";

import { Link } from "react-router-dom";
import MetaTags from "react-meta-tags";

import Header from "../../components/header";
import Breadcrumb from "../../components/breadcrumb";
import Footer from "../../components/footer";
import Container from "../../components/container";
import ContentText from "../../components/contentText";

import FormSearch from "./components/formSearch";
import { FormattedMessage } from "react-intl";

class SearchByAir extends Component {
  state = {};

  render() {
    return (
      <div className="page-aereo">
        <MetaTags>
          <meta
            name="title"
            content="Passagens Aéreas Baratas: compare as menores tarifas "
          />
          <meta
            name="description"
            content="Compare agora as tarifas de várias Cias Aéreas ao mesmo tempo: Gol, Azul, Latam, Avianca e mais. Viaje mais feliz!"
          />
          <meta
            name="keywords"
            content="passagens aéreas, passagens aéreas baratas"
          />
          {/* <meta property="og:title" content="MyApp" />
        <meta property="og:image" content="path/to/image.jpg" /> */}
        </MetaTags>

        <Breadcrumb>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            Vah <FormattedMessage id="menuAereo" />
          </li>
        </Breadcrumb>
        <Header />
        <div className="BannerSearch">
          <Container>
            <div className="BannerFormSearch">
              <FormSearch />
            </div>
          </Container>
        </div>
        <section className="main-info">
          <Container>
            <ContentText />
          </Container>
        </section>

        <Footer />
      </div>
    );
  }
}

export default SearchByAir;
