import React, { Fragment } from "react";
import { Row, Col } from "antd";
import styled from "styled-components";

import { FormattedMessage } from "react-intl";

import "./resultByAddress.css";

const Image = styled.img`
  max-width: 44px;
`;

const BoxResult = ({ item, appIcon, carDistance, isPromotion, appUrl }) => (
  <div className="itemResult app-result">
    <Row>
      <Col span={18}>
        <Row>
          <Col xs={8} md={4}>
            <Image src={appIcon} />
          </Col>
          <Col xs={16} md={20}>
            <Row>
              <p className="name-app">{item.product}</p>
              <p className="tempo-app">
                {carDistance ? (
                  <Fragment>
                    <FormattedMessage id="resultadoUrbanoChega" /> em{" "}
                    <strong>{carDistance} min.</strong>
                  </Fragment>
                ) : (
                  "Tempo indefinido"
                )}
              </p>
              {isPromotion && <p className="promo-app">{isPromotion.name}</p>}
            </Row>
          </Col>
        </Row>
      </Col>
      <Col xs={24} md={6}>
        <a className="ButtonSelect" target="_blank" href={appUrl}>
          <FormattedMessage id="resultadoUrbanoBtn" />
        </a>
      </Col>
    </Row>

    <div className="FooterResult">
      <p>
        {isPromotion && (
          <span className="Discount">-{isPromotion.discount}%</span>
        )}{" "}
        <span className="ValResult">{item.estimate}</span>
      </p>
    </div>
  </div>
);

export default BoxResult;
