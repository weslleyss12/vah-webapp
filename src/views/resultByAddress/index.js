import React, { Component } from "react";
import MetaTags from "react-meta-tags";
import styled from "styled-components";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Row, Col, Card } from "antd";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import { FormattedMessage } from "react-intl";

import Config from "../../config";

// Actions
import {
  searchUrban,
  setFilters,
  clearEstimates
} from "../searchByAddress/actions";

import AddressTransition from "../../components/transition";

import Breadcrumb from "../../components/breadcrumb";
import Header from "../../components/header";
import ResultHeader from "../../components/ResultHeader";
import Footer from "../../components/footer";
import Container from "../../components/container";
import SideBlue from "../../components/sideBlue";

import BoxResult from "./components/boxResult";

import "./components/resultByAddress.css";

import playerVah from "../../assets/images/icons/player_vah.jpg";
import playerUber from "../../assets/images/icons/player_uber.png";
import playerCabify from "../../assets/images/icons/player_cabify.png";
import playerYetgo from "../../assets/images/icons/player_yetgo.jpg";
import player99 from "../../assets/images/icons/player_99.png";

const MainInfo = styled.section`
  padding: 20px 0;
`;

const TripInfo = styled(Card)`
  font-size: 18px;
  font-weight: 700;
  line-height: 20.4px;
  text-transform: uppercase;
  letter-spacing: 0.41px;
  border-radius: 5px;
  margin-bottom: 20px;
  color: #244a88;
  font-family: "Gilroy-Bold";
  background-color: #e4e5ea;
`;

const RightCol = styled(Col)`
  text-align: right;
`;

class ResultByAddress extends Component {
  static propTypes = {
    match: PropTypes.shape().isRequired,
    searchUrban: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      orders: ["Preço", "Tempo de chegada"],
      order: props.filters.order || "Preço",
      companies: [],
      filteredCompanies: props.filters.companies || [],
      female: props.female || 0,
      hasTransition: true
    };
  }

  componentDidMount = () => {
    const {
      match: {
        params: {
          start_latitude,
          start_longitude,
          start_address,
          start_city,
          start_state,
          start_zip_code,
          start_country,
          end_latitude,
          end_longitude,
          end_address,
          end_city,
          end_state,
          end_zip_code,
          country,
          language,
          currency
        }
      }
    } = this.props;

    this.props
      .searchUrban({
        start_address,
        start_state,
        start_city,
        start_country,
        start_zip_code,
        start_latitude,
        start_longitude,

        end_address,
        end_state,
        end_city,
        end_zip_code,
        end_latitude,
        end_longitude,

        promotions: true,
        first_trip_promotions: true,
        country,
        language,
        currency,
        app_version: Config.APP_VERSION
      })
      .then(() => {
        const { pending, results } = this.props;

        if (!results || (results && results.estimates.length === 0 && !pending))
          return null;

        const { players } = results;
        const { filteredCompanies } = this.state;
        let companies = [];

        for (const key in players) {
          companies.push(key);
        }

        companies.sort();

        this.setState({
          ...this.state,
          companies,
          filteredCompanies:
            filteredCompanies.length > 0 ? filteredCompanies : companies
        });
      });
  };

  _handleOrderBy = order => {
    this.setState({ ...this.state, order });
  };

  _renderOrders = () => {
    const { orders, order } = this.state;

    return orders.map(o => (
      <span className="field-holder" key={o}>
        <div className="switch-containter">
          <div className="switch">
            <input
              id={o}
              className="cmn-toggle cmn-toggle-round"
              type="checkbox"
              checked={o === order}
              onChange={() => this._handleOrderBy(o)}
            />
            <label for={o} />
          </div>
          <span>
            {o == "Preço" ? (
              <FormattedMessage id="filtroUrbanoPreco" />
            ) : (
              <FormattedMessage id="filtroUrbanoTempo" />
            )}
          </span>
        </div>
      </span>
    ));
  };

  _handleCompanies = (company, e) => {
    const active = e.target.checked;
    const filteredCompanies = this.state.filteredCompanies.concat([]);

    if (active && filteredCompanies.indexOf(company) === -1) {
      filteredCompanies.push(company);
    } else if (filteredCompanies.indexOf(company) !== -1) {
      filteredCompanies.splice(filteredCompanies.indexOf(company), 1);
    }

    this.setState({ ...this.state, filteredCompanies });
  };

  _renderCompanies = () => {
    const { results } = this.props;
    const { companies, filteredCompanies } = this.state;

    return companies.map(company => (
      <span className="field-holder" key={company}>
        <div className="switch-containter">
          <div className="switch">
            <input
              id={results.players[company].name}
              className="cmn-toggle cmn-toggle-round"
              type="checkbox"
              checked={filteredCompanies.indexOf(company) !== -1}
              onChange={enable => this._handleCompanies(company, enable)}
            />
            <label for={results.players[company].name} />
          </div>
          <span>{results.players[company].name}</span>
        </div>
      </span>
    ));
  };

  _handleApply = e => {
    e.preventDefault();

    const { setFilters, filteredFemale, clearEstimates } = this.props;
    const { order, companies, filteredCompanies, female } = this.state;

    setTimeout(
      () => {
        if (
          order === "Preço" &&
          companies.length === filteredCompanies.length
        ) {
          if (filteredFemale !== female) {
            setFilters({
              female: female ? 1 : 0
            });
          } else {
            setFilters({});
          }
        } else {
          setFilters({
            order,
            companies: filteredCompanies,
            female: female ? 1 : 0
          });
        }
      },
      500,
      this.child.closeSide()
    );

    clearEstimates();
  };

  _renderItems = () => {
    // TODO: Verificar necessidade do uso da propriedade `hasPlayers` do codigo original
    const { results } = this.props;

    const checkedEstimates = results.estimates.filter(
      item =>
        !item.promotion || (item.promotion && item.promotion.first_trip === 0)
    );

    return checkedEstimates.map((item, index) => {
      let appIcon = null;
      let appUrl = null;
      const carDistance = item.time / 60;
      const isPromotion = item.promotion;
      const player = results.players[item.player];

      // console.log("item", results.players);

      // console.log("PLAYER", item.player);

      switch (item.player) {
        case "uber":
          appIcon = playerUber;
          appUrl = "http://onelink.to/yxz6j8";
          break;

        case "cabify":
          appIcon = playerCabify;
          appUrl = "http://onelink.to/amkggv";
          break;

        case "yetgo":
          appIcon = playerYetgo;
          appUrl = "http://onelink.to/6y3pzc";
          break;

        case "99":
          appIcon = player99;
          appUrl = "http://onelink.to/dut34q";
          break;

        case "easy-taxi":
          appIcon = player.icon;
          appUrl = "http://onelink.to/yacgjf";
          break;

        case "wappa":
          appIcon = player.icon;
          appUrl = "http://onelink.to/4tnx42";
          break;

        case "taxirio":
          appIcon = player.icon;
          appUrl = "http://onelink.to/4e4uss";
          break;

        case "coopertaxi":
          appIcon = player.icon;
          appUrl = "http://onelink.to/srepqp";
          break;

        case "sp-taxi":
          appIcon = player.icon;
          appUrl = "http://onelink.to/7uqe7g";
          break;

        case "taxify":
          appIcon = player.icon;
          appUrl = "http://onelink.to/3ffz5q";
          break;

        default:
          appIcon = player.icon ? player.icon : playerVah;
      }

      return (
        <BoxResult
          key={index}
          item={item}
          appUrl={appUrl}
          appIcon={appIcon}
          carDistance={carDistance}
          isPromotion={isPromotion}
        />
      );
    });
  };

  toggleTransition = () =>
    this.setState(({ hasTransition }) => ({ hasTransition: !hasTransition }));

  render() {
    const { pending, results, match } = this.props;

    if (results === null || pending)
      return (
        <AddressTransition
          results={results}
          pending={pending}
          toggleTransition={this.toggleTransition}
          activeTab="urbano"
        />
      );

    return (
      <div className="page-urbano">
        <MetaTags>
          <meta
            name="title"
            content="Melhores preços e cupom de desconto Uber, 99, Cabify e mais | Vah"
          />
          <meta
            name="description"
            content="Confira agora as tarifas e cupons de desconto válidos para Uber, táxi, 99, Cabify e Ladydriver. Compare todos ao mesmo tempo e viaje mais feliz!"
          />
          <meta
            name="keywords"
            content="cupom de desconto uber, cupom de desconto 99, cupom de desconto cabify"
          />
          {/* <meta property="og:title" content="MyApp" />
          <meta property="og:image" content="path/to/image.jpg" /> */}
        </MetaTags>
        <Breadcrumb>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/aplicativo-de-transporte">
              Vah <FormattedMessage id="menuUrbano" />{" "}
            </Link>
          </li>
          <li>Resultado</li>
        </Breadcrumb>
        <Header />
        <ResultHeader
          infos={match.params}
          backUrl="/aplicativo-de-transporte"
        />
        <MainInfo>
          <Container>
            <Row gutter={60}>
              <Col md={24} lg={17} className="no-barra">
                {results && (
                  <TripInfo>
                    <Row>
                      <Col span={12}>
                        <FormattedMessage id="resultadoUrbanoDisctancia" />
                        <br />
                        {results.trip.distance} KM
                      </Col>
                      <RightCol span={12}>
                        <FormattedMessage id="resultadoUrbanoDuracao" />
                        <br />
                        {results.trip.duration
                          ? Math.ceil(results.trip.duration / 60)
                          : "-"}{" "}
                        Minutos
                      </RightCol>
                    </Row>
                  </TripInfo>
                )}
              </Col>
              <Col md={24} lg={7}>
                <SideBlue onRef={ref => (this.child = ref)}>
                  <h2 className="title-filter">
                    <FormattedMessage id="filtroUrbanoTitle" />
                  </h2>
                  <form className="form-filter" onSubmit={this._handleApply}>
                    <fieldset>
                      <legend className="filter-type">
                        <FormattedMessage id="filtroUrbanoOrdenar" />
                      </legend>
                      <div>{this._renderOrders()}</div>
                    </fieldset>
                    <fieldset>
                      <legend className="filter-type">
                        <FormattedMessage id="filtroUrbanoEmpresas" />
                      </legend>
                      <div>
                        {this.state.companies.length > 0 &&
                          this._renderCompanies()}
                      </div>
                    </fieldset>
                    <div>
                      <button type="submit" className="apply-filter">
                        <FormattedMessage id="filtroUrbanoBtn" />
                      </button>
                    </div>
                  </form>
                </SideBlue>
              </Col>
              <Col md={24} lg={17}>
                {results && (
                  <TripInfo>
                    <Row>
                      <Col span={12}>
                        <FormattedMessage id="resultadoUrbanoDisctancia" />
                        <br />
                        {results.trip.distance} KM
                      </Col>
                      <RightCol span={12}>
                        <FormattedMessage id="resultadoUrbanoDuracao" />
                        <br />
                        {results.trip.duration
                          ? Math.ceil(results.trip.duration / 60)
                          : "-"}{" "}
                        Minutos
                      </RightCol>
                    </Row>
                  </TripInfo>
                )}

                {results &&
                  results.estimates.length === 0 &&
                  !pending && <p>Nenhum resultado encontrado</p>}

                {results &&
                  results.estimates.length > 0 &&
                  !pending &&
                  this._renderItems()}
              </Col>
            </Row>
          </Container>
        </MainInfo>
        <Footer />
      </div>
    );
  }
}

export default connect(
  state => ({ ...state.searchByAddress }),
  dispatch =>
    bindActionCreators({ searchUrban, setFilters, clearEstimates }, dispatch)
)(ResultByAddress);
