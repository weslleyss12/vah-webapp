import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import MetaTags from "react-meta-tags";

import { Row, Col, List } from "antd";
import styled from "styled-components";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import Breadcrumb from "../../components/breadcrumb";
import Header from "../../components/header";
import InfoHeader from "./components/InfoHeader";
import Footer from "../../components/footer";
import Container from "../../components/container";
import SideBlue from "../../components/sideBlue";
import BoxResult from "./components/boxResult";

import Config from "../../config";

import AddressTransition from "../../components/transition";

import { FormattedMessage } from "react-intl";

// Actions
import {
  searchRent,
  setFilters,
  clearEstimates
} from "../searchByRent/actions";

const MainInfo = styled.section`
  padding: 20px 0;
`;

const Form = styled.form`
  &.form-filter {
    padding-bottom: 10px;

    fieldset:nth-child(3) {
      margin-bottom: 10px;
    }
  }
`;

class ResultByRent extends Component {
  static propTypes = {
    match: PropTypes.shape().isRequired,
    searchRent: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      companies: [],
      filteredCompanies: props.filters.companies || [],
      categories: [],
      filteredCategories: props.filters.categories || [],
      setup: [],
      filteredSetup: props.filters.setup || []
    };
  }

  componentDidMount = () => {
    const {
      match: {
        params: {
          origin,
          origin_description,
          origin_city,
          origin_state,
          origin_date,
          origin_time,
          return: return_place,
          return_description,
          return_city,
          return_state,
          return_date,
          return_time
        }
      }
    } = this.props;

    this.props
      .searchRent({
        origin,
        origin_description,
        origin_city,
        origin_state,
        origin_date: `${origin_date} ${origin_time}:00`,
        return: return_place,
        return_description,
        return_city,
        return_state,
        return_date: `${return_date} ${return_time}:00`,
        app_version: Config.APP_VERSION
      })
      .then(() => {
        const { pending, full_estimates: estimates } = this.props;
        const {
          filteredCompanies,
          filteredCategories,
          filteredSetup
        } = this.state;
        let companies = [];
        let categories = [];
        let setup = [];

        if (pending || (estimates && estimates.length === 0 && !pending))
          return null;

        estimates.map(estimate => {
          if (companies.indexOf(estimate.flag.name) === -1) {
            companies.push(estimate.flag.name);
          }

          if (categories.indexOf(estimate.group.category) === -1) {
            categories.push(estimate.group.category);
          }

          if (setup.indexOf(`${estimate.group.setup.doors} Portas`) === -1) {
            setup.push(`${estimate.group.setup.doors} Portas`);
          }
          if (
            setup.indexOf("Ar-condicionado") === -1 &&
            estimate.group.setup.ac
          ) {
            setup.push("Ar-condicionado");
          }
          if (
            estimate.group.setup.transmission === "Manual" &&
            setup.indexOf("Manual") === -1
          ) {
            setup.push("Manual");
          }
          if (
            estimate.group.setup.transmission === "Automático" &&
            setup.indexOf("Automático") === -1
          ) {
            setup.push("Automático");
          }

          return estimates;
        });

        companies.sort();
        categories.sort();
        setup.sort();

        this.setState({
          ...this.state,
          companies,
          categories,
          setup,
          filteredCompanies:
            filteredCompanies.length > 0 ? filteredCompanies : companies,
          filteredCategories:
            filteredCategories.length > 0 ? filteredCategories : categories,
          filteredSetup: filteredSetup.length > 0 ? filteredSetup : setup
        });
      });
  };

  _handleCategories = (category, e) => {
    const active = e.target.checked;
    const filteredCategories = this.state.filteredCategories.concat([]);

    if (active && filteredCategories.indexOf(category) === -1) {
      filteredCategories.push(category);
    } else if (filteredCategories.indexOf(category) !== -1) {
      filteredCategories.splice(filteredCategories.indexOf(category), 1);
    }

    this.setState({ ...this.state, filteredCategories });
  };

  _renderCategories = () => {
    const { categories, filteredCategories } = this.state;

    return categories.map(category => (
      <span className="field-holder" key={category}>
        <div className="switch-containter">
          <div className="switch">
            <input
              id={category}
              className="cmn-toggle cmn-toggle-round"
              type="checkbox"
              checked={filteredCategories.indexOf(category) !== -1}
              onChange={enable => this._handleCategories(category, enable)}
            />
            <label for={category} />
          </div>
          <span>{category}</span>
        </div>
      </span>
    ));
  };

  _handleCompanies = (company, e) => {
    const active = e.target.checked;
    const filteredCompanies = this.state.filteredCompanies.concat([]);

    if (active && filteredCompanies.indexOf(company) === -1) {
      filteredCompanies.push(company);
    } else if (filteredCompanies.indexOf(company) !== -1) {
      filteredCompanies.splice(filteredCompanies.indexOf(company), 1);
    }

    this.setState({ ...this.state, filteredCompanies });
  };

  _renderCompanies = () => {
    const { companies, filteredCompanies } = this.state;

    return companies.map(company => (
      <span className="field-holder" key={company}>
        <div className="switch-containter">
          <div className="switch">
            <input
              id={company}
              className="cmn-toggle cmn-toggle-round"
              type="checkbox"
              checked={filteredCompanies.indexOf(company) !== -1}
              onChange={enable => this._handleCompanies(company, enable)}
            />
            <label for={company} />
          </div>
          <span>{company}</span>
        </div>
      </span>
    ));
  };

  _handleSetup = (c, e) => {
    const active = e.target.checked;
    const filteredSetup = this.state.filteredSetup.concat([]);

    if (active && filteredSetup.indexOf(c) === -1) {
      filteredSetup.push(c);
    } else if (filteredSetup.indexOf(c) !== -1) {
      filteredSetup.splice(filteredSetup.indexOf(c), 1);
    }

    this.setState({ ...this.state, filteredSetup });
  };

  _renderSetup = () => {
    const { setup, filteredSetup } = this.state;
    return setup.map(item => (
      <span className="field-holder" key={item}>
        <div className="switch-containter">
          <div className="switch">
            <input
              id={item}
              className="cmn-toggle cmn-toggle-round"
              type="checkbox"
              checked={filteredSetup.indexOf(item) !== -1}
              onChange={enable => this._handleSetup(item, enable)}
            />
            <label for={item} />
          </div>
          <span>{item.replace(" or ", " ou ")}</span>
        </div>
      </span>
    ));
  };

  _handleApply = e => {
    e.preventDefault();

    const { setFilters, clearEstimates } = this.props;
    const {
      companies,
      filteredCompanies,
      categories,
      filteredCategories,
      filteredSetup,
      setup
    } = this.state;

    setTimeout(
      () => {
        if (
          companies.length === filteredCompanies.length &&
          categories.length === filteredCategories.length &&
          setup.length === filteredSetup.length
        ) {
          setFilters({});
        } else {
          setFilters({
            companies: filteredCompanies,
            categories: filteredCategories,
            setup: setup.length === filteredSetup.length ? null : filteredSetup
          });
        }
      },
      500,
      this.child.closeSide()
    );

    clearEstimates();
  };

  render() {
    const { results, pending, match } = this.props;

    if (results === null || pending || this.state.hasTransition)
      return (
        <AddressTransition
          results={results}
          pending={pending}
          toggleTransition={this.toggleTransition}
          activeTab="aluguel"
        />
      );

    return (
      <div className="page-locacao">
        <MetaTags>
          <meta
            name="title"
            content="Compare para Alugar Carro com as menores tarifas | Vah Rent a Car"
          />
          <meta
            name="description"
            content="Para alugar carro com a melhor tarifa, compare preços das maioras locadoras de veículos. Veja preços da Localiza, Movida, Hertz, Avis Rent a Car e mais!"
          />
          <meta
            name="keywords"
            content="rent a car, aluguel de carros, locadora de veiculos"
          />
          {/* <meta property="og:title" content="MyApp" />
          <meta property="og:image" content="path/to/image.jpg" /> */}
        </MetaTags>
        <Breadcrumb>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/aluguel-de-carros">
              Vah <FormattedMessage id="menuLocacao" />
            </Link>
          </li>
          <li>Resultado</li>
        </Breadcrumb>
        <Header />
        <InfoHeader infos={match.params} backUrl="/aluguel-de-carros" />

        <MainInfo>
          <Container>
            <Row gutter={60}>
              <Col md={24} lg={7}>
                <SideBlue onRef={ref => (this.child = ref)}>
                  <h2 className="title-filter">
                    <FormattedMessage id="filtroLocacaoTitle" />
                  </h2>
                  <Form className="form-filter" onSubmit={this._handleApply}>
                    <fieldset>
                      <legend className="filter-type">
                        <FormattedMessage id="filtroLocacaoCategoria" />
                      </legend>
                      {this._renderCategories()}
                    </fieldset>
                    <fieldset>
                      <legend className="filter-type">
                        <FormattedMessage id="filtroLocacaoEmpresas" />
                      </legend>
                      {this._renderCompanies()}
                    </fieldset>
                    <fieldset>
                      <legend className="filter-type">
                        <FormattedMessage id="filtroLocacaoOpcionais" />
                      </legend>
                      {this._renderSetup()}
                    </fieldset>
                    <div>
                      <button className="apply-filter">Aplicar filtros</button>
                    </div>
                  </Form>
                </SideBlue>
              </Col>
              <Col md={24} lg={17}>
                {(results === null || pending) && (
                  <p>Estamos pesquisando as melhores tarifas para você.</p>
                )}

                {results &&
                  results.estimates.length === 0 &&
                  !pending && <p>Nenhum resultado encontrado</p>}

                {results &&
                  results.estimates.length > 0 &&
                  !pending && (
                    <List
                      dataSource={results.estimates}
                      renderItem={item => (
                        <BoxResult item={item} results={results} />
                      )}
                    />
                  )}
              </Col>
            </Row>
          </Container>
        </MainInfo>
        <Footer />
      </div>
    );
  }
}

export default connect(
  state => ({ ...state.searchByRent }),
  dispatch =>
    bindActionCreators(
      {
        searchRent,
        setFilters,
        clearEstimates
      },
      dispatch
    )
)(ResultByRent);
