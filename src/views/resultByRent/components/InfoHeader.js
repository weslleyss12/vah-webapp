import React from "react";
import { Row, Col } from "antd";
import { Link } from "react-router-dom";
import moment from "moment";
import { FormattedMessage } from "react-intl";
import Container from "../../../components/container";

const InfoHeader = ({ infos, backUrl }) => (
  <div className="info-wrapper">
    <Container>
      <Row type="flex" justify="center" align="middle">
        <Col xs={0} md={5}>
          <Link className="ButtonBack" to={backUrl}>
            <FormattedMessage id="geralBtnVoltar" />
          </Link>
        </Col>

        <Col xs={15} md={12}>
          <div className="InfosAddress">
            <p className="AddressStart">
              {`
                  ${decodeURIComponent(infos.origin_city)} -
                  ${decodeURIComponent(infos.origin_state)}
                `}
              <span>
                {moment(infos.origin_date).format("DD MMM")} {infos.origin_time}:00
              </span>
            </p>
            <p className="AddressEnd">
              {`
                  ${decodeURIComponent(infos.return_city)} -
                  ${decodeURIComponent(infos.return_state)}
                `}
              <span>
                {moment(infos.return_date).format("DD MMM")} {infos.return_time}:00
              </span>
            </p>
          </div>
        </Col>

        <Col className="EditCol" xs={9} md={7}>
          <Link className="ButtonEdit" to={backUrl}>
            <FormattedMessage id="geralBtnRefazer" />
          </Link>
        </Col>
      </Row>
    </Container>
  </div>
);

export default InfoHeader;
