import React from "react";
import CurrencyFormat from "react-currency-format";
import { Row, Col } from "antd";
import styled from "styled-components";

import icoPorta from "../../../assets/images/locacao/door.png";
import icoPessoas from "../../../assets/images/locacao/passenger.png";

import { FormattedMessage } from "react-intl";

import "./resultByRent.css";

function getMoney(str) {
  return parseInt(str.replace(/[\D]+/g, ""));
}
function formatReal(int) {
  var tmp = int + "";
  tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
  if (tmp.length > 6) tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");

  return tmp;
}

const Image = styled.img`
  width: 100%;
`;

const BoxResult = ({ item, results }) => {
  let vstr = getMoney(item.estimate);

  let valor = formatReal(vstr);

  return (
    <div className="itemResult carro-result">
      <Row>
        <Col xs={24} md={18}>
          <Row>
            <Col span={24}>
              <Row>
                <p className="categoria">{item.group.category}</p>
                <p className="marca">
                  {item.group.vehicle.replace(" or ", " ou ")}
                </p>

                <div className="itens-carro">
                  <p>
                    <span className="icoItem">
                      <img src={icoPorta} />
                    </span>{" "}
                    {item.group.setup.doors.replace(" or ", " ou ")}{" "}
                    <FormattedMessage id="resultadoLocacaoPortas" />
                  </p>
                  <p>
                    <span className="icoItem">
                      <img src={icoPessoas} />
                    </span>{" "}
                    {item.group.setup.passengers}{" "}
                    <FormattedMessage id="resultadoLocacaoPassageiros" />
                  </p>
                  <p>
                    {item.group.setup.transmission == "Manual"
                      ? "Manual"
                      : "Automático"}
                  </p>
                </div>
              </Row>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={6}>
          <Image src={item.group.image} className="img-car" />
          <a className="ButtonSelect" target="_blank" href={item.deeplink}>
            <FormattedMessage id="resultadoLocacaoBtn" />
          </a>
        </Col>
      </Row>

      <div className="FooterResult">
        <span className="imgrent">
          <img src={item.flag.image} />
        </span>
        <p>
          via {item.flag.name} <span className="ValResult">R${valor}</span>
        </p>
      </div>
    </div>
  );
};

export default BoxResult;
