import React from 'react';

import './resultByRent.css';
import carro from '../../../assets/images/core/carro.png';

import { Row, Col } from 'antd';

const BoxResult = () => (
  <div className="itemResult carro-result">
    <infosResult>
      <Row>
        <Col span={18}>
          <Row>
            <Col span={24}>
              <Row>
                <p className="categoria">econômico</p>
                <p className="marca">fiat argo ou similar</p>

                <div className="itens-carro">
                  <p>
                    <span>ico</span> 2-3 portas
                  </p>
                  <p>
                    <span>ico</span> 4 passageiros
                  </p>
                  <p>
                    <span>ico</span> Ar-condicionado
                  </p>
                </div>
              </Row>
            </Col>
          </Row>
        </Col>
        <Col span={6}>
          <img src={carro} />
          <a className="ButtonSelect" href="#">
            Selecionar
          </a>
        </Col>
      </Row>
    </infosResult>

    <div className="FooterResult">
      <p>
        via Kiwi.com <span className="ValResult">R$ 105</span>
      </p>
    </div>
  </div>
);

export default BoxResult;
