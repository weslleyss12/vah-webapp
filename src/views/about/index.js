import React, { Component } from "react";
import { Link } from "react-router-dom";
import MetaTags from "react-meta-tags";

import Container from "../../components/container";

import Header from "../../components/header";
import Footer from "../../components/footer";
import Breadcrumb from "../../components/breadcrumb";
import logoVah from "../../assets/images/core/logo-transition.png";

class About extends Component {
  state = {};
  render() {
    return (
      <div className="page-sobre">
        <MetaTags>
          <meta
            name="title"
            content="Quem é o Vah e como podemos te ajudar | Vah"
          />
          <meta
            name="description"
            content="Conheça mais sobre o Vah, comparador de preços de serviços de transporte. Economize se locomovendo na sua cidade ou em suas viagens! "
          />
          <meta name="keywords" content="Vah, aplicativo Vah" />
          {/* <meta property="og:title" content="MyApp" />
          <meta property="og:image" content="path/to/image.jpg" /> */}
        </MetaTags>
        <Breadcrumb>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>Sobre</li>
        </Breadcrumb>
        <Header />
        <Container>
          <div className="container-sobre">
            <p className="logo-transition">
              <img src={logoVah} width="120" alt="VAH" />
            </p>
            <h1>Vah, comparador de transportes</h1>
            <p>
              O VAH é uma startup criada por amigos de infância que resolveram
              se unir e fazer a melhor e mais abrangente ferramenta de
              comparação de preços de transporte do mercado. Seja para chamar um
              taxi da sua casa ao trabalho, seja para comparar passagens aéreas
              das suas próximas férias, comparar preços de locação de veículos
              para uma viagem de negócios ou mesmo para passagens rodoviárias.
              Nós do VAH criamos um motor de busca (metbuscador) e exibição de
              resultados ultradinâmico fazendo com que você tenha a melhor
              escolha à um clique de distância. Tudo isso para que no final você
              consiga economizar duas coisas muito valiozas: seu tempo e seu
              dinheiro.
            </p>
          </div>
        </Container>
        <Footer />
      </div>
    );
  }
}

export default About;
