import React from 'react';
import styled from 'styled-components';

import { Row, Col } from 'antd';

const TextGray = styled.p`
  opacity: 0.6;
  color: #595959;
  font-size: 14px;
  font-weight: 400;
  text-transform: uppercase;
  font-family: 'Gilroy-SemiBold';
  letter-spacing: 0.37px;
  margin: 0 0 -5px;
  padding: 0;
`;

const TextBlue = styled.p`
  color: #244a88;
  font-size: 20px;
  font-family: 'Gilroy-Medium';
  font-weight: 500;
  letter-spacing: 0.22px;
  margin: 0;
  padding: 0;

  small {
    font-size: 14px;
    text-transform: uppercase;
    font-weight: 700;
  }
`;

const ItemResult = styled.div`
  background: #f8f8f8;
  box-sizing: border-box;
  padding: 20px 20px 5px 20px;
  margin-bottom: 20px;
`;

const FooterResult = styled.div`
  border-top: 1px solid #efefef;
  padding: 5px 0 0;
  text-align: right;
  color: #595959;
  font-size: 14px;
  font-weight: 400;
  letter-spacing: 0.37px;
  margin-top: 10px;
  font-family: 'Gilroy-SemiBold';
`;

const ButtonSelect = styled.a`
  display: block;
  background: #77eb68;
  height: 32px;
  text-align: center;
  text-transform: uppercase;
  line-height: 32px;
  border-radius: 5px;
  margin-top: 10px;
`;

const ValResult = styled.span`
  color: #244a88;
  font-size: 27px;
  font-weight: 500;
  text-transform: uppercase;
  letter-spacing: 0.29px;
  font-family: 'Gilroy-Medium';
`;

const DescriptionMini = styled(Col)`
  opacity: 0.6;
  color: #595959;
  font-family: 'Gilroy-SemiBold';
  font-size: 14px;
  font-weight: 500;
  line-height: 18.48px;
  font-weight: 500;
  letter-spacing: 0.37px;
  margin-top: 5px;

  span {
    display: block;
  }
`;

const Divisor = styled(Col)`
  border-bottom: dotted 1px #ccc;
  margin: 20px 0;
`;

const RowInfo = styled(Row)``;

const BoxResultSimple = () => (
  <ItemResult>
    <infosResult>
      <Row>
        <Col span={18}>
          <RowInfo>
            <Col span={4}>Selos</Col>
            <Col span={20}>
              <Row>
                <Col span={8}>
                  <TextGray>Partida</TextGray>
                  <TextBlue>
                    19h10 <small>(Gru)</small>
                  </TextBlue>
                </Col>
                <Col span={8}>
                  <TextGray>Chegada</TextGray>
                  <TextBlue>
                    19h10 <small>(MIA)</small>
                  </TextBlue>
                </Col>
                <Col span={8}>
                  <TextGray>&nbsp;</TextGray>
                  <TextBlue>
                    <small>2 escalas</small>
                  </TextBlue>
                </Col>
                <DescriptionMini span={24}>
                  Latam Airlines + Várias Linhas Aéreas{' '}
                  <span>Total: 20h32</span>
                </DescriptionMini>
              </Row>
            </Col>
          </RowInfo>
        </Col>
        <Col span={6}>
          <ButtonSelect href="#">Selecionar</ButtonSelect>
        </Col>
      </Row>
    </infosResult>

    <FooterResult>
      <p>
        via Kiwi.com <ValResult>R$ 3.704</ValResult>
      </p>
    </FooterResult>
  </ItemResult>
);

export default BoxResultSimple;
