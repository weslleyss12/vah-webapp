import React, { Component } from "react";
import { Row, Col, Slider } from "antd";

import { FormattedMessage } from "react-intl";

import SideBlue from "../../../components/sideBlue";

class Filter extends Component {
  constructor(props) {
    super(props);

    const timeRange = ["Manhã", "Tarde", "Noite"];

    this.state = {
      hour: timeRange,
      filteredStops: props.filteredStops || [],
      filteredCompanies: props.filteredCompanies || [],
      filteredOutboundHour: props.filteredOutboundHour || timeRange,
      filteredInboundHour: props.filteredInboundHour || timeRange,
      price: props.price || 0
    };
  }

  componentWillReceiveProps = ({
    filteredStops,
    filteredCompanies,
    filteredOutboundHour,
    filteredInboundHour,
    price
  }) => {
    this.setState({
      filteredStops,
      filteredCompanies,
      filteredOutboundHour,
      filteredInboundHour,
      price
    });
  };

  handleStops = (stop, e) => {
    const active = e.target.checked;
    const filteredStops = this.state.filteredStops.concat([]);

    if (active && filteredStops.indexOf(stop) === -1) {
      filteredStops.push(stop);
    } else if (filteredStops.indexOf(stop) !== -1) {
      filteredStops.splice(filteredStops.indexOf(stop), 1);
    }

    this.setState({ filteredStops });
  };

  renderStops = () => {
    const { filteredStops } = this.state;
    const { stops } = this.props;

    return stops.map(stop => (
      <span className="field-holder" key={stop}>
        <div className="switch-containter">
          <div className="switch">
            <input
              id={stop}
              className="cmn-toggle cmn-toggle-round"
              type="checkbox"
              checked={filteredStops.indexOf(stop) !== -1}
              onChange={enable => this.handleStops(stop, enable)}
            />
            <label for={stop} />
          </div>
          <span>{stop}</span>
        </div>
      </span>
    ));
  };

  handleCompanies = (company, e) => {
    const active = e.target.checked;
    let filteredCompanies = this.state.filteredCompanies.concat([]);

    if (active && filteredCompanies.indexOf(company) === -1) {
      filteredCompanies.push(company);
    } else if (filteredCompanies.indexOf(company) !== -1) {
      filteredCompanies.splice(filteredCompanies.indexOf(company), 1);
    }

    this.setState({ filteredCompanies });
  };

  renderCompanies = () => {
    const { filteredCompanies } = this.state;
    const { companies } = this.props;

    return companies.map(company => (
      <span className="field-holder" key={company}>
        <div className="switch-containter">
          <div className="switch">
            <input
              id={company}
              className="cmn-toggle cmn-toggle-round"
              type="checkbox"
              checked={filteredCompanies.indexOf(company) !== -1}
              onChange={enable => this.handleCompanies(company, enable)}
            />
            <label for={company} />
          </div>
          <span>{company}</span>
        </div>
      </span>
    ));
  };

  handlePrice = price => {
    this.setState({ price: parseInt(price) });
  };

  handleOutboundHour = (hour, e) => {
    const active = e.target.checked;
    const filteredOutboundHour = this.state.filteredOutboundHour.concat([]);

    if (active && filteredOutboundHour.indexOf(hour) === -1) {
      filteredOutboundHour.push(hour);
    } else if (filteredOutboundHour.indexOf(hour) !== -1) {
      filteredOutboundHour.splice(filteredOutboundHour.indexOf(hour), 1);
    }

    this.setState({ filteredOutboundHour });
  };

  renderOutboundTimeRange = () => {
    const { hour, filteredOutboundHour } = this.state;

    return hour.map(time => (
      <span className="field-holder" key={time}>
        <div className="switch-containter">
          <div className="switch">
            <input
              id={time}
              className="cmn-toggle cmn-toggle-round"
              type="checkbox"
              checked={filteredOutboundHour.indexOf(time) !== -1}
              onChange={enable => this.handleOutboundHour(time, enable)}
            />
            <label for={time} />
          </div>
          <span>{time}</span>
        </div>
      </span>
    ));
  };

  handleInboundHour = (hour, e) => {
    const active = e.target.checked;
    const filteredInboundHour = this.state.filteredInboundHour.concat([]);

    if (active && filteredInboundHour.indexOf(hour) === -1) {
      filteredInboundHour.push(hour);
    } else if (filteredInboundHour.indexOf(hour) !== -1) {
      filteredInboundHour.splice(filteredInboundHour.indexOf(hour), 1);
    }

    this.setState({ filteredInboundHour });
  };

  renderInboundTimeRange = () => {
    const { hour, filteredInboundHour } = this.state;

    return hour.map(time => (
      <span className="field-holder" key={`inbound-${time}`}>
        <div className="switch-containter">
          <div className="switch">
            <input
              id={`inbound-${time}`}
              className="cmn-toggle cmn-toggle-round"
              type="checkbox"
              checked={filteredInboundHour.indexOf(time) !== -1}
              onChange={enable => this.handleInboundHour(time, enable)}
            />
            <label for={`inbound-${time}`} />
          </div>
          <span>{time}</span>
        </div>
      </span>
    ));
  };

  applyFilter = e => {
    e.preventDefault();

    const {
      hour,
      filteredStops,
      filteredCompanies,
      filteredOutboundHour,
      filteredInboundHour,
      price
    } = this.state;

    this.props.onApply({
      hour,
      filteredStops,
      filteredCompanies,
      filteredOutboundHour,
      filteredInboundHour,
      price
    });

    this.child.closeSide();
  };

  render() {
    const { price } = this.state;
    const { stops, companies, results, filters, type } = this.props;

    return (
      <SideBlue onRef={ref => (this.child = ref)}>
        <h2 className="title-filter">
          <FormattedMessage id="filtroAereoTitle" />{" "}
        </h2>

        <form className="form-filter" onSubmit={this.applyFilter}>
          {/* ESCALAS */}
          <fieldset>
            <legend className="filter-type">
              <FormattedMessage id="filtroAereoEscalas" />
            </legend>
            <div>{stops.length > 0 && this.renderStops()}</div>
          </fieldset>

          {/* EMPRESAS */}
          <fieldset>
            <legend className="filter-type">
              <FormattedMessage id="filtroAereoEmpresas" />
            </legend>
            <div>{companies.length > 0 && this.renderCompanies()}</div>
          </fieldset>

          {/* PREÇO */}
          <fieldset>
            <legend className="filter-type">
              <FormattedMessage id="filtroAereoPreco" />
            </legend>
            {results &&
              results.price_range && (
                <div className="price-wrapper">
                  <Slider
                    className="slide-side"
                    min={results.price_range.min}
                    max={results.price_range.max}
                    defaultValue={filters.price || results.price_range.max}
                    value={price || results.price_range.max}
                    tipFormatter={value => `R$ ${value}`}
                    onChange={this.handlePrice}
                  />

                  <Row>
                    <Col span={12}>
                      <span className="price-text">
                        R$ {results.price_range.min}
                      </span>
                    </Col>

                    <Col className="text-right" span={12}>
                      <span className="price-right">
                        R$ {results.price_range.max}
                      </span>
                    </Col>
                  </Row>
                </div>
              )}
          </fieldset>

          {/* PERÍODO IDA */}
          <fieldset>
            <legend className="filter-type">
              <FormattedMessage id="filtroAereoIda" />
            </legend>
            <div>{this.renderOutboundTimeRange()}</div>
          </fieldset>

          {/* PERÍODO VOLTA */}
          {type === "round_trip" && (
            <fieldset>
              <legend className="filter-type">
                <FormattedMessage id="filtroAereoVolta" />
              </legend>
              <div>{this.renderInboundTimeRange()}</div>
            </fieldset>
          )}

          {/* ACTION BUTTON */}
          <Row>
            {/* <Col span={12}>
              <button
                className="apply-filter"
                type="button"
                onClick={this.props.onRedefine}
              >
                REDEFINIR
              </button>
            </Col> */}
            <Col span={24}>
              <button className="apply-filter" type="submit">
                <FormattedMessage id="filtroAereoBtn" />
              </button>
            </Col>
          </Row>
        </form>
      </SideBlue>
    );
  }
}

export default Filter;
