import React from "react";
import { Row, Col } from "antd";
import { Link } from "react-router-dom";
import moment from "moment";

import { FormattedMessage } from "react-intl";

import Container from "../../../components/container";

import iconAirplane from "../../../assets/images/icons/ico-airplane.svg";
import iconAirplaneBack from "../../../assets/images/icons/ico-airplane-back.svg";

const InfoHeader = ({ infos, backUrl }) => (
  <div className="info-wrapper">
    <Container>
      <Row type="flex" justify="center" align="middle">
        <Col xs={0} md={5}>
          <Link className="ButtonBack" to={backUrl}>
            <FormattedMessage id="geralBtnVoltar" />
          </Link>
        </Col>

        <Col className="AirportWrapper" xs={15} md={12}>
          <p className="Airport">{infos.origin}</p>
          {infos.return_date ? (
            <img className="Airplane" src={iconAirplaneBack} alt="" />
          ) : (
            <img className="Airplane" src={iconAirplane} alt="" />
          )}
          <p className="Airport">{infos.destination}</p>
          <p className="Passengers">
            {moment(infos.go_date).format("DD MMM")}{" "}
            {infos.return_date &&
              `- ${moment(infos.return_date).format("DD MMM")}`}{" "}
            <span className="number">
              {`
              ${parseInt(infos.adults, 10) +
                parseInt(infos.children, 10) +
                parseInt(infos.infants, 10)}
            `}
              <FormattedMessage id="resultadoAereoPassageiro" />(s)
            </span>
          </p>
        </Col>

        <Col className="EditCol" xs={9} md={7}>
          <Link className="ButtonEdit" to={backUrl}>
            <FormattedMessage id="geralBtnRefazer" />
          </Link>
        </Col>
      </Row>
    </Container>
  </div>
);

export default InfoHeader;
