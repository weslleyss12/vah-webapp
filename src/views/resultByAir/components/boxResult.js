import React, { PureComponent } from "react";
import moment from "moment";
import { Row, Col } from "antd";
import { FormattedMessage } from "react-intl";

import "./resultByAir.css";

// Helpers
import { minutesInHours } from "../../../utils/helpers";

// Assets
import iconAirplaneBack from "../../../assets/images/icons/ico_airplane_back.png";
import iconAirplane from "../../../assets/images/icons/ico_airplane.png";

const _getCarriers = carriers => {
  const allCarriers = carriers.map(carrier => carrier.name);
  return allCarriers.join(" + ");
};

const _renderFlight = (item, type, hasInbound) => {
  if (!item) return null;

  const departure = moment(item.departure, "YYYY-MM-DD HH:mm:ss");
  const arrival = moment(item.arrival, "YYYY-MM-DD HH:mm:ss");
  const flightIcon = type === "outbound" ? iconAirplane : iconAirplaneBack;
  let diff = 0;

  if (arrival.format("DD") !== departure.format("DD")) {
    diff = parseInt(arrival.format("DD")) - parseInt(departure.format("DD"));
  }

  return (
    <Row gutter={16}>
      <Col xs={24} md={4}>
        <Row>
          <Col xs={12} md={24}>
            <img className="ico-aviao" src={flightIcon} alt={type} />
          </Col>
          <Col xs={12} md={24}>
            <img
              className="ico-company"
              src={item.carriers[0].image}
              alt={type}
              width="60"
            />
          </Col>
        </Row>
      </Col>
      <Col xs={24} md={20}>
        <Row gutter={12}>
          <Col xs={12} md={8}>
            <p className="text-gray">
              <FormattedMessage id="resultadoAereoPartida" />
            </p>
            <p className="text-blue">
              {`${departure.format("HH[H]mm")}`}{" "}
              <small>{`(${item.departure_airport})`}</small>
            </p>
          </Col>

          <Col xs={12} md={8}>
            <p className="text-gray">
              <FormattedMessage id="resultadoAereoChegada" />
            </p>
            <p className="text-blue">
              {`${arrival.format("HH[H]mm")}${diff > 0 ? ` +${diff}` : ""}`}{" "}
              <small>{`(${item.arrival_airport})`}</small>
            </p>
          </Col>

          <Col xs={24} md={8}>
            <p className="text-gray">&nbsp;</p>
            <p className="text-blue voodireto">
              <small>
                {item.stops === 0 ? "Voo direto" : `${item.stops} Escala(s)`}
              </small>
            </p>
          </Col>

          <Col className="description-mini" span={24}>
            {_getCarriers(item.carriers)}
            <span>Total: {minutesInHours(item.duration)}</span>
          </Col>
          {hasInbound && <Col className="divisor" span={24} />}
        </Row>
      </Col>
    </Row>
  );
};

const _renderFooter = (priceOption, index) => (
  <div className="footer-result" key={index}>
    <a href={priceOption.deeplink} target="_blank">
      <Row type="flex" justify="space-between" align="middle">
        <Col xs={12} md={9}>
          <span className="spanlink">via {priceOption.agent}</span>
        </Col>
        <Col xs={12} md={9}>
          <span className="val-result">{priceOption.estimate}</span>
        </Col>
        <Col xs={24} md={6}>
          <a className="button-select">
            <FormattedMessage id="resultadoAereoBtn" />
          </a>
        </Col>
      </Row>
    </a>
  </div>
);

class BoxResult extends PureComponent {
  state = {};

  render() {
    const { item } = this.props;

    console.log("ITEM RENDER", item);

    return (
      <div className="item-result item-air">
        {_renderFlight(item.outbound, "outbound", item.inbound ? true : false)}
        {_renderFlight(item.inbound, "inbound")}

        <Row>
          <Col md={24} md={20} offset={4} className="no-margin">
            {_renderFooter(item, 0)}
            {item.pricing_options.map((option, index) =>
              _renderFooter(option, index + 1)
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

export default BoxResult;
