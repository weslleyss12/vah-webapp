import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import MetaTags from "react-meta-tags";

import { Row, Col, List, Slider } from "antd";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import AddressTransition from "../../components/transition";

import Header from "../../components/header";
import Breadcrumb from "../../components/breadcrumb";
import InfoHeader from "./components/InfoHeader";
import Footer from "../../components/footer";
import Container from "../../components/container";
import BoxResult from "./components/boxResult";
import Filter from "./components/Filter";

import { FormattedMessage } from "react-intl";

import Config from "../../config";

// Actions
import { searchAir, setFilters, clearEstimates } from "../searchByAir/actions";

class ResultByAir extends Component {
  static propTypes = {
    match: PropTypes.shape().isRequired
  };

  constructor(props) {
    super(props);

    const timeRange = ["Manhã", "Tarde", "Noite"];

    this.state = {
      hour: timeRange,
      filteredOutboundHour: props.filters.hour_outbound || timeRange,
      filteredInboundHour: props.filters.hour_inbound || timeRange,
      companies: [],
      filteredCompanies: props.filters.companies || [],
      stops: [],
      filteredStops: props.filters.stops || [],
      price:
        props.filters.price || (props.results && props.results.price_range.max)
    };
  }

  componentDidMount = () => {
    const {
      match: {
        params: {
          origin,
          destination,
          go_date,
          return_date,
          adults,
          children,
          infants,
          direct
        }
      }
    } = this.props;

    this._handleRedefine();

    this.props
      .searchAir({
        origin,
        destination,
        outbound_date: go_date,
        inbound_date: return_date || null,
        adults,
        children,
        infants,
        direct,
        app_version: Config.APP_VERSION
      })
      .then(() => {
        const { pending, full_estimates: estimates } = this.props;
        const { filteredCompanies, filteredStops } = this.state;
        let companies = [];
        let stops = [];

        if (pending || (estimates && estimates.length === 0 && !pending))
          return null;

        estimates.map(estimate => {
          if (
            estimate.outbound &&
            companies.indexOf(estimate.outbound.carriers[0].name) === -1
          ) {
            companies.push(estimate.outbound.carriers[0].name);
          }

          if (
            estimate.inbound &&
            companies.indexOf(estimate.inbound.carriers[0].name) === -1
          ) {
            companies.push(estimate.inbound.carriers[0].name);
          }

          if (
            estimate.outbound.stops === 0 &&
            stops.indexOf("Voo direto") === -1
          ) {
            stops.push("Voo direto");
          }

          if (
            estimate.outbound.stops > 0 &&
            stops.indexOf(`${estimate.outbound.stops} Escala(s)`) === -1
          ) {
            stops.push(`${estimate.outbound.stops} Escala(s)`);
          }

          return estimates;
        });

        // Reorder
        companies.sort();
        stops.sort();

        // Put direct fligh on the top
        if (stops[stops.length - 1] === "Voo direto") {
          const last = stops.pop();
          stops = [last].concat(stops);
        }

        this.setState({
          ...this.state,
          companies,
          stops,
          filteredCompanies:
            filteredCompanies.length > 0 ? filteredCompanies : companies,
          filteredStops: filteredStops.length > 0 ? filteredStops : stops
        });
      });
  };

  _handleRedefine = () => {
    const { setFilters, results } = this.props;
    const { companies, stops } = this.state;

    this.setState({
      filteredCompanies: companies,
      filteredOutboundHour: ["Manhã", "Tarde", "Noite"],
      filteredInboundHour: ["Manhã", "Tarde", "Noite"],
      price: results && results.price_range.max,
      filteredStops: stops
    });

    setFilters({});
  };

  _handleApply = ({
    hour,
    filteredStops,
    filteredCompanies,
    filteredOutboundHour,
    filteredInboundHour,
    price
  }) => {
    const { setFilters, clearEstimates, results } = this.props;
    const { companies, stops } = this.state;

    this.setState({
      hour,
      filteredStops,
      filteredCompanies,
      filteredOutboundHour,
      filteredInboundHour,
      price
    });

    setTimeout(() => {
      if (
        price === results.price_range.max &&
        hour.length === filteredOutboundHour.length &&
        hour.length === filteredInboundHour.length &&
        companies.length === filteredCompanies.length &&
        stops.length === filteredStops.length
      ) {
        setFilters({});
      } else {
        setFilters({
          hour_outbound: filteredOutboundHour,
          hour_inbound: filteredInboundHour,
          price,
          companies:
            companies.length === filteredCompanies.length
              ? null
              : filteredCompanies,
          stops: stops.length === filteredStops.length ? null : filteredStops
        });
      }
    }, 500);

    clearEstimates();
  };

  render() {
    const { results, pending, filters, type, match } = this.props;

    if (results === null || pending || this.state.hasTransition)
      return (
        <AddressTransition
          results={results}
          pending={pending}
          toggleTransition={this.toggleTransition}
          activeTab="aereo"
        />
      );

    return (
      <div className="page-aereo">
        <MetaTags>
          <meta
            name="title"
            content="Passagens Aéreas Baratas: compare as menores tarifas "
          />
          <meta
            name="description"
            content="Compare agora as tarifas de várias Cias Aéreas ao mesmo tempo: Gol, Azul, Latam, Avianca e mais. Viaje mais feliz!"
          />
          <meta
            name="keywords"
            content="passagens aereas promocionais, comprar passagem aerea"
          />
          {/* <meta property="og:title" content="MyApp" />
        <meta property="og:image" content="path/to/image.jpg" /> */}
        </MetaTags>

        <Breadcrumb>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/passagens-aereas">
              Vah <FormattedMessage id="menuAereo" />
            </Link>
          </li>
          <li>Resultado</li>
        </Breadcrumb>
        <Header />
        <InfoHeader infos={match.params} backUrl="/passagens-aereas" />

        <section className="main-aereo">
          <Container>
            <Row gutter={60}>
              <Col md={24} lg={7}>
                <Filter
                  stops={this.state.stops}
                  filteredStops={this.state.filteredStops}
                  companies={this.state.companies}
                  filteredCompanies={this.state.filteredCompanies}
                  results={results}
                  filters={filters}
                  price={this.state.price}
                  hour={this.state.hour}
                  filteredOutboundHour={this.state.filteredOutboundHour}
                  filteredInboundHour={this.state.filteredInboundHour}
                  type={type}
                  onApply={this._handleApply}
                  onRedefine={this._handleRedefine}
                />
              </Col>

              <Col md={24} lg={17}>
                {(results === null || pending) && (
                  <p>Estamos pesquisando as melhores tarifas para você.</p>
                )}

                {results &&
                  results.estimates.length === 0 &&
                  !pending && <p>Nenhum resultado encontrado</p>}

                {results &&
                  results.estimates.length > 0 &&
                  !pending && (
                    <List
                      dataSource={results.estimates}
                      renderItem={item => <BoxResult item={item} />}
                    />
                  )}
              </Col>
            </Row>
          </Container>
        </section>
        <Footer />
      </div>
    );
  }
}

export default connect(
  state => ({ ...state.searchByAir }),
  dispatch =>
    bindActionCreators(
      {
        searchAir,
        setFilters,
        clearEstimates
      },
      dispatch
    )
)(ResultByAir);
