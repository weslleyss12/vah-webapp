import React, { Component } from "react";
import { Link } from "react-router-dom";
import MetaTags from "react-meta-tags";

import Container from "../../components/container";

import Header from "../../components/header";
import Footer from "../../components/footer";
import Breadcrumb from "../../components/breadcrumb";
import logoVah from "../../assets/images/core/logo-transition.png";

class Terms extends Component {
  state = {};
  render() {
    return (
      <div className="page-termos">
        <MetaTags>
          <meta name="title" content="Nossos termos de uso | Vah" />
          <meta
            name="description"
            content="Veja os termos de uso do Vah. Não importa para onde você vá, Vah com a gente!"
          />
          <meta name="keywords" content="Termos de uso vah" />
          {/* <meta property="og:title" content="MyApp" />
          <meta property="og:image" content="path/to/image.jpg" /> */}
        </MetaTags>
        <Breadcrumb>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/vah">vah</Link>
          </li>
          <li>Termos de uso</li>
        </Breadcrumb>
        <Header />
        <Container>
          <div className="container-sobre">
            <p className="logo-transition">
              <img src={logoVah} width="120" alt="VAH" />
            </p>
            <h2>Termo de Uso e Política de Privacidade</h2>
            <p>
              O USO DA PLATAFORMA APP VAH E DE SUAS FERRAMENTAS INDICARÁ
              AUTOMATICAMENTE O CONSENTIMENTO LIVRE E EXPRESSO DO USUÁRIO COM
              TODAS AS PRÁTICAS MENCIONADAS NO TERMO DE USO E POLÍTICA DE
              PRIVACIDADE, INCLUINDO ALTERAÇÕES E ATUALIZAÇÕES POSTERIORES.
            </p>

            <p>
              Este Termo de Uso e Política de Privacidade é um contrato de
              adesão, com eficácia e validade jurídica em conformidade com a
              legislação brasileira aplicável, e vigorará por prazo
              indeterminado.
            </p>

            <p>
              O APP VAHpoderá, a qualquer tempo, alterar o Termo de Uso e
              Política de Privacidadeao seu exclusivo critério. Quaisquer
              alterações no Termo de Uso e Política de Privacidadeserão
              disponibilizadas por meio da Plataforma, sempre indicando a data
              da última atualização. Caso o Usuário continue acessando e
              usufruindo da Plataforma, considerar-se-á expressa a sua
              concordância com as alterações. O Usuárioentende e concorda que,
              assim que publicada a alteração do Termo de Uso e Política de
              Privacidadeda Plataforma, passará a ser submetido ao Termo de Uso
              e Política de Privacidade atualizados. Orientamos que o Usuário
              verifique periodicamente o Termo de Uso e Política de Privacidade
              para se informar acerca das eventuais alterações feitas. Em caso
              de discordância com os termos dispostos, o Usuário poderá cessar o
              uso da Plataforma.
            </p>

            <p>
              <strong>1.0 - Plataforma de consulta</strong>
            </p>
            <p>
              1.1 A plataforma APP VAH (“Plataforma”) é um agregador, com fins
              estatísticos, de informações e dados públicos disponíveis online
              de serviços de transporte privado e urbano de passageiros
              prestados por terceiros (“Serviços”), de maneira que o uso da
              Plataforma permitirá ao Usuário realizar pesquisas mercadológicas,
              monitorar e comparar preços habituais e promocionais dos Serviços,
              para livre escolha pelo Usuário da contratação.
            </p>
            <p>
              1.2 A Plataforma é de propriedade exclusiva do APP VAH, sendo
              disponibilizada ao Usuário no site www.appvah.com e no aplicativo.
              A Plataforma poderá requerer dados de credenciamento do Usuário
              para a sua utilização.
            </p>
            <p>
              1.3 A Plataforma não permite a realização de reservas ou
              contratação direta dos Serviços, servindo exclusivamente como
              forma de viabilizar consultas às potenciais condições dos Serviços
              ou direcionar o Usuário aos prestadores de Serviços.
            </p>
            <p>
              1.4 O Usuário deverá confirmar diretamente com os prestadores dos
              Serviços a disponibilidade dos resultados da busca na Plataforma,
              suas condições de preço, pagamento e prazo, inclusive com relação
              ao conteúdo promocional.
            </p>
            <p>
              1.5 Os resultados da busca na Plataforma são orçamentos estimados
              dos valores dos Serviços, obtidos por meio de parcerias do APP VAH
              com os prestadores dos Serviços ou por consulta, com base em
              métodos de cálculo dos preços dos Serviços disponibilizados ao
              público.
            </p>
            <p>
              1.6 As informações da Plataforma referentes aos valores habituais
              ou tarifas promocionais podem estar desatualizadas, possuir
              inconsistências, margens de erro ou taxas adicionais aplicáveis,
              cabendo exclusivamente ao Usuário confirmá-las diretamente com os
              prestadores dos Serviços no momento da contratação. Em nenhuma
              hipótese o APP VAH será responsabilizado pela exatidão do
              resultado da busca na Plataforma.
            </p>
            <p>
              1.7 Para efetuar a compra, o Usuário será redirecionado ao site ou
              aplicativo do prestador de Serviços de seu interesse.
            </p>
            <p>
              1.8 A Plataforma não se responsabiliza, direta ou indiretamente,
              pelos Serviços prestados, em qualquer aspecto, nem pela cobrança
              de qualquer valor pelo prestador de Serviços.
            </p>
            <p>
              1.9 O APP VAH não possui qualquer vínculo com os prestadores dos
              Serviços, de maneira que os resultados das pesquisas
              mercadológicas realizadas pelos Usuários na Plataforma são
              meramente para consulta e, em hipótese alguma, se caracterizam
              como recomendação, propaganda, patrocínio ou aprovação pelo APP
              VAH dos Serviços listados no resultado.
            </p>
            <p>
              1.10 A Plataforma poderá indicar links de referência para outros
              sites ou aplicativos operados por terceiros, estando o APP VAH
              isento de qualquer responsabilidade pelo conteúdo destes sites ou
              aplicativos, bem como danos e prejuízos causados ao Usuário
              durante o uso deles. Caso o Usuário decida acessá-los, estará
              sujeito aos respectivos termos de uso e políticas de privacidade
              aplicáveis.
            </p>
            <p>
              <strong>2.0 - Uso da Plataforma</strong>
            </p>
            <p>
              2.1 Caso solicitado, o Usuário deverá preencher todos os campos de
              cadastro para uso da Plataforma, comprometendo-se a prestar
              informações verdadeiras, responsabilizando-se civil e
              criminalmente pela autenticidade de tais informações e assumindo o
              compromisso de atualização dos dados informados em caso de
              alteração.
            </p>
            <p>
              2.2 O APP VAH pode, a seu exclusivo critério e a qualquer momento,
              sem notificação prévia ou responsabilidade, suspender, restringir
              ou extinguir o acesso do Usuário à Plataforma, não sendo devida
              nenhuma indenização ou ressarcimento ao Usuário.
            </p>
            <p>
              <strong>3.0 - Responsabilidade do APP VAH</strong>
            </p>
            <p>
              3.1 A Plataforma APP VAH não presta diretamente os Serviços, não
              possui qualquer vínculo com os prestadores dos serviços, tampouco
              realiza controle ou faz intermediações de tais serviços realizados
              por terceiros, estando, portanto, isenta de qualquer
              responsabilidade pelos Serviços contratados pelo Usuário com base
              na pesquisa mercadológica realizada na Plataforma, incluindo a
              reserva, o pagamento e o transporte em si. A contratação pelo
              Usuário dos Serviços estará sujeita às regras específicas do termo
              de uso e das políticas de privacidade dos prestadores dos
              Serviços.
            </p>
            <p>
              3.2 O Usuário concorda que em caso de problemas ou disputas em
              relação aos Serviços, deverá resolvê-los diretamente com o
              prestador dos Serviços, isentando o APP VAH de qualquer
              responsabilidade.
            </p>
            <p>
              3.3 O APP VAH não será responsabilizado por casos de quebra de
              segurança provocados por terceiros ou falhas de serviço de
              natureza elétrica ou de telecomunicações, tal qual pela ocorrência
              de desastres ou demais casos fortuitos que impeçam o bom
              funcionamento da Plataforma.
            </p>
            <p>
              <strong>4.0 - Obrigações do Usuário</strong>
            </p>
            <p>4.1 Constituem obrigações do Usuário:</p>

            <ul>
              <li>
                fornecer informações verdadeiras, precisas, atualizadas, e
                completas, no ato do seu cadastramento junto à Plataforma, caso
                solicitado;
              </li>
              <li>
                utilizar a Plataforma unicamente com intuito lícito, sendo
                vedada qualquer utilização para fins estranhos à finalidade
                original da Plataforma;
              </li>
              <li>
                não utilizar a Plataforma para armazenar, inserir, distribuir,
                publicar, transmitir, reproduzir, copiar ou de qualquer forma
                colocar à disposição de terceiros arquivos, mensagens, desenhos,
                gráficos, sons, imagens, fotografias, programas de computador, e
                quaisquer outros materiais que: (i) violem direitos de
                terceiros, de qualquer tipo, incluindo direitos de propriedade
                industrial e intelectual da Plataforma ou de terceiros; (ii)
                contenham conteúdo delituoso, difamatório, infame, violento,
                pornográfico, ou contrário à lei, à moral e aos bons costumes;
                (iii) contenham conteúdo discriminatório em razão de sexo, raça,
                religião, condição social, idade, crença, e qualquer outro; (iv)
                contenham conteúdo que possa induzir a um estado de ansiedade ou
                temor; (v) induzam ou incitem os usuários a se envolver em
                práticas perigosas, de risco, ou nocivas à integridade física e
                psíquica; (vi) que contenham informações falsas, inexatas,
                exageradas, ou que de qualquer forma induzam a erro quanto às
                reais intenções do Usuário; (vii) sejam contrários à honra,
                reputação, intimidade e privacidade de qualquer pessoa; (viii)
                constituam publicidade ilícita ou enganosa, e/ou concorrência
                desleal; e/ou (ix) contenha malware ou qualquer outro programa,
                aplicação, complemento contaminante ou destrutivo, ou que de
                outra forma cause dificuldade ao normal funcionamento da
                Plataforma, entre outras que possam ser nocivos a terceiros e a
                Plataforma;
              </li>
              <li>
                não utilizar a Plataforma para cometer e/ou tentar cometer atos
                que tenham como objetivo: (i) obter acesso não autorizado a
                outro computador, servidor, ou rede; (ii) interromper serviço,
                servidores, ou rede de computadores por meio de qualquer método
                ilícito; (iii) burlar qualquer sistema de autenticação ou de
                segurança; (iv) vigiar secretamente terceiros; (v) acessar
                informações confidenciais, de qualquer natureza, tais como nome
                de usuários ou senhas de acesso de outro usuário da internet que
                esteja vulnerável; e/ou (vi) modificar, adaptar, traduzir,
                preparar trabalhos derivados, descompilar, fazer engenharia
                reversa, desmontar ou tentar alterar o código-fonte e/ou
                software da Plataforma;
              </li>
              <li>
                não divulgar, tornar disponível, transmitir ou disponibilizar
                qualquer tipo de propaganda ou meio análogo à propaganda por
                meio da Plataforma;{" "}
              </li>
              <li>
                respeitar todas as condições estabelecidas neste Termo de Uso e
                Política de Privacidade, bem como a legislação brasileira
                vigente.
              </li>
            </ul>

            <p>
              4.2 O APP VAH pode, a seu exclusivo critério e a qualquer momento,
              sem notificação prévia ou responsabilidade, suspender, restringir
              ou extinguir o acesso do Usuário à Plataforma, sem prejuízo de
              comunicar às autoridades competentes, a seu exclusivo critério, no
              caso de descumprimento de qualquer das obrigações ora
              estabelecidas, bem como ser indenizado por perdas e danos
              causados.
            </p>
            <p>
              <strong>5.0 - Política de Privacidade</strong>
            </p>
            <p>
              5.1 A Plataforma poderá utilizar recursos de geolocalização do
              Usuário, visando aumentar a precisão dos resultados dos Serviços
              em benefício do Usuário.
            </p>
            <p>
              5.2 A Plataforma poderá coletar dados e informações do Usuário
              para fins de melhorias no seu sistema em benefício do Usuário,
              ficando expressamente autorizada a armazenar dados de acesso à
              Plataforma, deslocamentos, movimentações e georreferenciamento do
              Usuário durante o uso da Plataforma.
            </p>
            <p>
              5.3 O APP VAH adota as medidas de segurança adequadas, de acordo
              com os padrões de mercado, para a proteção dos dados do Usuário e
              fará todo o possível para mantê-lossempre seguros e utilizá-los de
              forma a preservar a honra, intimidade, atividade privada, imagem e
              sigilo, os mantendo criptografados e protegidos. No entanto, o
              Usuário reconhece que nenhum sistema, servidor ou software está
              absolutamente imune a ataques e/ou invasões de hackers e outros
              agentes maliciosos, de maneira que o APP VAH não garante de forma
              alguma que tais medidas de segurança sejam isentas de erros ou que
              não estejam sujeitas a interferência de terceiros (hackers, entre
              outros). Por sua natureza, apesar dos melhores esforços do APP
              VAH, qualquer medida de segurança pode falhar e pode se tornar
              público qualquer dos dadosdo Usuário.
            </p>
            <p>
              5.4 O Usuário entende e assume expressamente esse risco e concorda
              que o APP VAH não será responsável por tal tipo de exposição,
              exclusão, obtenção, utilização ou divulgação não autorizada dos
              dados resultantes de ataques que o APP VAH não poderia
              razoavelmente evitar por meio dos referidos padrões de segurança.
            </p>
            <p>
              5.5 O Usuário autoriza expressamente que as informações dos
              registros eletrônicos de acessos e de navegação, bem como seu
              cadastro, se aplicável, na Plataforma sejam fornecidos a terceiros
              ou a autoridades competentes, em atendimento à solicitação formal
              destas e nos demais casos previstos na legislação vigente. O APP
              VAH adota a política de preservação irrestrita da identidade do
              Usuário, de maneira que os dados coletados do Usuário serão
              descaracterizados antes de disponibilizados e não permitirão a
              identificação do Usuário.
            </p>
            <p>
              5.6 Todas as informações das viagens do Usuário, bem como de seu
              cadastro, se aplicável, são armazenadas em um banco de dados do
              APP VAH, com acesso restrito às pessoas habilitadas e obrigadas,
              por contrato, a manter a confidencialidade das informações e não
              as utilizar inadequadamente.
            </p>
            <p>
              <strong>6.0 - Foro e lei aplicável</strong>
            </p>
            <p>
              6.1 Fica eleito o foro da cidade de São Paulo para resolver
              qualquer demanda oriunda do presente Termo de Uso e/ou Política de
              Privacidade. Os Termo de Uso e Política de Privacidade e todos os
              aspectos da relação jurídica por eles instituídas, serão regidos e
              interpretados pelas leis da República Federativa do Brasil.
            </p>
          </div>
        </Container>
        <Footer />
      </div>
    );
  }
}

export default Terms;
