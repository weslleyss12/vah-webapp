import { fetchDefaultOptions } from '../../utils/fetchDefaultOptions';
import Config from '../../config';

import {
  ACTION_SEARCH_BY_RIDE_SET_ADDRESS,
  ACTION_SEARCH_BY_RIDE_SET_DATE,
  ACTION_SEARCH_BY_RIDE_SET_HOUR,
  ACTION_SEARCH_BY_RIDE_SET_SEATS,
  ACTION_SEARCH_BY_RIDE_REVERSE_ADDRESSES,
  ACTION_SEARCH_BY_RIDE_RESET,
  ACTION_SEARCH_BY_RIDE_FETCH,
  ACTION_SEARCH_BY_RIDE_SET_FILTERS
} from '../../main/constants';

function actionSearch(values) {
  const url = `${Config.API_BASE_URL}/estimates/ride`;

  return fetch(url, fetchDefaultOptions('POST', values))
    .then(response => response.json())
    .then(responseJson => responseJson)
    .catch(error => error.message);
}

export function searchRide(values) {
  return {
    type: ACTION_SEARCH_BY_RIDE_FETCH,
    payload: actionSearch(values)
  };
}

export function setAddress(address, type) {
  return {
    type: ACTION_SEARCH_BY_RIDE_SET_ADDRESS,
    payload: { address, type }
  };
}

export function setDate(date) {
  return {
    type: ACTION_SEARCH_BY_RIDE_SET_DATE,
    payload: date
  };
}

export function setHour(hour) {
  return {
    type: ACTION_SEARCH_BY_RIDE_SET_HOUR,
    payload: hour
  };
}

export function setSeats(seats) {
  return {
    type: ACTION_SEARCH_BY_RIDE_SET_SEATS,
    payload: seats
  };
}

export function reverseAddress() {
  return {
    type: ACTION_SEARCH_BY_RIDE_REVERSE_ADDRESSES
  };
}

export function setFilters(filters) {
  return {
    type: ACTION_SEARCH_BY_RIDE_SET_FILTERS,
    payload: filters
  };
}

export function resetSearch() {
  return {
    type: ACTION_SEARCH_BY_RIDE_RESET
  };
}
