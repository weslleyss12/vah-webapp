import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import MetaTags from "react-meta-tags";

import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import Breadcrumb from "../../components/breadcrumb";
import Header from "../../components/header";
import Footer from "../../components/footer";
import Container from "../../components/container";

import google from "../../assets/images/google.png";

import ContentText from "../../components/contentText";

import FormSearch from "./components/formSearch";

import { FormattedMessage } from "react-intl";

// Actions
import { resetSearch } from "./actions";

class SearchByRide extends Component {
  static propTypes = {
    resetSearch: PropTypes.func.isRequired
  };

  state = {};

  componentDidMount() {
    // Reset old search
    this.props.resetSearch();
  }

  componentWillUnmount() {
    // Reset old search
    this.props.resetSearch();
  }

  render() {
    return (
      <div className="page-carona">
        <MetaTags>
          <meta
            name="title"
            content="Carona Compartilhada: veja caronas disponíveis | Vah"
          />
          <meta
            name="description"
            content="Escolha seu destino e veja agora as vagas de carona compartilhada disponíveis. Viajar de carona ficou fácil!"
          />
          <meta name="keywords" content="carona compartilhada, carona" />
          {/* <meta property="og:title" content="MyApp" />
          <meta property="og:image" content="path/to/image.jpg" /> */}
        </MetaTags>

        <Breadcrumb>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            Vah <FormattedMessage id="menuCarona" />
          </li>
        </Breadcrumb>
        <Header />
        <div className="BannerSearch">
          <Container>
            <div className="BannerFormSearch">
              <FormSearch />
            </div>
          </Container>
        </div>
        <section className="main-info">
          <Container>
            <ContentText />
          </Container>
        </section>
        <Footer />
      </div>
    );
  }
}

export default connect(
  state => ({ ...state.SearchByRide }),
  dispatch => bindActionCreators({ resetSearch }, dispatch)
)(SearchByRide);
