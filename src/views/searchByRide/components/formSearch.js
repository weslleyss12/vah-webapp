/* global google */

import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { get } from "lodash";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Form, Input, Button, DatePicker, AutoComplete, Icon } from "antd";
import { withRouter } from "react-router-dom";
import PlacesAutocomplete, {
  geocodeByPlaceId,
  getLatLng
} from "react-places-autocomplete";
import PropTypes from "prop-types";
import moment from "moment";
import "moment/locale/pt-br";

import googleImg from "../../../assets/images/google.png";

import "./formSearch.css";
import Config from "../../../config";
import Geocoder from "../../../components/react-native-geocoding";

// Helpers
import {
  formattingPlaceApiResult,
  formattingCurrentLocation,
  getFromPlaceApiResult,
  getLocation
} from "../../../utils/helpers";
import generatePath from "../../../utils/generatePath";
import localeDatepPicker from "../../../utils/localeDatepPicker";

// Actions
import {
  setAddress,
  reverseAddress,
  setDate,
  setHour,
  setSeats
} from "../actions";

const FormItem = Form.Item;
const { Option } = AutoComplete;

class FormSearch extends Component {
  static propTypes = {
    form: PropTypes.shape().isRequired,
    setAddress: PropTypes.func.isRequired,
    reverseAddress: PropTypes.func.isRequired,
    setDate: PropTypes.func.isRequired,
    setHour: PropTypes.func.isRequired,
    setSeats: PropTypes.func.isRequired,
    hour: PropTypes.number.isRequired,
    seats: PropTypes.number.isRequired,
    match: PropTypes.shape().isRequired,
    origin: PropTypes.shape(),
    destination: PropTypes.shape(),
    history: PropTypes.shape().isRequired
  };

  static defaultProps = {
    origin: null,
    destination: null
  };

  constructor(props) {
    super(props);

    // Geocoder
    Geocoder.setApiKey(Config.GOOGLE_PLACES_API);

    this.state = {
      isValidOrigin: false,
      isValidDestination: false,
      loadingCurrentLocation: false,
      current_location: null
    };
  }

  componentDidMount() {
    this.props.form.validateFields();
    this.requestUserLocation();
  }

  requestUserLocation = () => {
    getLocation((status, coords) => {
      if (status) {
        this.setState({
          current_location: coords
        });
      }
    });
  };

  increaseCounter = (e, type) => {
    e.preventDefault();
    const { hour, seats } = this.props;

    if (type === "hours") {
      if (hour < 24) this.props.setHour(hour + 1);
    } else this.props.setSeats(seats + 1);
  };

  decreaseCounter = (e, type) => {
    e.preventDefault();
    const { hour, seats } = this.props;

    if (hour > 0 && type === "hours") this.props.setHour(hour - 1);
    if (seats > 0 && type === "seats") this.props.setSeats(seats - 1);
  };

  handleSubmit = e => {
    e.preventDefault();
    const {
      origin,
      destination,
      date,
      hour,
      seats,
      match,
      history
    } = this.props;
    const matchParams = get(match, "params", {});

    const userLang = navigator.language || navigator.userLanguage;
    const lang = userLang.split("-");

    const latam = {
      AR: "ARS",
      BO: "BOB",
      BR: "BRL",
      CL: "CLP",
      CO: "COP",
      CR: "CRC",
      CU: "CUC",
      EC: "USD",
      SV: "USD",
      GT: "CTQ",
      HT: "HTG",
      HN: "HLN",
      MX: "MXN",
      NI: "NIO",
      PA: "PAB",
      PY: "PYG",
      PE: "PEN",
      DO: "DOP",
      UY: "UYU",
      VE: "VEF"
    };

    const moeda = lang[1];
    let currencyMoney = "";

    if (latam[`${moeda}`]) {
      currencyMoney = latam[`${moeda}`];
    } else {
      currencyMoney = "USD";
    }

    this.props.form.validateFields(err => {
      if (!err) {
        const url = generatePath(
          "/carona-compartilhada/buscar/:date/:start_hour/:seats/:start_latitude/:start_longitude/:start_address/:start_city/:start_state/:start_zip_code/:end_latitude/:end_longitude/:end_address/:end_city/:end_state/:end_zip_code/:country/:language/:currency/",
          {
            ...matchParams,

            date,
            start_hour: hour,
            seats,

            start_latitude: origin.latitude,
            start_longitude: origin.longitude,
            start_address: origin.street,
            start_city: origin.city,
            start_state: origin.state,
            start_zip_code: origin.zipcode || "cep",

            country: origin.state,
            language: lang[0],
            currency: currencyMoney,

            end_latitude: destination.latitude,
            end_longitude: destination.longitude,
            end_address: destination.street,
            end_city: destination.city,
            end_state: destination.state,
            end_zip_code: destination.zipcode || "cep"
          }
        );

        history.push(url);
      }
    });
  };

  _handleSearch = (value, placeType) => {
    this.props.form.setFieldsValue({ [placeType]: value });

    if (placeType === "origin") this.setState({ isValidOrigin: false });
    else this.setState({ isValidDestination: false });
  };

  _handleSearchPress = (_value, option, placeType) => {
    const {
      props: { suggestion }
    } = option;
    const address = formattingPlaceApiResult(suggestion);

    if (address.id) {
      geocodeByPlaceId(address.id)
        .then(result => {
          address.zipcode = getFromPlaceApiResult(result[0], "postal_code");
          return getLatLng(result[0]);
        })
        .then(({ lat, lng }) => {
          address.latitude = lat;
          address.longitude = lng;

          this.props.setAddress(address, placeType);

          if (placeType === "origin") this.setState({ isValidOrigin: true });
          else this.setState({ isValidDestination: true });
        })
        .catch(error => console.error(error));
    }
  };

  renderFunc = (
    placeType,
    { getInputProps, getSuggestionItemProps, suggestions }
  ) => (
      <AutoComplete
        {...getInputProps()}
        placeholder={<FormattedMessage id="buscaCaronaPlace" />}
        onChange={value => getInputProps().onChange({ target: { value } })}
        onSelect={(value, option) =>
          this._handleSearchPress(value, option, placeType)
        }
        dataSource={suggestions.map(suggestion => (
          <Option
            {...getSuggestionItemProps(suggestion)}
            value={suggestion.description}
            suggestion={suggestion}
          >
            {suggestion.description}
          </Option>
        )).concat([
          <Option disabled key="all" className="show-all">
            <p className="google-direitos">powered by <img src={googleImg} alt="Google" width="50" /></p>
          </Option>,
        ])}
      >
        <Input
          suffix={
            placeType === "origin" &&
            (!this.state.loadingCurrentLocation ? (
              <Button
                type="button"
                className="btn-location"
                onClick={this.getUserLocation}
              />
            ) : (
                <Icon type="loading" />
              ))
          }
        />
      </AutoComplete>
    );

  getUserLocation = () => {
    this.setState({ loadingCurrentLocation: true });

    getLocation((status, coords) => {
      if (status) {
        Geocoder.getFromLatLng(coords.latitude, coords.longitude).then(
          json => {
            const first = json.results[0].address_components;
            const address = formattingCurrentLocation(first);

            address.latitude = coords.latitude;
            address.longitude = coords.longitude;
            address.type = "history";

            this.props.setAddress(address, "origin");

            this.props.form.setFieldsValue({
              origin: `${address.street} - ${address.neighborhood}, ${
                address.city
                } - ${address.state}`
            });

            this.setState({
              loadingCurrentLocation: false,
              isValidOrigin: true
            });
          },
          error => {
            console.log(error);
            this.setState({ loadingCurrentLocation: false });
          }
        );
      } else {
        this.setState({ loadingCurrentLocation: false });
      }
    });
  };

  reverseAddress = () => {
    const { getFieldsValue, setFieldsValue } = this.props.form;
    const fields = getFieldsValue();

    setFieldsValue({
      origin: fields.destination,
      destination: fields.origin
    });

    this.props.reverseAddress();
  };

  handleSelectDate = (date, dateString) => {
    const formatedDate = date.format("YYYY-MM-DD");

    this.props.form.setFieldsValue({ ida: dateString });
    this.props.setDate(formatedDate);
  };

  hasErrors = fieldsError =>
    Object.keys(fieldsError).some(field => fieldsError[field]);

  disabledDate = current =>
    current && moment(current).endOf("day") < moment().endOf("day");

  render() {
    const {
      hour,
      seats,
      form: { getFieldDecorator, getFieldsError }
    } = this.props;
    const { isValidOrigin, isValidDestination, current_location } = this.state;

    const searchOptions = current_location
      ? {
        location: new google.maps.LatLng(
          current_location.latitude,
          current_location.longitude
        ),
        radius: 2000
      }
      : {
        componentRestrictions: { country: "br" }
      };

    return (
      <Form
        className="search-form form-carona"
        layout="inline"
        onSubmit={this.handleSubmit}
      >
        <h1>
          <FormattedMessage id="buscaCaronaTitle" />
        </h1>

        <FormItem validateStatus="" help="" className="field-big">
          <label>
            <FormattedMessage id="buscaCaronaSaindo" />
          </label>
          {getFieldDecorator("origin", {
            rules: [{ required: true }]
          })(
            <PlacesAutocomplete
              onChange={value => this._handleSearch(value, "origin")}
              searchOptions={searchOptions}
            >
              {(...rest) => this.renderFunc("origin", ...rest)}
            </PlacesAutocomplete>
          )}
        </FormItem>

        <button
          className="swap-button"
          type="button"
          onClick={this.reverseAddress}
        />

        <FormItem validateStatus="" help="" className="field-big">
          <label>
            <FormattedMessage id="buscaCaronaIndo" />
          </label>
          {getFieldDecorator("destination", {
            rules: [{ required: true }]
          })(
            <PlacesAutocomplete
              onChange={value => this._handleSearch(value, "destination")}
              searchOptions={searchOptions}
            >
              {(...rest) => this.renderFunc("destination", ...rest)}
            </PlacesAutocomplete>
          )}
        </FormItem>

        <FormItem validateStatus="" help="" className="field-medium">
          {getFieldDecorator("ida", {
            rules: [{ required: true }]
          })(
            <div>
              <label>
                <FormattedMessage id="buscaCaronaDataIda" />
              </label>
              <DatePicker
                locale={localeDatepPicker}
                format="DD/MMM/YY"
                placeholder="Data"
                onChange={this.handleSelectDate}
                disabledDate={this.disabledDate}
                className="form-input"
                allowClear={false}
              />
            </div>
          )}
        </FormItem>

        <FormItem className="field-count">
          <label>
            <FormattedMessage id="buscaCaronaSaida" />
          </label>
          <span className="campo">
            <button onClick={e => this.decreaseCounter(e, "hours")}>-</button>
            <Input type="text" value={`${hour}h`} readOnly />
            <button onClick={e => this.increaseCounter(e, "hours")}>+</button>
          </span>
        </FormItem>

        <FormItem className="field-count">
          <label>
            <FormattedMessage id="buscaCaronaLugares" />
          </label>
          <span className="campo">
            <button
              type="button"
              onClick={e => this.decreaseCounter(e, "seats")}
            >
              -
            </button>
            <Input type="text" value={seats} readOnly />
            <button
              type="button"
              onClick={e => this.increaseCounter(e, "seats")}
            >
              +
            </button>
          </span>
        </FormItem>

        <FormItem className="field-full">
          <Button
            type="primary"
            htmlType="submit"
            className="btn-comparar"
            disabled={
              this.hasErrors(getFieldsError()) ||
              !isValidOrigin ||
              !isValidDestination
            }
          >
            <FormattedMessage id="buscaCaronaBotao" />
          </Button>
        </FormItem>
      </Form>
    );
  }
}

const WrappedFormSearch = Form.create()(FormSearch);

export default withRouter(
  connect(
    state => ({ ...state.searchByRide }),
    dispatch =>
      bindActionCreators(
        { setAddress, reverseAddress, setDate, setHour, setSeats },
        dispatch
      )
  )(WrappedFormSearch)
);
