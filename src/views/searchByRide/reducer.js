import { timeMoment } from '../../utils/helpers'

import {
  ACTION_SEARCH_BY_RIDE_SET_ADDRESS,
  ACTION_SEARCH_BY_RIDE_SET_DATE,
  ACTION_SEARCH_BY_RIDE_SET_HOUR,
  ACTION_SEARCH_BY_RIDE_SET_SEATS,
  ACTION_SEARCH_BY_RIDE_REVERSE_ADDRESSES,
  ACTION_SEARCH_BY_RIDE_RESET,
  ACTION_SEARCH_BY_RIDE_FETCH_PENDING,
  ACTION_SEARCH_BY_RIDE_FETCH_FULFILLED,
  ACTION_SEARCH_BY_RIDE_FETCH_REJECTED,
  ACTION_SEARCH_BY_RIDE_SET_FILTERS
} from '../../main/constants';

const INITIAL_STATE = {
  current_location: null,
  origin: null,
  destination: null,
  date: null,
  hour: 8,
  seats: 1,
  pending: true,
  error: null,
  results: null,
  full_estimates: [],
  filters: {}
};

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case ACTION_SEARCH_BY_RIDE_SET_ADDRESS:
      return { ...state, [payload.type]: payload.address }

    case ACTION_SEARCH_BY_RIDE_SET_DATE:
      return { ...state, date: payload }

    case ACTION_SEARCH_BY_RIDE_SET_HOUR:
      return { ...state, hour: payload }

    case ACTION_SEARCH_BY_RIDE_SET_SEATS:
      return { ...state, seats: payload }

    case ACTION_SEARCH_BY_RIDE_REVERSE_ADDRESSES:
      return { ...state, origin: state.destination, destination: state.origin }

    case ACTION_SEARCH_BY_RIDE_FETCH_PENDING:
      return { ...state, pending: true, results: null, full_estimates: [], filters: {} }

    case ACTION_SEARCH_BY_RIDE_FETCH_FULFILLED:
      if (payload.status) {
        return { ...state, pending: false, results: payload.data, full_estimates: payload.data.estimates }
      }

      return { ...state, pending: false }

    case ACTION_SEARCH_BY_RIDE_FETCH_REJECTED:
      return { ...state, pending: false, error: payload }

    case ACTION_SEARCH_BY_RIDE_SET_FILTERS:
      const results = filterItems(payload, state)
      return { ...state, filters: payload, results }

    case ACTION_SEARCH_BY_RIDE_RESET:
      return INITIAL_STATE

    default:
      return state;
  }
};

const filterItems = (filters, state, localResults) => {
  const e = state ? state.full_estimates : localResults.estimates
  const r = state ? state.results : localResults

  const validHour = (hour) => (hour !== 'Any time' && hour !== 'Qualquer horário')

  const isAValidMoment = (hour, date) => {
    if (
      ((hour.indexOf('Morning') !== -1 || hour.indexOf('Manhã') !== -1) && timeMoment(date) === 'morning') ||
      ((hour.indexOf('Afternoon') !== -1 || hour.indexOf('Tarde') !== -1) && timeMoment(date) === 'afternoon') ||
      ((hour.indexOf('Night') !== -1 || hour.indexOf('Noite') !== -1) && timeMoment(date) === 'night')
    ) {
      return true
    }

    return false
  }

  const estimates = e.filter(estimate => {
    return (
      ((filters.hour && validHour(filters.hour)) ? isAValidMoment(filters.hour, estimate.departure_at) : true) &&
      (filters.price ? (parseInt(estimate.price) <= filters.price) : true)
    )
  })

  return { ...r, estimates }
}
