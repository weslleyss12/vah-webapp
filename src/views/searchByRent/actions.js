import { fetchDefaultOptions } from '../../utils/fetchDefaultOptions';
import Config from '../../config';

import {
  ACTION_SEARCH_BY_RENT_RESET,
  ACTION_SEARCH_BY_RENT_SET_PLACE,
  ACTION_SEARCH_BY_RENT_SET_DATE,
  ACTION_SEARCH_BY_RENT_SET_TIME,
  ACTION_SEARCH_BY_RENT_SET_TYPE,
  ACTION_SEARCH_BY_RENT_AUTOCOMPLETE_FETCH,
  ACTION_SEARCH_BY_RENT_FETCH,
  ACTION_SEARCH_BY_RENT_REVERSE_PLACE,
  ACTION_SEARCH_BY_RENT_SET_FILTERS,
  ACTION_SEARCH_BY_RENT_CLEAR_ESTIMATES
} from '../../main/constants';

function actionAutocomplete(term) {
  const url = `${Config.API_BASE_URL}/rent/places?term=${term}`;

  return fetch(url, fetchDefaultOptions())
    .then(response => response.json())
    .then(responseJson => responseJson)
    .catch(error => error.message);
}

function actionSearch(values) {
  const url = `${Config.API_BASE_URL}/estimates/rent`;

  return fetch(url, fetchDefaultOptions('POST', values))
    .then(response => response.json())
    .then(responseJson => responseJson)
    .catch(error => error.message);
}

export function autocomplete(values) {
  return {
    type: ACTION_SEARCH_BY_RENT_AUTOCOMPLETE_FETCH,
    payload: actionAutocomplete(values)
  };
}

export function searchRent(values) {
  return {
    type: ACTION_SEARCH_BY_RENT_FETCH,
    payload: actionSearch(values)
  };
}

export function setPlace(place, type) {
  return {
    type: ACTION_SEARCH_BY_RENT_SET_PLACE,
    payload: { place, type }
  };
}

export function setDate(date, type) {
  return {
    type: ACTION_SEARCH_BY_RENT_SET_DATE,
    payload: { date, type }
  };
}

export function setTime(time, type) {
  return {
    type: ACTION_SEARCH_BY_RENT_SET_TIME,
    payload: { time, type }
  };
}

export function setType(type) {
  return {
    type: ACTION_SEARCH_BY_RENT_SET_TYPE,
    payload: type
  };
}

export function reversePlace() {
  return {
    type: ACTION_SEARCH_BY_RENT_REVERSE_PLACE
  };
}

export function setFilters(filters) {
  return {
    type: ACTION_SEARCH_BY_RENT_SET_FILTERS,
    payload: filters
  };
}

export function clearEstimates() {
  return {
    type: ACTION_SEARCH_BY_RENT_CLEAR_ESTIMATES
  };
}

export function resetSearch() {
  return {
    type: ACTION_SEARCH_BY_RENT_RESET
  };
}
