import {
  ACTION_SEARCH_BY_RENT_SET_PLACE,
  ACTION_SEARCH_BY_RENT_SET_DATE,
  ACTION_SEARCH_BY_RENT_SET_TIME,
  ACTION_SEARCH_BY_RENT_SET_TYPE,
  ACTION_SEARCH_BY_RENT_RESET,
  ACTION_SEARCH_BY_RENT_FETCH_PENDING,
  ACTION_SEARCH_BY_RENT_FETCH_FULFILLED,
  ACTION_SEARCH_BY_RENT_FETCH_REJECTED,
  ACTION_SEARCH_BY_RENT_AUTOCOMPLETE_FETCH_PENDING,
  ACTION_SEARCH_BY_RENT_AUTOCOMPLETE_FETCH_FULFILLED,
  ACTION_SEARCH_BY_RENT_AUTOCOMPLETE_FETCH_REJECTED,
  ACTION_SEARCH_BY_RENT_REVERSE_PLACE,
  ACTION_SEARCH_BY_RENT_SET_FILTERS,
  ACTION_SEARCH_BY_RENT_CLEAR_ESTIMATES
} from '../../main/constants';

const INITIAL_STATE = {
  type: 'same',
  pickup_location: null,
  return_location: null,
  pickup_date: null,
  return_date: null,
  pickup_time: 12,
  return_time: 12,
  pending: true,
  error: null,
  results: null,
  full_estimates: [],
  filters: {},
  autocomplete_results: [],
  pending_autocomplete: false
}

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case ACTION_SEARCH_BY_RENT_SET_PLACE:
      const data = {
        ...state,
        [payload.type + '_location']: payload.place,
        autocomplete_results: []
      }

      if (payload.type === 'pickup') {
        data['return_location'] = payload.place
      }

      return data

    case ACTION_SEARCH_BY_RENT_REVERSE_PLACE:
      return { ...state, pickup_location: state.return_location, return_location: state.pickup_location }

    case ACTION_SEARCH_BY_RENT_SET_DATE:
      return { ...state, [payload.type + '_date']: payload.date }

    case ACTION_SEARCH_BY_RENT_SET_TIME:
      return { ...state, [payload.type + '_time']: payload.time }

    case ACTION_SEARCH_BY_RENT_SET_TYPE:
      return { ...state, type: payload, return_location: (payload === 'same' ? state.pickup_location : state.return_location) }

    case ACTION_SEARCH_BY_RENT_FETCH_PENDING:
      return { ...state, pending: true, results: null, full_estimates: [], filters: {} }

    case ACTION_SEARCH_BY_RENT_AUTOCOMPLETE_FETCH_PENDING:
      return { ...state, pending_autocomplete: true, results: null }

    case ACTION_SEARCH_BY_RENT_FETCH_FULFILLED:
      if (payload.status) {
        return { ...state, pending: false, results: payload.data, full_estimates: payload.data.estimates }
      }

      return { ...state, pending: false }

    case ACTION_SEARCH_BY_RENT_AUTOCOMPLETE_FETCH_FULFILLED:
      if (payload.status) {
        return { ...state, pending_autocomplete: false, autocomplete_results: payload.data }
      }

      return { ...state, pending: false }

    case ACTION_SEARCH_BY_RENT_FETCH_REJECTED:
      return { ...state, pending: false, error: payload }

    case ACTION_SEARCH_BY_RENT_AUTOCOMPLETE_FETCH_REJECTED:
      return { ...state, pending_autocomplete: false, error: payload }

    case ACTION_SEARCH_BY_RENT_SET_FILTERS:
      const results = filterItems(payload, state)
      return { ...state, filters: payload, results, pending: false }

    case ACTION_SEARCH_BY_RENT_CLEAR_ESTIMATES:
      return { ...state, results: { ...state.results, estimates: [] }, pending: true }

    case ACTION_SEARCH_BY_RENT_RESET:
      return INITIAL_STATE

    default:
      return state
  }
}

const filterItems = (filters, state, localResults) => {
  const e = state ? state.full_estimates : localResults.estimates
  const r = state ? state.results : localResults

  const hasOptional = (filters, setup) => {
    let status = true

    if (filters.indexOf('Ar-condicionado') !== -1 && !setup.ac) {
      status = false
    }
    if (filters.indexOf(`${setup.doors} Portas`) === -1) {
      status = false
    }
    if (filters.indexOf('Manual') !== -1 && setup.transmission !== 'Manual') {
      status = false
    }
    if (filters.indexOf('Automático') !== -1 && setup.transmission !== 'Automático') {
      status = false
    }

    return status
  }

  const estimates = e.filter(estimate => {
    return (
      (filters.companies ? filters.companies.indexOf(estimate.flag.name) !== -1 : true) &&
      (filters.categories ? filters.categories.indexOf(estimate.group.category) !== -1 : true) &&
      (filters.setup ? hasOptional(filters.setup, estimate.group.setup) : true)
    )
  })

  return { ...r, estimates }
}
