import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import MetaTags from "react-meta-tags";

import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import Breadcrumb from "../../components/breadcrumb";
import Header from "../../components/header";
import Footer from "../../components/footer";
import Container from "../../components/container";

import FormSearch from "./components/formSearch";

import ContentText from "../../components/contentText";

import { FormattedMessage } from "react-intl";

// Actions
import { resetSearch } from "./actions";

class SearchByRent extends Component {
  static propTypes = {
    resetSearch: PropTypes.func.isRequired
  };

  state = {};

  componentDidMount() {
    // Reset old search
    this.props.resetSearch();
  }

  componentWillUnmount() {
    // Reset old search
    this.props.resetSearch();
  }

  render() {
    return (
      <div className="page-locacao">
        <MetaTags>
          <meta
            name="title"
            content="Aluguel de Carros: compare preços em várias locadoras | Vah"
          />
          <meta
            name="description"
            content="Confira agora tarifas de várias locadoras de veículos: Movida, Hertz, Avis, Localiza e mais. Compare todas ao mesmo tempo e viaje mais feliz!"
          />
          <meta
            name="keywords"
            content="aluguel de carros, locadora de veiculos"
          />
          {/* <meta property="og:title" content="MyApp" />
          <meta property="og:image" content="path/to/image.jpg" /> */}
        </MetaTags>
        <Breadcrumb>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            Vah <FormattedMessage id="menuLocacao" />
          </li>
        </Breadcrumb>
        <Header />
        <div className="BannerSearch">
          <Container>
            <div className="BannerFormSearch">
              <FormSearch />
            </div>
          </Container>
        </div>
        <section className="main-info">
          <Container>
            <ContentText />
          </Container>
        </section>

        <Footer />
      </div>
    );
  }
}

export default connect(
  state => ({ ...state.searchByRent }),
  dispatch => bindActionCreators({ resetSearch }, dispatch)
)(SearchByRent);
