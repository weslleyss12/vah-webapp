import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import { get } from "lodash";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Form,
  Input,
  Button,
  DatePicker,
  Radio,
  AutoComplete,
  Row,
  Col
} from "antd";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import moment from "moment";
import "moment/locale/pt-br";

import "./formSearch.css";

// Helpers
import { trim, slugify } from "../../../utils/helpers";
import generatePath from "../../../utils/generatePath";
import localeDatepPicker from "../../../utils/localeDatepPicker";

// Actions
import {
  setType,
  setPlace,
  autocomplete,
  reversePlace,
  setDate,
  setTime
} from "../actions";

const FormItem = Form.Item;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const { Option } = AutoComplete;

class FormSearch extends Component {
  static propTypes = {
    form: PropTypes.shape().isRequired,
    setType: PropTypes.func.isRequired,
    setPlace: PropTypes.func.isRequired,
    autocomplete: PropTypes.func.isRequired,
    reversePlace: PropTypes.func.isRequired,
    setDate: PropTypes.func.isRequired
  };

  state = {
    pickup: "",
    returnPlace: "",
    returnDateOpen: false,
    ida: null,
    volta: null,
    isValidOrigin: false,
    isValidDestination: false
  };

  componentDidMount() {
    this.props.form.validateFields();
  }

  onChangeType = e => {
    const type = e.target.value;
    this.props.setType(type);
  };

  renderOption = (place, placeType) => (
    <Option key={`${placeType}-${place.id}`} value={place.name} place={place}>
      {place.name}
    </Option>
  );

  handleSelect = (value, option, placeType) => {
    const {
      props: { place }
    } = option;

    this.props.setPlace(place, placeType);

    if (placeType === "pickup") this.setState({ isValidOrigin: true });
    else this.setState({ isValidDestination: true });
  };

  _loadAutocomplete(input) {
    if (this.searching) {
      clearTimeout(this.searching);
    }

    this.searching = setTimeout(() => {
      this.props.autocomplete(input);
    }, 300);
  }

  handleSearch = (text, placeType) => {
    const { searchTerm } = this.state;
    const term = trim(text);
    const slugT = slugify(text);

    if (placeType === "pickup") this.setState({ isValidOrigin: false });
    else this.setState({ isValidDestination: false });

    if (!term) {
      this.setState({ ...this.state, results: [] });
    } else if (term.length > 2 && slugT !== searchTerm) {
      this.setState({ ...this.state, searchTerm: slugT });
      this._loadAutocomplete(term);
    }
  };

  reversePlace = () => {
    this.setState(({ pickup, returnPlace }) => ({
      pickup: returnPlace,
      returnPlace: pickup
    }));

    this.props.reversePlace();
  };

  handlePickupDate = (date, dateString) => {
    const formatedDate = date.format("YYYY-MM-DD");

    this.setState({ ida: date });
    this.setState({ volta: date });
    this.props.form.setFieldsValue({ ida: dateString });
    this.props.setDate(formatedDate, "pickup");
  };

  handleReturnDate = (date, dateString) => {
    const formatedDate = date.format("YYYY-MM-DD");

    this.setState({ volta: date });
    this.props.form.setFieldsValue({ volta: dateString });
    this.props.setDate(formatedDate, "return");
  };

  handlePickupDateOpen = open =>
    !open && this.setState({ returnDateOpen: true });

  handleReturnDateOpen = open => this.setState({ returnDateOpen: open });

  disabledPickupDate = date =>
    date && moment(date).endOf("day") < moment().endOf("day");

  disabledReturnDate = date => {
    const { ida } = this.state;

    if (!date || !ida) return moment(date).endOf("day") < moment().endOf("day");

    return date < ida;
  };

  increaseCounter = (e, type) => {
    e.preventDefault();
    const { pickup_time, return_time } = this.props;

    if (type === "pickup_time" && pickup_time < 24)
      this.props.setTime(pickup_time + 1, "pickup");

    if (type === "return_time" && return_time < 24)
      this.props.setTime(return_time + 1, "return");
  };

  decreaseCounter = (e, type) => {
    e.preventDefault();
    const { pickup_time, return_time } = this.props;

    if (type === "pickup_time" && pickup_time > 0)
      this.props.setTime(pickup_time - 1, "pickup");

    if (type === "return_time" && return_time > 0)
      this.props.setTime(return_time - 1, "return");
  };

  handleSubmit = e => {
    e.preventDefault();
    const {
      pickup_location,
      return_location,
      pickup_date,
      return_date,
      pickup_time,
      return_time,
      match,
      history
    } = this.props;
    const matchParams = get(match, "params", {});

    const userLang = navigator.language || navigator.userLanguage;
    const lang = userLang.split("-");

    const latam = {
      AR: "ARS",
      BO: "BOB",
      BR: "BRL",
      CL: "CLP",
      CO: "COP",
      CR: "CRC",
      CU: "CUC",
      EC: "USD",
      SV: "USD",
      GT: "CTQ",
      HT: "HTG",
      HN: "HLN",
      MX: "MXN",
      NI: "NIO",
      PA: "PAB",
      PY: "PYG",
      PE: "PEN",
      DO: "DOP",
      UY: "UYU",
      VE: "VEF"
    };

    const moeda = lang[1];
    let currencyMoney = "";

    if (latam[`${moeda}`]) {
      currencyMoney = latam[`${moeda}`];
    } else {
      currencyMoney = "USD";
    }

    this.props.form.validateFields(err => {
      if (!err) {
        const url = generatePath(
          "/aluguel-de-carros/rent-a-car/:origin_date/:origin_time/:return_date/:return_time/:origin/:origin_description/:origin_city/:origin_state/:return/:return_description/:return_city/:return_state/:country/:language/:currency/",
          {
            ...matchParams,

            origin_date: pickup_date,
            origin_time: pickup_time,
            return_date,
            return_time,

            origin: pickup_location.id,
            origin_description: pickup_location.name,
            origin_city: pickup_location.city,
            origin_state: pickup_location.state,

            country: pickup_location.city,
            language: lang[0],
            currency: currencyMoney,

            return: return_location.id,
            return_description: return_location.name,
            return_city: return_location.city,
            return_state: return_location.state
          }
        );

        history.push(url);
      }
    });
  };

  hasErrors = fieldsError =>
    Object.keys(fieldsError).some(field => fieldsError[field]);

  render() {
    const {
      type,
      autocomplete_results,
      pickup_time,
      return_time,
      form: { getFieldDecorator, getFieldsError }
    } = this.props;
    const {
      returnDateOpen,
      isValidOrigin,
      isValidDestination,
      volta
    } = this.state;

    const samePlace = type === "same";

    return (
      <Form
        className="search-form form-locacao"
        layout="inline"
        onSubmit={this.handleSubmit}
      >
        <h1>
          <FormattedMessage id="buscaLocacaoTitle" />
        </h1>
        <Row>
          <FormItem className="field-filtro">
            {getFieldDecorator("radio-button", {
              initialValue: type,
              onChange: this.onChangeType
            })(
              <RadioGroup>
                <RadioButton value="same">
                  <FormattedMessage id="buscaLocacaoFiltro1" />
                </RadioButton>
                <RadioButton value="different">
                  <FormattedMessage id="buscaLocacaoFiltro2" />
                </RadioButton>
              </RadioGroup>
            )}
          </FormItem>
        </Row>

        <Row>
          <Col xs={24} md={!samePlace ? 4 : 10}>
            <FormItem
              validateStatus=""
              help=""
              className={`${!samePlace ? "field-big" : "field-extra"}`}
            >
              {getFieldDecorator("pickup", {
                rules: [{ required: true }]
              })(
                <div>
                  <label className="labelMenos">
                    <FormattedMessage id="buscaLocacaoRetirada" />
                  </label>
                  <AutoComplete
                    value={this.state.pickup}
                    placeholder={<FormattedMessage id="buscaLocacaoRetirada" />}
                    dataSource={autocomplete_results.map(place =>
                      this.renderOption(place, "pickup")
                    )}
                    onChange={value => this.setState({ pickup: value })}
                    onSearch={value => this.handleSearch(value, "pickup")}
                    onSelect={(value, option) =>
                      this.handleSelect(value, option, "pickup")
                    }
                  />
                </div>
              )}
            </FormItem>
          </Col>

          {!samePlace && (
            <Col xs={24} md={6}>
              <Fragment>
                <Row>
                  <Col xs={24} md={4} className="swap-button-wrapper">
                    <button
                      className="swap-button"
                      type="button"
                      onClick={this.reversePlace}
                    />
                  </Col>

                  <Col xs={24} md={20}>
                    <FormItem validateStatus="" help="" className="field-big">
                      {getFieldDecorator("return", {
                        rules: [{ required: true }]
                      })(
                        <div>
                          <label className="labelMenos">
                            <FormattedMessage id="buscaLocacaoDevolucao" />
                          </label>
                          <AutoComplete
                            value={this.state.returnPlace}
                            placeholder={
                              <FormattedMessage id="buscaLocacaoDevolucao" />
                            }
                            dataSource={autocomplete_results.map(place =>
                              this.renderOption(place, "return")
                            )}
                            onChange={value =>
                              this.setState({ returnPlace: value })
                            }
                            onSearch={value =>
                              this.handleSearch(value, "return")
                            }
                            onSelect={(value, option) =>
                              this.handleSelect(value, option, "return")
                            }
                          />
                        </div>
                      )}
                    </FormItem>
                  </Col>
                </Row>
              </Fragment>
            </Col>
          )}

          <Col xs={24} md={4}>
            <FormItem validateStatus="" help="" className="field-medium">
              {getFieldDecorator("ida", {
                rules: [{ required: true }]
              })(
                <div>
                  <label>
                    <FormattedMessage id="buscaLocacaoDataIda" />
                  </label>
                  <DatePicker
                    locale={localeDatepPicker}
                    format="DD/MMM/YY"
                    placeholder="Data"
                    onChange={this.handlePickupDate}
                    onOpenChange={this.handlePickupDateOpen}
                    disabledDate={this.disabledPickupDate}
                    className="form-input"
                    allowClear={false}
                  />
                </div>
              )}
            </FormItem>
          </Col>

          <Col xs={24} md={3}>
            <FormItem className="field-count">
              <label>
                <FormattedMessage id="buscaLocacaoHorarioIda" />
              </label>
              <span className="campo">
                <button onClick={e => this.decreaseCounter(e, "pickup_time")}>
                  -
                </button>
                <Input type="text" value={`${pickup_time}h`} readOnly />
                <button onClick={e => this.increaseCounter(e, "pickup_time")}>
                  +
                </button>
              </span>
            </FormItem>
          </Col>

          <Col xs={24} md={4}>
            <FormItem validateStatus="" help="" className="field-medium">
              {getFieldDecorator("volta", {
                rules: [{ required: true }]
              })(
                <div>
                  <label>
                    <FormattedMessage id="buscaLocacaoDataVolta" />
                  </label>
                  <DatePicker
                    locale={localeDatepPicker}
                    format="DD/MMM/YY"
                    placeholder="Data"
                    onChange={this.handleReturnDate}
                    onOpenChange={this.handleReturnDateOpen}
                    disabledDate={this.disabledReturnDate}
                    className="form-input"
                    allowClear={false}
                    value={volta ? volta : ""}
                    open={returnDateOpen}
                  />
                </div>
              )}
            </FormItem>
          </Col>

          <Col xs={24} md={3}>
            <FormItem className="field-count">
              <label>
                <FormattedMessage id="buscaLocacaoHorarioVolta" />
              </label>
              <span className="campo">
                <button onClick={e => this.decreaseCounter(e, "return_time")}>
                  -
                </button>
                <Input type="text" value={`${return_time}h`} readOnly />
                <button onClick={e => this.increaseCounter(e, "return_time")}>
                  +
                </button>
              </span>
            </FormItem>
          </Col>
        </Row>

        <Row>
          <FormItem className="field-full">
            <Button
              type="primary"
              htmlType="submit"
              className="btn-comparar"
              disabled={
                this.hasErrors(getFieldsError()) ||
                !isValidOrigin ||
                (!samePlace && !isValidDestination)
              }
            >
              <FormattedMessage id="buscaLocacaoBotao" />
            </Button>
          </FormItem>
        </Row>
      </Form>
    );
  }
}

const WrappedFormSearch = Form.create()(FormSearch);

export default withRouter(
  connect(
    state => ({ ...state.searchByRent }),
    dispatch =>
      bindActionCreators(
        { setType, setPlace, autocomplete, reversePlace, setDate, setTime },
        dispatch
      )
  )(WrappedFormSearch)
);
