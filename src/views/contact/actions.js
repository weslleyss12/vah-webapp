import { fetchDefaultOptions } from "../../utils/fetchDefaultOptions";
import Config from "../../config";

import { ACTION_CONTACT, ACTION_CONTACT_RESET } from "../../main/constants";

function save(values) {
  const url = `${Config.API_BASE_URL}/contact`;

  return fetch(url, fetchDefaultOptions("POST", values))
    .then(response => response.json())
    .then(responseJson => responseJson)
    .catch(error => error.message);
}

export function saveContact(values) {
  return {
    type: ACTION_CONTACT,
    payload: save(values)
  };
}

export function resetContact(resetDone) {
  return {
    type: ACTION_CONTACT_RESET,
    payload: resetDone
  };
}
