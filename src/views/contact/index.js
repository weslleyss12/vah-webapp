import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";
import MetaTags from "react-meta-tags";
import { Form, Input, Button, Checkbox, Select, Row, Col } from "antd";
import TextArea from "antd/lib/input/TextArea";
import PropTypes from "prop-types";

// Actions
import { saveContact, resetContact } from "./actions";

import Container from "../../components/container";
import Header from "../../components/header";
import Footer from "../../components/footer";
import Breadcrumb from "../../components/breadcrumb";
import logoVah from "../../assets/images/core/logo-transition.png";

import { FormattedMessage } from "react-intl";

const FormItem = Form.Item;
const { Option } = Select;

class Contact extends Component {
  static propTypes = {
    form: PropTypes.shape().isRequired,
    saveContact: PropTypes.func.isRequired,
    resetContact: PropTypes.func.isRequired,
    done: PropTypes.bool.isRequired,
    pending: PropTypes.bool.isRequired
  };

  state = {};

  componentDidMount = () => {
    this.props.resetContact(true);

    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { name, email, reason, message, newsletter } = values;

        this.props.saveContact({
          name,
          email,
          reason,
          message,
          optin: newsletter
        });
      }
    });
  };

  handleSelectChange = value => {
    console.log(value);
  };

  hasErrors = fieldsError =>
    Object.keys(fieldsError).some(field => fieldsError[field]);

  render() {
    const {
      form: {
        getFieldDecorator,
        getFieldsError,
        getFieldError,
        isFieldTouched
      },
      done,
      pending
    } = this.props;

    // Only show error after a field is touched.
    const nameError = isFieldTouched("name") && getFieldError("name");
    const emailError = isFieldTouched("email") && getFieldError("email");
    const newsletterError =
      isFieldTouched("newsletter") && getFieldError("newsletter");
    const reasonError = isFieldTouched("reason") && getFieldError("reason");
    const messageError = isFieldTouched("message") && getFieldError("message");

    let lang = localStorage.getItem("idioma");
    let placeNome = "";
    let placeEmail = "";
    let placeMensagem = "";

    if (!lang || lang == "pt-BR") {
      placeNome = "Nome";
      placeEmail = "E-mail";
      placeMensagem = "Mensagem";
    } else if (lang == "es-gl") {
      placeNome = "Nombre";
      placeEmail = "E-mail";
      placeMensagem = "Mensaje";
    }

    return (
      <div className="page-contact">
        <MetaTags>
          <meta name="title" content="Entre em contato com a gente! | Vah" />
          <meta
            name="description"
            content="Ei, precisa de ajuda? Entre em contato com a equipe Vah. Vamos ficar felizes em te ajudar na sua próxima viagem!"
          />
          <meta name="keywords" content="Vah, Fale conosco Vah" />
          {/* <meta property="og:title" content="MyApp" />
          <meta property="og:image" content="path/to/image.jpg" /> */}
        </MetaTags>

        <Breadcrumb>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/vah">vah</Link>
          </li>
          <li>Contato</li>
        </Breadcrumb>
        <Header />

        <Container>
          <div className="container-sobre">
            {done ? (
              <Row type="flex" justify="center" align="center">
                <Col md={24} lg={16}>
                  <p className="logo-transition">
                    <img src={logoVah} width="120" alt="VAH" />
                  </p>
                </Col>

                <Col md={24} lg={15}>
                  <h2>
                    Sua mensagem foi enviada com sucesso. <br />
                    {""} Obrigado pelo contato, responderemos em breve.
                  </h2>
                </Col>

                <Col md={24} lg={15} className="colCenter">
                  <Link to="/">
                    <Button
                      type="primary"
                      htmlType="submit"
                      className="login-form-button"
                    >
                      Voltar
                    </Button>
                  </Link>
                </Col>
              </Row>
            ) : (
              <Fragment>
                <Row type="flex" justify="center" align="center">
                  <Col md={24} lg={16}>
                    <p className="logo-transition">
                      <img src={logoVah} width="120" alt="VAH" />
                    </p>
                  </Col>

                  <Col md={24} lg={15}>
                    <h2>
                      <FormattedMessage id="contatoTitle1" />
                      <br />
                      <FormattedMessage id="contatoTitle2" />
                    </h2>
                  </Col>
                </Row>

                <Row type="flex" justify="center" align="top">
                  <Col md={24} lg={14}>
                    <Form onSubmit={this.handleSubmit} className="login-form">
                      <legend>
                        <FormattedMessage id="contatoLegenda" />
                      </legend>
                      <FormItem
                        validateStatus={nameError ? "error" : ""}
                        help={nameError || ""}
                      >
                        {getFieldDecorator("name", {
                          rules: [
                            { required: true, message: "Campo obrigatório!" }
                          ]
                        })(<Input placeholder={placeNome} />)}
                      </FormItem>

                      <FormItem
                        validateStatus={emailError ? "error" : ""}
                        help={emailError || ""}
                      >
                        {getFieldDecorator("email", {
                          rules: [
                            {
                              type: "email",
                              message: "O E-mail informado não é valido."
                            },
                            { required: true, message: "Campo obrigatório!" }
                          ]
                        })(<Input type="email" placeholder={placeEmail} />)}
                      </FormItem>

                      <FormItem
                        className="inputCheck"
                        validateStatus={newsletterError ? "error" : ""}
                        help={newsletterError || ""}
                      >
                        {getFieldDecorator("newsletter", {
                          valuePropName: "checked"
                        })(
                          <Checkbox>
                            <FormattedMessage id="contatoNovidade" />
                          </Checkbox>
                        )}
                      </FormItem>

                      <FormItem
                        validateStatus={reasonError ? "error" : ""}
                        help={reasonError || ""}
                      >
                        {getFieldDecorator("reason", {
                          rules: [
                            { required: true, message: "Campo obrigatório!" }
                          ]
                        })(
                          <Select
                            placeholder={
                              <FormattedMessage id="contatoMotivo" />
                            }
                            onChange={this.handleSelectChange}
                          >
                            <Option value="Sugestão">
                              <FormattedMessage id="contatoMotivoOp1" />
                            </Option>
                            <Option value="Reclamação">
                              <FormattedMessage id="contatoMotivoOp2" />
                            </Option>
                            <Option value="Elogio">
                              <FormattedMessage id="contatoMotivoOp3" />
                            </Option>
                            <Option value="Dúvidas">
                              <FormattedMessage id="contatoMotivoOp4" />
                            </Option>
                          </Select>
                        )}
                      </FormItem>

                      <FormItem
                        validateStatus={messageError ? "error" : ""}
                        help={messageError || ""}
                      >
                        {getFieldDecorator("message", {
                          rules: [
                            { required: true, message: "Campo obrigatório!" }
                          ]
                        })(
                          <TextArea
                            type="textarea"
                            placeholder={placeMensagem}
                          />
                        )}
                      </FormItem>

                      <FormItem>
                        <Button
                          type="primary"
                          htmlType="submit"
                          className="login-form-button"
                          loading={pending}
                          disabled={this.hasErrors(getFieldsError())}
                        >
                          Enviar
                        </Button>
                      </FormItem>
                    </Form>
                  </Col>
                </Row>
              </Fragment>
            )}
          </div>
        </Container>
        <Footer />
      </div>
    );
  }
}

const WrappedContact = Form.create()(Contact);

export default connect(
  state => ({ ...state.contact }),
  dispatch =>
    bindActionCreators(
      {
        saveContact,
        resetContact
      },
      dispatch
    )
)(WrappedContact);
