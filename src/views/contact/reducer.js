import {
  ACTION_CONTACT_PENDING,
  ACTION_CONTACT_FULFILLED,
  ACTION_CONTACT_REJECTED,
  ACTION_CONTACT_RESET
} from "../../main/constants";

const INITIAL_STATE = {
  pending: false,
  error: null,
  done: false
};

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case ACTION_CONTACT_PENDING:
      return { ...state, pending: true };

    case ACTION_CONTACT_FULFILLED:
      if (payload.status) {
        return { ...state, pending: false, done: true };
      }

      return { ...state, pending: false, error: payload.errors };

    case ACTION_CONTACT_REJECTED:
      return { ...state, error: payload, pending: false };

    case ACTION_CONTACT_RESET:
      return { ...INITIAL_STATE, done: payload ? false : state.done };

    default:
      return state;
  }
};
