import { fetchDefaultOptions } from "../../utils/fetchDefaultOptions";
import Config from "../../config";

import {
  ACTION_SEARCH_BY_ADDRESS_SET_ADDRESS,
  ACTION_SEARCH_BY_ADDRESS_REVERSE_ADDRESS,
  ACTION_SEARCH_BY_ADDRESS_RESET,
  ACTION_SEARCH_BY_ADDRESS_FETCH,
  ACTION_SEARCH_BY_ADDRESS_CURRENT_LOCATION,
  ACTION_SEARCH_BY_ADDRESS_SET_FILTERS,
  ACTION_SEARCH_BY_ADDRESS_CLEAR_ESTIMATES
} from "../../main/constants";

function actionSearch(values) {
  const url = `${Config.API_BASE_URL}/estimates/urban`;

  return fetch(url, fetchDefaultOptions("POST", values))
    .then(response => response.json())
    .then(responseJson => responseJson)
    .catch(error => error.message);
}

export function searchUrban(values) {
  return {
    type: ACTION_SEARCH_BY_ADDRESS_FETCH,
    payload: actionSearch(values)
  };
}

export function setAddress(address, type) {
  return {
    type: ACTION_SEARCH_BY_ADDRESS_SET_ADDRESS,
    payload: { address, type }
  };
}

export function reverseAddress() {
  return {
    type: ACTION_SEARCH_BY_ADDRESS_REVERSE_ADDRESS
  };
}

export function setCurrentLocation(currentLocation) {
  return {
    type: ACTION_SEARCH_BY_ADDRESS_CURRENT_LOCATION,
    payload: currentLocation
  };
}

export function setFilters(filters) {
  return {
    type: ACTION_SEARCH_BY_ADDRESS_SET_FILTERS,
    payload: filters
  };
}

export function clearEstimates() {
  return {
    type: ACTION_SEARCH_BY_ADDRESS_CLEAR_ESTIMATES
  };
}

export function resetSearch() {
  return {
    type: ACTION_SEARCH_BY_ADDRESS_RESET
  };
}
