import {
  ACTION_SEARCH_BY_ADDRESS_SET_ADDRESS,
  ACTION_SEARCH_BY_ADDRESS_REVERSE_ADDRESS,
  ACTION_SEARCH_BY_ADDRESS_RESET,
  ACTION_SEARCH_BY_ADDRESS_FETCH_PENDING,
  ACTION_SEARCH_BY_ADDRESS_FETCH_FULFILLED,
  ACTION_SEARCH_BY_ADDRESS_FETCH_REJECTED,
  ACTION_SEARCH_BY_ADDRESS_CURRENT_LOCATION,
  ACTION_SEARCH_BY_ADDRESS_SET_FILTERS,
  ACTION_SEARCH_BY_ADDRESS_CLEAR_ESTIMATES
} from '../../main/constants'

const INITIAL_STATE = {
  current_location: null,
  origin: null,
  destination: null,
  home: null,
  work: null,
  pending: true,
  error: null,
  results: null,
  full_estimates: [],
  female: 1,
  filters: {}
}

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case ACTION_SEARCH_BY_ADDRESS_SET_ADDRESS:
      return { ...state, [payload.type]: payload.address }

    case ACTION_SEARCH_BY_ADDRESS_REVERSE_ADDRESS:
      return { ...state, origin: state.destination, destination: state.origin }

    case ACTION_SEARCH_BY_ADDRESS_FETCH_PENDING:
      return { ...state, pending: true, results: null, full_estimates: [], filters: {} }

    case ACTION_SEARCH_BY_ADDRESS_FETCH_FULFILLED:
      if (payload.status) {
        const results2 = filterItems(state.filters, state.female, null, payload.data)
        return { ...state, pending: false, results: results2, full_estimates: payload.data.estimates }
      }

      return { ...state, pending: false }

    case ACTION_SEARCH_BY_ADDRESS_FETCH_REJECTED:
      return { ...state, pending: false, error: payload }

    case ACTION_SEARCH_BY_ADDRESS_CURRENT_LOCATION:
      return { ...state, current_location: payload }

    case ACTION_SEARCH_BY_ADDRESS_SET_FILTERS:
      const { female } = payload

      // TODO: Verificar a necessidade do `model`
      // model.filters.set('urban', { female })
      delete payload.female

      const results = filterItems(payload, female, state)

      return { ...state, filters: payload, female, results, pending: false }

    case ACTION_SEARCH_BY_ADDRESS_CLEAR_ESTIMATES:
      return { ...state, results: { ...state.results, estimates: [] }, pending: true }

    case ACTION_SEARCH_BY_ADDRESS_RESET:
      return INITIAL_STATE

    default:
      return state
  }
}

const filterItems = (filters, female, state, localResults) => {
  const p = state ? state.results.players : localResults.players
  const e = state ? state.full_estimates : localResults.estimates
  const r = state ? state.results : localResults

  const compare = (a, b) => {
    if (a.time === null) {
      return 1
    }
    else if (b.time === null) {
      return -1
    }
    else if (a.time < b.time) {
      return -1
    }
    else if (a.time > b.time) {
      return 1
    }

    return 0
  }

  const estimates = e.filter(estimate => {
    return (
      (filters.companies ? filters.companies.indexOf(estimate.player) !== -1 : true) &&
      (female === 0 ? p[estimate.player].women_only === 0 : true)
    )
  })

  if (filters.order && (filters.order.toLowerCase() === 'tempo de chegada' || filters.order.toLowerCase() === 'estimated time of arrival')) {
    estimates.sort(compare)
  }

  return { ...r, estimates }
}
