/* global google */

import React, { Fragment, Component } from "react";
import { get } from "lodash";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import { Row, Col, Form, Button, AutoComplete, Input, Icon } from "antd";
import PlacesAutocomplete, {
  geocodeByPlaceId,
  getLatLng
} from "react-places-autocomplete";
import PropTypes from "prop-types";

import "./formSearch.css";
import Config from "../../../config";
import Geocoder from "../../../components/react-native-geocoding";

import googleImg from "../../../assets/images/google.png";

// Helpers
import {
  formattingPlaceApiResult,
  formattingCurrentLocation,
  getFromPlaceApiResult,
  getLocation
} from "../../../utils/helpers";
import generatePath from "../../../utils/generatePath";

// Actions
import { setAddress, reverseAddress } from "../actions";

const FormItem = Form.Item;
const { Option } = AutoComplete;

class FormSearch extends Component {
  static propTypes = {
    form: PropTypes.shape().isRequired,
    setAddress: PropTypes.func.isRequired,
    reverseAddress: PropTypes.func.isRequired,
    match: PropTypes.shape().isRequired,
    origin: PropTypes.shape(),
    destination: PropTypes.shape(),
    history: PropTypes.shape().isRequired
  };

  static defaultProps = {
    origin: null,
    destination: null
  };

  constructor(props) {
    super(props);

    // Geocoder
    Geocoder.setApiKey(Config.GOOGLE_PLACES_API);

    this.state = {
      isValidOrigin: false,
      isValidDestination: false,
      loadingCurrentLocation: false,
      current_location: null
    };
  }

  componentDidMount = () => {
    this.props.form.validateFields();
    this.requestUserLocation();
  };

  requestUserLocation = () => {
    getLocation((status, coords) => {
      if (status) {
        this.setState({
          current_location: coords
        });
      }
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { origin, destination, match, history } = this.props;
    const matchParams = get(match, "params", {});

    const userLang = navigator.language || navigator.userLanguage;
    const lang = userLang.split("-");

    const latam = {
      AR: "ARS",
      BO: "BOB",
      BR: "BRL",
      CL: "CLP",
      CO: "COP",
      CR: "CRC",
      CU: "CUC",
      EC: "USD",
      SV: "USD",
      GT: "CTQ",
      HT: "HTG",
      HN: "HLN",
      MX: "MXN",
      NI: "NIO",
      PA: "PAB",
      PY: "PYG",
      PE: "PEN",
      DO: "DOP",
      UY: "UYU",
      VE: "VEF"
    };

    const moeda = lang[1];
    let currencyMoney = "";

    if (latam[`${moeda}`]) {
      currencyMoney = latam[`${moeda}`];
    } else {
      currencyMoney = "USD";
    }

    this.props.form.validateFields(err => {
      if (!err) {
        const url = generatePath(
          "/aplicativo-de-transporte/preco-e-cupom-de-desconto/:start_latitude/:start_longitude/:start_address/:start_country/:start_city/:start_state/:start_zip_code/:end_latitude/:end_longitude/:end_address/:end_city/:end_state/:end_zip_code/:country/:language/:currency/",
          {
            ...matchParams,

            start_address: origin.street,
            start_state: origin.state,
            start_city: origin.city,
            start_country: origin.country,
            start_zip_code: origin.zipcode || "cep",
            start_latitude: origin.latitude,
            start_longitude: origin.longitude,

            end_address: destination.street,
            end_state: destination.state,
            end_city: destination.city,
            end_zip_code: destination.zipcode || "cep",
            end_latitude: destination.latitude,
            end_longitude: destination.longitude,

            country: lang[1] || "BR",
            language: lang[0],
            currency: currencyMoney
          }
        );

        history.push(url);
      }
    });
  };

  _handleSearch = (value, placeType) => {
    this.props.form.setFieldsValue({ [placeType]: value });

    if (placeType === "origin") this.setState({ isValidOrigin: false });
    else this.setState({ isValidDestination: false });
  };

  _handleSearchPress = (_value, option, placeType) => {
    const {
      props: { suggestion }
    } = option;
    const address = formattingPlaceApiResult(suggestion);

    if (address.id) {
      geocodeByPlaceId(address.id)
        .then(result => {
          address.zipcode = getFromPlaceApiResult(result[0], "postal_code");
          address.country = getFromPlaceApiResult(result[0], "country");

          return getLatLng(result[0]);
        })
        .then(({ lat, lng }) => {
          address.latitude = lat;
          address.longitude = lng;

          this.props.setAddress(address, placeType);

          if (placeType === "origin") this.setState({ isValidOrigin: true });
          else this.setState({ isValidDestination: true });
        })
        .catch(error => console.error(error));
    }
  };

  renderFunc = (
    placeType,
    { getInputProps, getSuggestionItemProps, suggestions }
  ) => (
      <AutoComplete
        {...getInputProps()}
        placeholder={<FormattedMessage id="buscaUrbanoPlace" />}
        onChange={value => getInputProps().onChange({ target: { value } })}
        onSelect={(value, option) =>
          this._handleSearchPress(value, option, placeType)
        }
        dataSource={suggestions.map(suggestion => (
          <Option
            {...getSuggestionItemProps(suggestion)}
            value={suggestion.description}
            suggestion={suggestion}
          >
            {suggestion.description}
          </Option>
        )).concat([
          <Option disabled key="all" className="show-all">
            <p className="google-direitos">powered by <img src={googleImg} alt="Google" width="50" /></p>
          </Option>,
        ])}
      >
        <Input
          suffix={
            placeType === "origin" &&
            (!this.state.loadingCurrentLocation ? (
              <Button
                type="button"
                className="btn-location"
                onClick={this.getUserLocation}
              />
            ) : (
                <Icon type="loading" />
              ))
          }
        />
      </AutoComplete>
    );

  getUserLocation = () => {
    this.setState({ loadingCurrentLocation: true });

    getLocation((status, coords) => {
      if (status) {
        Geocoder.getFromLatLng(coords.latitude, coords.longitude).then(
          json => {
            const first = json.results[0].address_components;
            const address = formattingCurrentLocation(first);

            address.latitude = coords.latitude;
            address.longitude = coords.longitude;
            address.type = "history";

            this.props.setAddress(address, "origin");

            this.props.form.setFieldsValue({
              origin: `${address.street} - ${address.neighborhood}, ${
                address.city
                } - ${address.state}`
            });

            this.setState({
              loadingCurrentLocation: false,
              isValidOrigin: true
            });
          },
          error => {
            console.log(error);
            this.setState({ loadingCurrentLocation: false });
          }
        );
      } else {
        this.setState({ loadingCurrentLocation: false });
      }
    });
  };

  reverseAddress = () => {
    const { getFieldsValue, setFieldsValue } = this.props.form;
    const fields = getFieldsValue();

    setFieldsValue({
      origin: fields.destination,
      destination: fields.origin
    });

    this.props.reverseAddress();
  };

  hasErrors = fieldsError =>
    Object.keys(fieldsError).some(field => fieldsError[field]);

  render() {
    const {
      form: { getFieldsError, getFieldDecorator }
    } = this.props;
    const { isValidOrigin, isValidDestination, current_location } = this.state;

    const searchOptions = current_location
      ? {
        location: new google.maps.LatLng(
          current_location.latitude,
          current_location.longitude
        ),
        radius: 2000
      }
      : {
        componentRestrictions: { country: "br" }
      };

    return (
      <Row type="flex" align="center">
        <Col span={22}>
          <Form
            className="search-form form-urbano"
            layout="inline"
            onSubmit={this.handleSubmit}
          >
            <h1>
              <FormattedMessage id="buscaUrbanoTitle" />
            </h1>

            <Row
              gutter={10}
              type="flex"
              justify="middle"
              className="search-row"
            >
              <Col xs={24} md={11}>
                <FormItem
                  validateStatus=""
                  help=""
                  label={<FormattedMessage id="buscaUrbanoPartida" />}
                  className="field-big"
                >
                  {getFieldDecorator("origin", {
                    rules: [{ required: true }]
                  })(
                    <PlacesAutocomplete
                      onChange={value => this._handleSearch(value, "origin")}
                      searchOptions={searchOptions}
                      className="field-big-input"
                    >
                      {(...rest) => this.renderFunc("origin", ...rest)}
                    </PlacesAutocomplete>
                  )}
                </FormItem>
                {/* <p className="google-direitos">powered by <img src={googleImg} alt="Google" width="50" /></p> */}
              </Col>

              <Col xs={24} md={2} className="swap-button-wrapper">
                <button
                  className="swap-button"
                  type="button"
                  onClick={this.reverseAddress}
                />
              </Col>

              <Col xs={24} md={11}>
                <FormItem
                  validateStatus=""
                  label={<FormattedMessage id="buscaUrbanoDestino" />}
                  help=""
                  className="field-big no-margin"
                >
                  {getFieldDecorator("destination", {
                    rules: [{ required: true }]
                  })(
                    <PlacesAutocomplete
                      onChange={value =>
                        this._handleSearch(value, "destination")
                      }
                      searchOptions={searchOptions}
                    >
                      {(...rest) => this.renderFunc("destination", ...rest)}
                    </PlacesAutocomplete>
                  )}
                </FormItem>
              </Col>
            </Row>

            <FormItem className="field-full">
              <Button
                type="primary"
                htmlType="submit"
                className="btn-comparar"
                disabled={
                  this.hasErrors(getFieldsError()) ||
                  !isValidOrigin ||
                  !isValidDestination
                }
              >
                <FormattedMessage id="buscaUrbanoBotao" />
              </Button>
            </FormItem>
          </Form>
        </Col>
      </Row>
    );
  }
}

const WrappedFormSearch = Form.create()(FormSearch);

export default withRouter(
  connect(
    state => ({ ...state.searchByAddress }),
    dispatch => bindActionCreators({ setAddress, reverseAddress }, dispatch)
  )(WrappedFormSearch)
);
