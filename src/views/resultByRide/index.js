import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import MetaTags from "react-meta-tags";
import styled from "styled-components";
import { Row, Col, Card, Slider } from "antd";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import Breadcrumb from "../../components/breadcrumb";
import Header from "../../components/header";
import ResultHeader from "../../components/ResultHeader";
import Footer from "../../components/footer";
import Container from "../../components/container";
import SideBlue from "../../components/sideBlue";

import BoxResult from "./components/boxResult";

import AddressTransition from "../../components/transition";

import { FormattedMessage } from "react-intl";

import Config from "../../config";

// Actions
import { searchRide, setFilters } from "../searchByRide/actions";

const MainInfo = styled.section`
  padding: 20px 0;
`;

const TripInfo = styled(Card)`
  font-size: 18px;
  font-weight: 700;
  line-height: 20.4px;
  text-transform: uppercase;
  letter-spacing: 0.41px;
  border-radius: 5px;
  margin-bottom: 20px;
  color: #244a88;
  background-color: #e4e5ea;

  h1 {
    font-size: 18px;
    font-weight: 700;
    line-height: 20.4px;
    text-transform: uppercase;
    letter-spacing: 0.41px;
    color: rgb(36, 74, 136);
  }
`;

const RightCol = styled(Col)`
  text-align: right;
`;

const Form = styled.form`
  &.form-filter {
    padding-bottom: 10px;

    fieldset:nth-child(2) {
      margin-bottom: 10px;
    }
  }
`;

const PriceWrapper = styled.div`
  margin-right: 2rem;
`;

const PriceSlider = styled(Slider)`
  margin-top: 5px;
  margin-bottom: 0;

  .ant-slider-rail {
    height: 3px;
  }

  &:hover .ant-slider-track {
    background-color: #a2ea71;
  }

  .ant-slider-track {
    height: 3px;
    background-color: #a2ea71;
  }

  .ant-slider-handle {
    width: 18px;
    height: 18px;
    border: none;
    margin-left: -9px;
    margin-top: -8px;
    background-color: #a2ea71;
  }
`;

const PriceText = styled.span`
  font-size: 14px;
  font-weight: 400;
  line-height: 28.75px;
  letter-spacing: 0.35px;
  color: #ffffff;
  text-transform: uppercase;
`;

const PriceRight = PriceText.extend`
  color: #a2ea71;
`;

class ResultByRide extends Component {
  static propTypes = {
    match: PropTypes.shape().isRequired
  };

  constructor(props) {
    super(props);

    const timeRange = ["Manhã", "Tarde", "Noite"];

    this.state = {
      hour: timeRange,
      filteredHour: props.filters.hour || timeRange,
      hasTransition: true
    };
  }

  componentDidMount = () => {
    const {
      match: {
        params: {
          date,
          start_hour,
          seats,
          start_latitude,
          start_longitude,
          start_address,
          start_city,
          start_state,
          start_zip_code,
          end_latitude,
          end_longitude,
          end_address,
          end_city,
          end_state,
          end_zip_code
        }
      }
    } = this.props;

    this.props.searchRide({
      date,
      start_hour,
      seats,

      start_latitude,
      start_longitude,
      start_address,
      start_city,
      start_state,
      start_zip_code,

      end_latitude,
      end_longitude,
      end_address,
      end_city,
      end_state,
      end_zip_code,
      app_version: Config.APP_VERSION
    });
  };

  _handleHour = (hour, e) => {
    const active = e.target.checked;
    const filteredHour = this.state.filteredHour.concat([]);

    if (active && filteredHour.indexOf(hour) === -1) {
      filteredHour.push(hour);
    } else if (filteredHour.indexOf(hour) !== -1) {
      filteredHour.splice(filteredHour.indexOf(hour), 1);
    }

    this.setState({ ...this.state, filteredHour });
  };

  _renderTimeRange = () => {
    const { hour, filteredHour } = this.state;

    return hour.map(time => (
      <span className="field-holder" key={time}>
        <div className="switch-containter">
          <div className="switch">
            <input
              id={time}
              className="cmn-toggle cmn-toggle-round"
              type="checkbox"
              checked={filteredHour.indexOf(time) !== -1}
              onChange={enable => this._handleHour(time, enable)}
            />
            <label for={time} />
          </div>
          <span>{time}</span>
        </div>
      </span>
    ));
  };

  _renderItems = () => {
    const { results } = this.props;

    return results.estimates.map((item, index) => (
      <BoxResult key={index} item={item} />
    ));
  };

  _handlePrice(price) {
    this.setState({ ...this.state, price: parseInt(price) });
  }

  _handleApply = e => {
    e.preventDefault();

    const { setFilters, full_estimates: estimates } = this.props;
    const { hour, filteredHour, price } = this.state;

    if (
      hour.length === filteredHour.length &&
      price === estimates[estimates.length - 1].price
    ) {
      setFilters({});
    } else {
      setFilters({ hour: filteredHour, price });
    }

    // this.child.closeSide();
  };

  toggleTransition = () =>
    this.setState(({ hasTransition }) => ({ hasTransition: !hasTransition }));

  render() {
    const {
      results,
      pending,
      filters,
      full_estimates: estimates,
      match
    } = this.props;

    const { price } = this.state;
    const minValue = estimates.length > 0 && estimates[0].price;
    const maxValue =
      estimates.length > 0 && estimates[estimates.length - 1].price;

    if (results === null || pending)
      return (
        <AddressTransition
          results={results}
          pending={pending}
          toggleTransition={this.toggleTransition}
          activeTab="carona"
        />
      );

    return (
      <div className="page-carona">
        <MetaTags>
          <meta
            name="title"
            content="Site e Aplicativo de Carona: veja vagas disponíveis agora | Vah"
          />
          <meta
            name="description"
            content="Escolha seu destino e veja agora as vagas de carona compartilhada disponíveis. Viajar de carona ficou fácil!"
            content=""
          />
          <meta name="keywords" content="aplicativo de carona" />
          {/* <meta property="og:title" content="MyApp" />
          <meta property="og:image" content="path/to/image.jpg" /> */}
        </MetaTags>
        <Breadcrumb>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/carona-compartilhada">
              Vah <FormattedMessage id="menuCarona" />
            </Link>
          </li>
          <li>Resultado</li>
        </Breadcrumb>
        <Header />
        <ResultHeader infos={match.params} backUrl="/carona-compartilhada" />
        <MainInfo>
          <Container>
            <Row gutter={60}>
              <Col md={24} lg={7}>
                <SideBlue onRef={ref => (this.child = ref)}>
                  <h2 className="title-filter">
                    <FormattedMessage id="filtroCaronaTitle" />
                  </h2>
                  <Form className="form-filter" onSubmit={this._handleApply}>
                    <fieldset>
                      <legend className="filter-type">
                        <FormattedMessage id="filtroCaronaPeriodo" />
                      </legend>
                      {this._renderTimeRange()}
                    </fieldset>
                    <fieldset>
                      <legend className="filter-type">
                        <FormattedMessage id="filtroCaronaPreco" />
                      </legend>
                      {results &&
                        estimates.length > 0 && (
                          <PriceWrapper>
                            <PriceSlider
                              min={minValue}
                              max={maxValue}
                              defaultValue={filters.price || maxValue}
                              value={price || maxValue}
                              tipFormatter={value => `R$ ${value}`}
                              onChange={this._handlePrice}
                            />

                            <Row>
                              <Col span={12}>
                                <PriceText>R$ {minValue}</PriceText>
                              </Col>

                              <RightCol span={12}>
                                <PriceRight>R$ {maxValue}</PriceRight>
                              </RightCol>
                            </Row>
                          </PriceWrapper>
                        )}
                    </fieldset>
                    <div>
                      <button className="apply-filter">
                        <FormattedMessage id="filtroCaronaBtn" />
                      </button>
                    </div>
                  </Form>
                </SideBlue>
              </Col>
              <Col md={24} lg={17}>
                {(results === null || pending) && (
                  <p>Estamos pesquisando as melhores tarifas para você.</p>
                )}

                {results &&
                  results.estimates.length > 0 &&
                  !pending && (
                    <TripInfo>
                      <h1>
                        <Row>
                          <Col span={12}>
                            <FormattedMessage id="resultadoCaronaDuracao" />
                            <br />
                            {results.trip.duration
                              ? Math.ceil(results.trip.duration / 60)
                              : "-"}{" "}
                            H
                          </Col>
                          <RightCol span={12}>
                            <FormattedMessage id="resultadoCaronaApartir" />
                            <br />
                            R$ {results.estimates[0].price}
                          </RightCol>
                        </Row>
                      </h1>
                    </TripInfo>
                  )}

                {results &&
                  results.estimates.length === 0 &&
                  !pending && <p>Nenhum resultado encontrado</p>}

                {results &&
                  results.estimates.length > 0 &&
                  !pending &&
                  this._renderItems()}
              </Col>
            </Row>
          </Container>
        </MainInfo>
        <Footer />
      </div>
    );
  }
}

export default connect(
  state => ({ ...state.searchByRide }),
  dispatch => bindActionCreators({ searchRide, setFilters }, dispatch)
)(ResultByRide);
