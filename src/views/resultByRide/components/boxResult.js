import React from "react";
import moment from "moment";

import "./boxResult.css";
import { calendarLocale } from "../../../utils/helpers";

import { Row, Col } from "antd";

import { FormattedMessage } from "react-intl";

const BoxResult = ({ item }) => {
  return (
    <div className="ItemResult itemRide">
      <div>
        <Row>
          <Col xs={24} md={4} className="col-data">
            <p>
              {moment(item.departure_at, "YYYY-MM-DD hh:mm:ss").format("DD")} /
              {
                calendarLocale.pt.monthNamesShort[
                  moment(item.departure_at, "YYYY-MM-DD hh:mm:ss").format(
                    "MM"
                  ) - 1
                ]
              }
            </p>
            <span />
          </Col>
          <Col xs={24} md={14}>
            <Row className="RowInfo topo">
              <Col span={24}>
                <Row>
                  <Col xs={22} md={16}>
                    <p className="TextGray">
                      <FormattedMessage id="buscaCaronaSaindo" />
                    </p>
                    <p className="TextBlue">{item.departure_address}</p>
                  </Col>
                  <Col xs={22} md={8}>
                    <p className="TextGray">&nbsp;</p>
                    <p className="TextBlue">
                      <small>
                        {moment(
                          item.departure_at,
                          "YYYY-MM-DD HH:mm:ss"
                        ).format("HH[H]mm")}
                      </small>
                    </p>
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row className="RowInfo">
              <Col span={24}>
                <Row>
                  <Col xs={22} md={16}>
                    <p className="TextGray">
                      <FormattedMessage id="buscaCaronaIndo" />
                    </p>
                    <p className="TextBlue">{item.arrival_address}</p>
                  </Col>
                  <Col xs={22} md={8}>
                    <p className="TextGray">&nbsp;</p>
                    <p className="TextBlue">
                      <small>
                        {moment(item.arrival_at, "YYYY-MM-DD HH:mm:ss").format(
                          "HH[H]mm"
                        )}
                      </small>
                    </p>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
          <Col xs={24} md={6}>
            <a className="ButtonSelect" href={item.deeplink} target="_blank">
              <FormattedMessage id="resultadoCaronaBtn" />
            </a>
          </Col>
        </Row>
      </div>

      <div className="FooterResult">
        <p>
          <a href={item.deeplink} target="_blank">
            <span className="ValResult">R$ {item.price}</span>
          </a>
        </p>
      </div>
    </div>
  );
};

export default BoxResult;
