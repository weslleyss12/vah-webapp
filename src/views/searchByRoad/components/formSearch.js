import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { get } from "lodash";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { Row, Col, Form, Button, DatePicker, AutoComplete } from "antd";
import PropTypes from "prop-types";
import moment from "moment";
import "moment/locale/pt-br";

import "./formSearch.css";

// Helpers
import { trim, slugify } from "../../../utils/helpers";
import generatePath from "../../../utils/generatePath";
import localeDatepPicker from "../../../utils/localeDatepPicker";

// Actions
import { setPlace, autocomplete, setDate, reversePlace } from "../actions";

const FormItem = Form.Item;
const { Option } = AutoComplete;

class FormSearch extends Component {
  static propTypes = {
    form: PropTypes.shape().isRequired,
    setPlace: PropTypes.func.isRequired,
    setDate: PropTypes.func.isRequired,
    reversePlace: PropTypes.func.isRequired,
    autocomplete: PropTypes.func.isRequired,
    origin: PropTypes.shape(),
    destination: PropTypes.shape()
  };

  static defaultProps = {
    origin: null,
    destination: null
  };

  state = {
    searchTerm: null,
    isValidOrigin: false,
    isValidDestination: false
  };

  componentDidMount() {
    this.props.form.validateFields();
  }

  renderOption = (place, placeType) => (
    <Option
      key={`${placeType}-${place.id}`}
      value={`${place.description} / ${place.state}`}
      place={place}
    >
      {place.description} / {place.state}
    </Option>
  );

  handleSelect = (value, option, placeType) => {
    const {
      props: { place }
    } = option;
    const { origin, destination } = this.props;

    if (
      (placeType === "origin" && destination && place.id === destination.id) ||
      (placeType === "destination" && origin && place.id === origin.id)
    ) {
      alert("A origem e o destino não podem ser os mesmos.");
    } else {
      this.props.setPlace(place, placeType);

      if (placeType === "origin") this.setState({ isValidOrigin: true });
      else this.setState({ isValidDestination: true });
    }
  };

  handleSelectDate = (date, dateString) => {
    const formatedDate = date.format("YYYY-MM-DD");

    this.props.form.setFieldsValue({ ida: dateString });
    this.props.setDate(formatedDate, "go");
  };

  disabledSelectDate = date =>
    date && moment(date).endOf("day") < moment().endOf("day");

  _loadAutocomplete = input => {
    const { autocomplete } = this.props;

    if (this.searching) {
      clearTimeout(this.searching);
    }

    this.searching = setTimeout(() => {
      autocomplete(input);
    }, 300);
  };

  handleSearch = (text, placeType) => {
    const { searchTerm } = this.state;
    const term = trim(text);
    const slugT = slugify(text);

    if (placeType === "origin") this.setState({ isValidOrigin: false });
    else this.setState({ isValidDestination: false });

    if (!term) {
      this.setState({ ...this.state, results: [] });
    } else if (term.length > 2 && slugT !== searchTerm) {
      this.setState({ ...this.state, searchTerm: slugT });
      this._loadAutocomplete(term);
    }
  };

  reversePlace = () => {
    this.setState(({ origin, destination }) => ({
      origin: destination,
      destination: origin
    }));

    this.props.reversePlace();
  };

  handleSubmit = e => {
    e.preventDefault();
    const { origin, destination, go_date, match, history } = this.props;
    const matchParams = get(match, "params", {});

    const userLang = navigator.language || navigator.userLanguage;
    const lang = userLang.split("-");

    const latam = {
      AR: "ARS",
      BO: "BOB",
      BR: "BRL",
      CL: "CLP",
      CO: "COP",
      CR: "CRC",
      CU: "CUC",
      EC: "USD",
      SV: "USD",
      GT: "CTQ",
      HT: "HTG",
      HN: "HLN",
      MX: "MXN",
      NI: "NIO",
      PA: "PAB",
      PY: "PYG",
      PE: "PEN",
      DO: "DOP",
      UY: "UYU",
      VE: "VEF"
    };

    const moeda = lang[1];
    let currencyMoney = "";

    if (latam[`${moeda}`]) {
      currencyMoney = latam[`${moeda}`];
    } else {
      currencyMoney = "USD";
    }

    console.log("currencyMoney", currencyMoney);

    this.props.form.validateFields(err => {
      if (!err) {
        const url = generatePath(
          "/passagem-de-onibus/comprar/:origin_id/:origin_place/:origin_state/:destination_id/:destination_place/:destination_state/:go_date/:country/:language/:currency/",
          {
            ...matchParams,
            origin_id: origin.id,
            origin_place: origin.description,
            origin_state: origin.state,

            country: origin.state,
            language: lang[0],
            currency: currencyMoney,

            destination_id: destination.id,
            destination_place: destination.description,
            destination_state: destination.state,
            go_date
          }
        );

        history.push(url);
      }
    });
  };

  hasErrors = fieldsError =>
    Object.keys(fieldsError).some(field => fieldsError[field]);

  render() {
    const {
      autocomplete_results,
      form: { getFieldDecorator, getFieldsError }
    } = this.props;
    const { isValidOrigin, isValidDestination } = this.state;

    return (
      <Form
        className="search-form form-rodoviario"
        layout="inline"
        onSubmit={this.handleSubmit}
      >
        <h1>
          <FormattedMessage id="buscaRodoviarioTitle" />
        </h1>

        <Row gutter={10} type="flex" justify="bottom" className="search-row">
          <Col xs={24} md={9}>
            <FormItem validateStatus="" help="" className="field-big">
              {getFieldDecorator("retirada", {
                rules: [{ required: true }]
              })(
                <div>
                  <label>
                    <FormattedMessage id="buscaRodoviarioSaindo" />
                  </label>
                  <AutoComplete
                    value={this.state.origin}
                    placeholder={
                      <FormattedMessage id="buscaRodoviarioSaindo" />
                    }
                    dataSource={autocomplete_results.map(place =>
                      this.renderOption(place, "origin")
                    )}
                    onChange={value => this.setState({ origin: value })}
                    onSearch={value => this.handleSearch(value, "origin")}
                    onSelect={(value, option) =>
                      this.handleSelect(value, option, "origin")
                    }
                  />
                </div>
              )}
            </FormItem>
          </Col>

          <Col xs={24} md={2} className="swap-button-wrapper">
            <button
              className="swap-button"
              type="button"
              onClick={this.reversePlace}
            />
          </Col>

          <Col xs={24} md={9}>
            <FormItem validateStatus="" help="" className="field-big">
              {getFieldDecorator("devolucao", {
                rules: [{ required: true }]
              })(
                <div>
                  <label>
                    <FormattedMessage id="buscaRodoviarioIndo" />
                  </label>
                  <AutoComplete
                    value={this.state.destination}
                    placeholder={<FormattedMessage id="buscaRodoviarioIndo" />}
                    dataSource={autocomplete_results.map(place =>
                      this.renderOption(place, "destination")
                    )}
                    onChange={value => this.setState({ destination: value })}
                    onSearch={value => this.handleSearch(value, "destination")}
                    onSelect={(value, option) =>
                      this.handleSelect(value, option, "destination")
                    }
                  />
                </div>
              )}
            </FormItem>
          </Col>

          <Col xs={24} md={4}>
            <FormItem validateStatus="" help="" className="field-medium">
              {getFieldDecorator("ida", {
                rules: [{ required: true }]
              })(
                <div>
                  <label>
                    <FormattedMessage id="buscaRodoviarioDataIda" />
                  </label>
                  <DatePicker
                    locale={localeDatepPicker}
                    format="DD/MMM/YY"
                    placeholder="Data"
                    onChange={this.handleSelectDate}
                    disabledDate={this.disabledSelectDate}
                    className="form-input"
                    allowClear={false}
                  />
                </div>
              )}
            </FormItem>
          </Col>
        </Row>

        <FormItem className="field-full">
          <Button
            type="primary"
            htmlType="submit"
            className="btn-comparar"
            disabled={
              this.hasErrors(getFieldsError()) ||
              !isValidOrigin ||
              !isValidDestination
            }
          >
            <FormattedMessage id="buscaRodoviarioBotao" />
          </Button>
        </FormItem>
      </Form>
    );
  }
}

const WrappedFormSearch = Form.create()(FormSearch);

export default withRouter(
  connect(
    state => ({
      ...state.searchByRoad
    }),
    dispatch =>
      bindActionCreators(
        {
          setPlace,
          autocomplete,
          setDate,
          reversePlace
        },
        dispatch
      )
  )(WrappedFormSearch)
);
