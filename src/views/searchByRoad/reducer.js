import { timeMoment } from '../../utils/helpers'

import {
  ACTION_SEARCH_BY_ROAD_SET_PLACE,
  ACTION_SEARCH_BY_ROAD_SET_DATE,
  ACTION_SEARCH_BY_ROAD_REVERSE_PLACES,
  ACTION_SEARCH_BY_ROAD_RESET,
  ACTION_SEARCH_BY_ROAD_FETCH_PENDING,
  ACTION_SEARCH_BY_ROAD_FETCH_FULFILLED,
  ACTION_SEARCH_BY_ROAD_FETCH_REJECTED,
  ACTION_SEARCH_BY_ROAD_AUTOCOMPLETE_FETCH_PENDING,
  ACTION_SEARCH_BY_ROAD_AUTOCOMPLETE_FETCH_FULFILLED,
  ACTION_SEARCH_BY_ROAD_AUTOCOMPLETE_FETCH_REJECTED,
  ACTION_SEARCH_BY_ROAD_SET_FILTERS,
  ACTION_SEARCH_BY_ROAD_CLEAR_ESTIMATES
} from '../../main/constants';

const INITIAL_STATE = {
  origin: null,
  destination: null,
  go_date: null,
  return_date: null,
  pending: true,
  error: null,
  results: null,
  full_estimates: [],
  filters: {},
  autocomplete_results: [],
  pending_autocomplete: false
}

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case ACTION_SEARCH_BY_ROAD_SET_PLACE:
      return { ...state, [payload.type]: payload.place, autocomplete_results: [] }

    case ACTION_SEARCH_BY_ROAD_SET_DATE:
      return { ...state, [payload.type + '_date']: payload.date }

    case ACTION_SEARCH_BY_ROAD_REVERSE_PLACES:
      return { ...state, origin: state.destination, destination: state.origin }

    case ACTION_SEARCH_BY_ROAD_FETCH_PENDING:
      return { ...state, pending: true, results: null, full_estimates: [], filters: {} }

    case ACTION_SEARCH_BY_ROAD_AUTOCOMPLETE_FETCH_PENDING:
      return { ...state, pending_autocomplete: true, results: null }

    case ACTION_SEARCH_BY_ROAD_FETCH_FULFILLED:
      if (payload.status) {
        return { ...state, pending: false, results: payload.data, full_estimates: payload.data.estimates }
      }

      return { ...state, pending: false }

    case ACTION_SEARCH_BY_ROAD_AUTOCOMPLETE_FETCH_FULFILLED:
      if (payload.status) {
        return { ...state, pending_autocomplete: false, autocomplete_results: payload.data }
      }

      return { ...state, pending: false }

    case ACTION_SEARCH_BY_ROAD_FETCH_REJECTED:
      return { ...state, pending: false, error: payload }

    case ACTION_SEARCH_BY_ROAD_AUTOCOMPLETE_FETCH_REJECTED:
      return { ...state, pending_autocomplete: false, error: payload }

    case ACTION_SEARCH_BY_ROAD_SET_FILTERS:
      const results = filterItems(payload, state)
      return { ...state, filters: payload, results, pending: false }

    case ACTION_SEARCH_BY_ROAD_CLEAR_ESTIMATES:
      return { ...state, results: { ...state.results, estimates: [] }, pending: true }

    case ACTION_SEARCH_BY_ROAD_RESET:
      return INITIAL_STATE

    default:
      return state
  }
}

const filterItems = (filters, state, localResults) => {
  const e = state ? state.full_estimates : localResults.estimates
  const r = state ? state.results : localResults

  const validHour = (hour) => (hour !== 'Any time' && hour !== 'Qualquer horário')

  const isAValidMoment = (hour, date) => {
    if (
      ((hour.indexOf('Morning') !== -1 || hour.indexOf('Manhã') !== -1) && timeMoment(date) === 'morning') ||
      ((hour.indexOf('Afternoon') !== -1 || hour.indexOf('Tarde') !== -1) && timeMoment(date) === 'afternoon') ||
      ((hour.indexOf('Night') !== -1 || hour.indexOf('Noite') !== -1) && timeMoment(date) === 'night')
    ) {
      return true
    }

    return false
  }

  const getClassByProduct = (product) => {
    if (product.indexOf('Convencional') !== -1) {
      return 'Convencional'
    }
    else if (product.indexOf('Executivo') !== -1) {
      return 'Executivo'
    }
    else if (product.indexOf('Leito') !== -1) {
      return 'Leito'
    }
  }

  const estimates = e.filter(estimate => {
    return (
      (filters.companies ? filters.companies.indexOf(estimate.company) !== -1 : true) &&
      (filters.classes ? filters.classes.indexOf(getClassByProduct(estimate.product)) !== -1 : true) &&
      ((filters.hour && validHour(filters.hour)) ? isAValidMoment(filters.hour, estimate.departure.date) : true)
    )
  })

  return { ...r, estimates }
}
