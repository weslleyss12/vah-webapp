import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import MetaTags from "react-meta-tags";

import styled from "styled-components";
import { Row, Col, List } from "antd";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import Breadcrumb from "../../components/breadcrumb";
import Header from "../../components/header";
import ResultHeader from "../../components/ResultHeader";
import Footer from "../../components/footer";
import Container from "../../components/container";
import SideBlue from "../../components/sideBlue";
import BoxResult from "./components/boxResult";

import AddressTransition from "../../components/transition";

import { FormattedMessage } from "react-intl";

import Config from "../../config";

// Actions
import {
  searchRoad,
  setFilters,
  clearEstimates
} from "../searchByRoad/actions";

const MainInfo = styled.section`
  padding: 20px 0;
`;

const Form = styled.form`
  &.form-filter {
    padding-bottom: 10px;

    fieldset:nth-child(3) {
      margin-bottom: 10px;
    }
  }
`;

class ResultByRoad extends Component {
  static propTypes = {
    match: PropTypes.shape().isRequired
  };

  constructor(props) {
    super(props);

    const timeRange = ["Manhã", "Tarde", "Noite"];

    this.state = {
      hour: timeRange,
      filteredHour: props.filters.hour || timeRange,
      companies: [],
      filteredCompanies: props.filters.companies || [],
      classes: [],
      filteredClasses: props.filters.classes || []
    };
  }

  componentDidMount = () => {
    const {
      match: {
        params: {
          origin_id,
          destination_id,
          country,
          language,
          currency,
          go_date
        }
      }
    } = this.props;

    this.props
      .searchRoad({
        origin: origin_id,
        destination: destination_id,
        date: go_date,
        country,
        language,
        currency,
        app_version: Config.APP_VERSION
      })
      .then(() => {
        const { pending, full_estimates: estimates } = this.props;
        const { filteredCompanies, filteredClasses } = this.state;
        const companies = [];
        const classes = [];

        if (pending || (estimates && estimates.length === 0 && !pending))
          return null;

        estimates.map(estimate => {
          const company = ("" + estimate.company).trim();
          const product = ("" + estimate.product).trim();

          if (companies.indexOf(company) === -1) {
            companies.push(company);
          }

          if (
            product.indexOf("Convencional") !== -1 &&
            classes.indexOf("Convencional") === -1
          ) {
            classes.push("Convencional");
          } else if (
            product.indexOf("Executivo") !== -1 &&
            classes.indexOf("Executivo") === -1
          ) {
            classes.push("Executivo");
          } else if (
            product.indexOf("Leito") !== -1 &&
            classes.indexOf("Leito") === -1
          ) {
            classes.push("Leito");
          }

          return estimates;
        });

        companies.sort();
        classes.sort();

        this.setState({
          ...this.state,
          companies,
          classes,
          filteredCompanies:
            filteredCompanies.length > 0 ? filteredCompanies : companies,
          filteredClasses:
            filteredClasses.length > 0 ? filteredClasses : classes
        });
      });
  };

  _handleHour = (hour, e) => {
    const active = e.target.checked;
    const filteredHour = this.state.filteredHour.concat([]);

    if (active && filteredHour.indexOf(hour) === -1) {
      filteredHour.push(hour);
    } else if (filteredHour.indexOf(hour) !== -1) {
      filteredHour.splice(filteredHour.indexOf(hour), 1);
    }

    this.setState({ ...this.state, filteredHour });
  };

  _renderTimeRange = () => {
    const { hour, filteredHour } = this.state;

    return hour.map(time => (
      <span className="field-holder" key={time}>
        <div className="switch-containter">
          <div className="switch">
            <input
              id={time}
              className="cmn-toggle cmn-toggle-round"
              type="checkbox"
              checked={filteredHour.indexOf(time) !== -1}
              onChange={enable => this._handleHour(time, enable)}
            />
            <label for={time} />
          </div>
          <span>{time}</span>
        </div>
      </span>
    ));
  };

  _handleCompanies = (company, e) => {
    const active = e.target.checked;
    const filteredCompanies = this.state.filteredCompanies.concat([]);

    if (active && filteredCompanies.indexOf(company) === -1) {
      filteredCompanies.push(company);
    } else if (filteredCompanies.indexOf(company) !== -1) {
      filteredCompanies.splice(filteredCompanies.indexOf(company), 1);
    }

    this.setState({ ...this.state, filteredCompanies });
  };

  _renderCompanies = () => {
    const { companies, filteredCompanies } = this.state;

    return companies.map(company => (
      <span className="field-holder" key={company}>
        <div className="switch-containter">
          <div className="switch">
            <input
              id={company}
              className="cmn-toggle cmn-toggle-round"
              type="checkbox"
              checked={filteredCompanies.indexOf(company) !== -1}
              onChange={enable => this._handleCompanies(company, enable)}
            />
            <label for={company} />
          </div>
          <span>{company}</span>
        </div>
      </span>
    ));
  };

  _handleClasses = (c, e) => {
    const active = e.target.checked;
    const filteredClasses = this.state.filteredClasses.concat([]);

    if (active && filteredClasses.indexOf(c) === -1) {
      filteredClasses.push(c);
    } else if (filteredClasses.indexOf(c) !== -1) {
      filteredClasses.splice(filteredClasses.indexOf(c), 1);
    }

    this.setState({ ...this.state, filteredClasses });
  };

  _renderClasses = () => {
    const { classes, filteredClasses } = this.state;

    return classes.map(c => (
      <span className="field-holder" key={c}>
        <div className="switch-containter">
          <div className="switch">
            <input
              id={c}
              className="cmn-toggle cmn-toggle-round"
              type="checkbox"
              checked={filteredClasses.indexOf(c) !== -1}
              onChange={enable => this._handleClasses(c, enable)}
            />
            <label for={c} />
          </div>
          <span>{c}</span>
        </div>
      </span>
    ));
  };

  _handleApply = e => {
    e.preventDefault();

    const { setFilters, clearEstimates } = this.props;
    const {
      hour,
      filteredHour,
      companies,
      filteredCompanies,
      classes,
      filteredClasses
    } = this.state;

    setTimeout(
      () => {
        if (
          hour.length === filteredHour.length &&
          companies.length === filteredCompanies.length &&
          classes.length === filteredClasses.length
        ) {
          setFilters({});
        } else {
          setFilters({
            hour: filteredHour,
            companies: filteredCompanies,
            classes: filteredClasses
          });
        }
      },
      500,
      this.child.closeSide()
    );

    clearEstimates();
  };

  render() {
    const { results, pending, match } = this.props;

    if (results === null || pending || this.state.hasTransition)
      return (
        <AddressTransition
          results={results}
          pending={pending}
          toggleTransition={this.toggleTransition}
          activeTab="rodoviario"
        />
      );

    return (
      <div className="page-rodoviario">
        <MetaTags>
          <meta
            name="title"
            content="Comprar passagem de ônibus na Internet com melhores preços | Vah"
          />
          <meta
            name="description"
            content="Comprar passagem de ônibus com o melhor preço ficou fácil! Escolha seu destino e compare os preços de diferentes viações ao mesmo tempo. "
          />
          <meta name="keywords" content="comprar passagem de onibus" />
          {/* <meta property="og:title" content="MyApp" />
          <meta property="og:image" content="path/to/image.jpg" /> */}
        </MetaTags>
        <Breadcrumb>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/passagem-de-onibus">
              Vah <FormattedMessage id="menuRodoviario" />
            </Link>
          </li>
          <li>Resultado</li>
        </Breadcrumb>
        <Header />
        <ResultHeader infos={match.params} backUrl="/passagem-de-onibus" />
        <MainInfo>
          <Container>
            <Row gutter={60}>
              <Col md={24} lg={7}>
                <SideBlue onRef={ref => (this.child = ref)}>
                  <h2 className="title-filter">
                    <FormattedMessage id="filtroRodoviarioTitle" />
                  </h2>
                  <Form className="form-filter" onSubmit={this._handleApply}>
                    <fieldset>
                      <legend className="filter-type">
                        <FormattedMessage id="filtroRodoviarioPeriodo" />
                      </legend>
                      {this._renderTimeRange()}
                    </fieldset>
                    <fieldset>
                      <legend className="filter-type">
                        <FormattedMessage id="filtroRodoviarioEmpresas" />
                      </legend>
                      {this._renderCompanies()}
                    </fieldset>
                    <fieldset>
                      <legend className="filter-type">
                        <FormattedMessage id="filtroRodoviarioClasse" />
                      </legend>
                      {this._renderClasses()}
                    </fieldset>
                    <div>
                      <button className="apply-filter">
                        <FormattedMessage id="filtroRodoviarioBtn" />
                      </button>
                    </div>
                  </Form>
                </SideBlue>
              </Col>
              <Col md={24} lg={17}>
                {(results === null || pending) && (
                  <p>Estamos pesquisando as melhores tarifas para você.</p>
                )}

                {results &&
                  results.estimates.length === 0 &&
                  !pending && <p>Nenhum resultado encontrado</p>}

                {results &&
                  results.estimates.length > 0 &&
                  !pending && (
                    <List
                      dataSource={results.estimates}
                      renderItem={item => <BoxResult item={item} />}
                    />
                  )}
              </Col>
            </Row>
          </Container>
        </MainInfo>
        <Footer />
      </div>
    );
  }
}

export default connect(
  state => ({ ...state.searchByRoad }),
  dispatch =>
    bindActionCreators(
      {
        searchRoad,
        setFilters,
        clearEstimates
      },
      dispatch
    )
)(ResultByRoad);
