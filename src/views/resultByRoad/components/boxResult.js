import React from "react";
import moment from "moment";

import { minutesInHours } from "../../../utils/helpers";

import "./boxResult.css";

import { Row, Col } from "antd";

import { FormattedMessage } from "react-intl";

const BoxResult = ({ item }) => {
  let price = String(item.estimate).replace(".", ",");
  let valor = price.replace("R$", "");

  return (
    <div className="ItemResult itemRoad">
      <div>
        <Row>
          <Col xs={24} md={4} className="col-data">
            <p>
              <img src={item.image} width="70%" alt="" />
            </p>
            <span />
          </Col>
          <Col xs={24} md={14}>
            <Row className="RowInfo topo">
              <Col span={24}>
                <Row>
                  <Col xs={22} md={16}>
                    <p className="TextGray">
                      <FormattedMessage id="resultadoRodoviarioSaindo" />
                    </p>
                    <p className="TextBlue">{item.departure.place}</p>
                  </Col>
                  <Col xs={22} md={8}>
                    <p className="TextGray">&nbsp;</p>
                    <p className="TextBlue">
                      <small>
                        {moment(
                          item.departure.date,
                          "YYYY-MM-DD hh:mm:ss"
                        ).format("HH[H]mm")}
                      </small>
                    </p>
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row className="RowInfo">
              <Col span={24}>
                <Row>
                  <Col xs={22} md={16}>
                    <p className="TextGray">
                      <FormattedMessage id="resultadoRodoviarioIndo" />
                    </p>
                    <p className="TextBlue">{item.arrival.place}</p>
                  </Col>
                  <Col xs={22} md={8}>
                    <p className="TextGray">&nbsp;</p>
                    <p className="TextBlue">
                      <small>
                        {moment(
                          item.arrival.date,
                          "YYYY-MM-DD hh:mm:ss"
                        ).format("HH[H]mm")}
                      </small>
                    </p>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
          <Col xs={24} md={6}>
            <a className="ButtonSelect" href={item.deeplink} target="_blank">
              <FormattedMessage id="resultadoRodoviarioBtn" />
            </a>
          </Col>
        </Row>
      </div>

      <p className="timer">
        <FormattedMessage id="resultadoRodoviarioDuracao" />{" "}
        {minutesInHours(item.duration)}
      </p>

      <div className="FooterResult">
        <p>
          <span className="product">{item.product}</span> Via Viação{" "}
          {item.company}{" "}
          <a href={item.deeplink} target="_blank">
            <span className="ValResult"> {price}</span>
          </a>
        </p>
      </div>
    </div>
  );
};

export default BoxResult;
