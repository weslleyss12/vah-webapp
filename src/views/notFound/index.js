import React, { Component } from "react";
import { Link } from "react-router-dom";
import MetaTags from "react-meta-tags";

import Container from "../../components/container";

import { Button, Row, Col } from "antd";

import Header from "../../components/header";
import Footer from "../../components/footer";
import Breadcrumb from "../../components/breadcrumb";
import logoVah from "../../assets/images/core/logo-transition.png";

class NotFound extends Component {
  state = {};
  render() {
    return (
      <div className="page-contact">
        <MetaTags>
          <meta name="title" content="tit404" />
          <meta
            name="description"
            content="A página que você procura não foi encontrada. Volte para nossa página inicial!"
          />
          {/* <meta property="og:title" content="MyApp" />
          <meta property="og:image" content="path/to/image.jpg" /> */}
        </MetaTags>
        <Breadcrumb>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>404</li>
        </Breadcrumb>
        <Header />
        <Container>
          <div className="container-sobre">
            <Row type="flex" justify="center" align="center">
              <Col md={24} lg={16}>
                <p className="logo-transition">
                  <img src={logoVah} width="120" alt="VAH" />
                </p>
              </Col>

              <Col md={24} lg={15}>
                <h2 className="tit404">
                  <span className="t404">404</span> Ops, esta página não existe!
                </h2>
              </Col>

              <Col md={24} lg={15} className="colCenter">
                <Link to="/">
                  <Button
                    type="primary"
                    htmlType="submit"
                    className="login-form-button"
                  >
                    Voltar
                  </Button>
                </Link>
              </Col>
            </Row>
          </div>
        </Container>
        <Footer />
      </div>
    );
  }
}

export default NotFound;
