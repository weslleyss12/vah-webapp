import React, { Component, Fragment } from "react";
import MetaTags from "react-meta-tags";
import styled from "styled-components";
import { Tabs } from "antd";

import Header from "../../components/header";
import Breadcrumb from "../../components/breadcrumb";
import Footer from "../../components/footer";
import Container from "../../components/container";

import BuscaUrbano from "../searchByAddress/components/formSearch";
import BuscaAereo from "../searchByAir/components/formSearch";
import BuscaLocacao from "../searchByRent/components/formSearch";
import BuscaRodoviario from "../searchByRoad/components/formSearch";
import BuscaCarona from "../searchByRide/components/formSearch";

import google from "../../assets/images/google.png";

import icoAereo from "../../assets/images/icons/ico-vah-aereo.svg";
import icoCarona from "../../assets/images/icons/ico-vah-carona.svg";
import icoLocacao from "../../assets/images/icons/ico-vah-locacao.png";
import icoRodoviario from "../../assets/images/icons/ico-vah-rodoviario.svg";
import icoUrbano from "../../assets/images/icons/ico-vah-urbano.svg";

import icoAereoHover from "../../assets/images/icons/ico-vah-aereo-hover.svg";
import icoCaronaHover from "../../assets/images/icons/ico-vah-carona-hover.svg";
import icoLocacaoHover from "../../assets/images/icons/ico-vah-locacao-hover.png";
import icoRodoviarioHover from "../../assets/images/icons/ico-vah-rodoviario-hover.svg";
import icoUrbanoHover from "../../assets/images/icons/ico-vah-urbano-hover.svg";

import bgUrbano from "../../assets/images/home/home-urbano.jpg";
import bgAereo from "../../assets/images/aereo/banner-aereo.jpg";
import bgLocacao from "../../assets/images/home/home-locacao.png";
import bgRodoviario from "../../assets/images/home/home-rodoviario.png";
import bgCarona from "../../assets/images/home/home-carona.png";

import ContentText from "../../components/contentText";

import { FormattedMessage } from "react-intl";

import "../../assets/css/App.css";

const TabPane = Tabs.TabPane;

const tabUrbano = (
  <Fragment>
    <span className="spanIco">
      <i className="iUrbano">
        <img src={icoUrbano} width="50" />
        <img src={icoUrbanoHover} className="icoHover" width="50" />
      </i>
    </span>{" "}
    <span className="text">
      vah<strong>
        <FormattedMessage id="menuUrbano" />
      </strong>
    </span>
  </Fragment>
);
const tabAereo = (
  <Fragment>
    <span className="spanIco">
      <i className="iAereo">
        <img src={icoAereo} width="50" />
        <img src={icoAereoHover} className="icoHover" width="50" />
      </i>
    </span>{" "}
    <span className="text">
      vah<strong>
        <FormattedMessage id="menuAereo" />
      </strong>
    </span>
  </Fragment>
);
const tabLocacao = (
  <Fragment>
    <span className="spanIco">
      <i className="iLocacao">
        <img src={icoLocacao} width="50" />
        <img src={icoLocacaoHover} className="icoHover" width="50" />
      </i>
    </span>{" "}
    <span className="text">
      vah<strong>
        <FormattedMessage id="menuLocacao" />
      </strong>
    </span>
  </Fragment>
);
const tabRodoviario = (
  <Fragment>
    <span className="spanIco">
      <i className="iRodoviario">
        <img src={icoRodoviario} width="50" />
        <img src={icoRodoviarioHover} className="icoHover" width="50" />
      </i>
    </span>{" "}
    <span className="text">
      vah<strong>
        <FormattedMessage id="menuRodoviario" />
      </strong>
    </span>
  </Fragment>
);
const tabCarona = (
  <Fragment>
    <span className="spanIco">
      <i className="iCarona">
        <img src={icoCarona} width="40" />
        <img src={icoCaronaHover} className="icoHover" width="40" />
      </i>
    </span>{" "}
    <span className="text">
      vah<strong>
        <FormattedMessage id="menuCarona" />
      </strong>
    </span>
  </Fragment>
);

class Home extends Component {
  state = {
    activeKey: "2",
    activeImage: `${bgAereo}`
  };

  callback = key => {
    switch (key) {
      case "1":
        this.setState({ activeImage: `${bgUrbano}`, activeKey: key });
        break;
      case "2":
        this.setState({ activeImage: `${bgAereo}`, activeKey: key });
        break;
      case "3":
        this.setState({ activeImage: `${bgLocacao}`, activeKey: key });
        break;
      case "4":
        this.setState({ activeImage: `${bgRodoviario}`, activeKey: key });
        break;
      case "5":
        this.setState({ activeImage: `${bgCarona}`, activeKey: key });
        break;
      default:
        this.setState({ activeImage: `${bgUrbano}`, activeKey: key });
    }
  };

  render() {
    const { activeImage } = this.state;

    const BannerSearch = styled.div`
      width: 100%;
      min-height: 505px;
      background-image: url("${activeImage}");
      background-size: cover;
      box-sizing: border-box;
      padding: 40px 0;
      transition: background-image 1s ease;
    `;

    return (
      <div className="page-home">
        <MetaTags>
          <meta
            name="title"
            content="Vah - Mais economia no transporte na cidade e na viagem"
          />
          <meta
            name="description"
            content="Compare preços de Aplicativo de Transporte, Passagem Aérea, Passagem de Ônibus, Aluguel de Carros ou Carona - tudo em um só lugar. Viaje mais feliz!"
          />
          <meta name="keywords" content="Transporte" />
          {/* <meta property="og:title" content="MyApp" />
          <meta property="og:image" content="path/to/image.jpg" /> */}
        </MetaTags>
        <Breadcrumb />
        <Header />
        <BannerSearch className="BannerSearch">
          <Container>
            <Tabs
              type="card"
              className="tabHome"
              onChange={this.callback}
              activeKey={this.state.activeKey}
            >
              {/* <TabPane tab={tabUrbano} key="1">
                <BuscaUrbano />
              </TabPane> */}
              <TabPane tab={tabAereo} key="2">
                <BuscaAereo />
              </TabPane>
              <TabPane tab={tabRodoviario} key="4">
                <BuscaRodoviario />
              </TabPane>
              <TabPane tab={tabLocacao} key="3">
                <BuscaLocacao />
              </TabPane>
              <TabPane tab={tabCarona} key="5">
                <BuscaCarona />
              </TabPane>
            </Tabs>
          </Container>
        </BannerSearch>
        <section className="main-info">
          <Container>
            <ContentText />
          </Container>
        </section>
        <Footer />
      </div>
    );
  }
}

export default Home;
