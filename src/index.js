import React, { Fragment } from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";

import SmartBanner from "react-smartbanner";
import "../node_modules/react-smartbanner/dist/main.css";

import "./assets/scss/index.css";
import "antd/dist/antd.css";

import Routes from "./routes/Routes";
import configureStore from "./configureStore";
import { unregister } from "./registerServiceWorker";

import { addLocaleData, IntlProvider } from "react-intl";
import en from "react-intl/locale-data/en";
import pt from "react-intl/locale-data/pt";
import es from "react-intl/locale-data/es";
import { flattenMessages } from "./utils/i18n";
import messages from "./messages";

const rootElement = document.getElementById("root");
const store = configureStore();

let lang = localStorage.getItem("idioma");

if (!lang) {
  lang = "pt-BR";
}

let params = new URLSearchParams(document.location.search.substring(1));
let locale = params.get("lang") || lang;

addLocaleData([...en, ...pt, ...es]);

//ReactDOM.render(<SmartBanner title={'Vah'} />, rootElement);

render(
  <Fragment>
    <SmartBanner
      title={"Veja o melhor transporte para você!"}
      storeText={{
        ios: "Use o app VAH agora mesmo",
        android: "Use o app VAH agora mesmo"
      }}
      price={{
        ios: "",
        android: ""
      }}
      button="Abrir"
    />
    <Provider store={store}>
      <IntlProvider
        locale={locale}
        messages={flattenMessages(messages[locale])}
      >
        <Routes />
      </IntlProvider>
    </Provider>
  </Fragment>,
  rootElement
);

unregister();
