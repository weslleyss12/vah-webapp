import React, { Component, Fragment } from "react";

import {
  fetchDefaults,
  fetchDefaultOptions
} from "./utils/fetchDefaultOptions";
import Config from "./config";

class App extends Component {
  state = {
    user: null,
    error: null
  };

  componentWillMount = () => {
    this._checkUser();
  };

  _checkUser = () => {
    this.setState({ ...this.state, error: null });
    const vah_user = localStorage.getItem("vah_user");

    if (!vah_user) {
      this._registerDevice();
    } else {
      const user = JSON.parse(vah_user);
      fetchDefaults.token = user.token;

      this.setState({ ...this.state, user });
    }
  };

  _registerDevice() {
    const url = `${Config.API_BASE_URL}/users`;
    const { appVersion, platform } = window.navigator;
    const data = {
      device_id: "temp",
      os: platform,
      os_version: appVersion
    };

    fetch(url, fetchDefaultOptions("POST", data))
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.status) {
          const user = responseJson.data;

          fetchDefaults.token = user.token;

          this.setState({ ...this.state, user }, () => {
            localStorage.setItem("vah_user", JSON.stringify(user));
          });
        } else {
          console.log("Error: ", responseJson.error);
          this.setState({ ...this.state, error: "103" });
        }
      })
      .catch(error => {
        console.log("error: ", error.message);
        this.setState({ ...this.state, error: "104" });
      });
  }

  render() {
    return <Fragment>{this.props.children}</Fragment>;
  }
}

export default App;
