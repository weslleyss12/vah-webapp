import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import ScrollToTop from "../components/ScrollToTop";
import App from "../App";
import About from "../views/about";
import Terms from "../views/terms";
import Home from "../views/home";
import Contact from "../views/contact";
import NotFound from "../views/notFound";

import SearchByAddress from "../views/searchByAddress";
import ResultByAddress from "../views/resultByAddress";

import SearchByAir from "../views/searchByAir";
import ResultByAir from "../views/resultByAir";

import SearchByRent from "../views/searchByRent";
import ResultByRent from "../views/resultByRent";

import SearchByRoad from "../views/searchByRoad";
import ResultByRoad from "../views/resultByRoad";

import SearchByRide from "../views/searchByRide";
import ResultByRide from "../views/resultByRide";

const Routes = () => (
  <BrowserRouter>
    <ScrollToTop>
      <Switch>
        <Route exact path="/vah" component={About} />
        <Route exact path="/vah/termos-de-uso" component={Terms} />
        <Route exact path="/vah/contato" component={Contact} />

        {/* <Route
          exact
          path="/aplicativo-de-transporte"
          component={SearchByAddress}
        />
        <Route
          exact
          path="/aplicativo-de-transporte/preco-e-cupom-de-desconto/:start_latitude/:start_longitude/:start_address/:start_country/:start_city/:start_state/:start_zip_code/:end_latitude/:end_longitude/:end_address/:end_city/:end_state/:end_zip_code/:country/:language/:currency/"
          component={ResultByAddress}
        /> */}

        <Route exact path="/passagem-de-onibus" component={SearchByRoad} />
        <Route
          exact
          path="/passagem-de-onibus/comprar/:origin_id/:origin_place/:origin_state/:destination_id/:destination_place/:destination_state/:go_date/:country/:language/:currency/"
          component={ResultByRoad}
        />

        <Route exact path="/carona-compartilhada" component={SearchByRide} />
        <Route
          exact
          path="/carona-compartilhada/buscar/:date/:start_hour/:seats/:start_latitude/:start_longitude/:start_address/:start_city/:start_state/:start_zip_code/:end_latitude/:end_longitude/:end_address/:end_city/:end_state/:end_zip_code/:country/:language/:currency/"
          component={ResultByRide}
        />

        <Route exact path="/passagens-aereas" component={SearchByAir} />
        <Route
          exact
          path="/passagens-aereas/comprar/:origin/:destination/:adults/:children/:infants/:direct/:go_date/:return_date?/:country/:language/:currency/"
          component={ResultByAir}
        />

        <Route exact path="/aluguel-de-carros" component={SearchByRent} />
        <Route
          exact
          path="/aluguel-de-carros/rent-a-car/:origin_date/:origin_time/:return_date/:return_time/:origin/:origin_description/:origin_city/:origin_state/:return/:return_description/:return_city/:return_state/:country/:language/:currency/"
          component={ResultByRent}
        />

        <Route exact path="/" component={Home} />

        <Route component={NotFound} />
      </Switch>
    </ScrollToTop>
  </BrowserRouter>
);

export default Routes;
