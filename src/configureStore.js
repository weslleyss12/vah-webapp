import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';

import reducers from './redux';

export default () => createStore(reducers, {}, applyMiddleware(thunk, promiseMiddleware()));
