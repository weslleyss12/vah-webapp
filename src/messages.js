export default {
  "es-gl": {
    //MENU HEADER
    menuUrbano: "Urbano",
    menuAereo: "Aéreo",
    menuRodoviario: "Autobús",
    menuLocacao: "Alquiler",
    menuCarona: "Aventón",

    //FOOTER
    footerSlogan: "AHORRAR TIEMPO Y DINERO",
    footerSobre: "Sobre",
    footerTermos: "Términos de uso",
    footerContato: "Contacto",
    footerBlog: "Blog",
    footerSiga: "Síguenos",

    //BANNER Download APP
    bannerTitulo: "Una aplicación, ",
    bannerTituloB: "muchas posibilidades.",
    bannerDescricao: "Descarga ahora la tuya",

    //CONTENT Home Pages
    contentTituloA: "No importa adónde ",
    contentTituloB: ", ve con nosotros:",
    contentOque: "¿Qué es VAH?",
    contentOqueText:
      "Vah es un comparador de precios de los principales servicios de transporte. Aquí encuentras aplicaciones de transporte, pasajes aéreos, pasajes de autobús, alquiler de carros, además de aventón  en Brasil y el exterior. ¡Ahora es fácil encontrar el transporte más barato!",
    contentGratuito: "¡VAH es 100% gratis!",
    contentGratuitoText:
      "¡No pagas ningún valor adicional para usar el comparador de transportes más completo de Brasil! Después de seleccionar la mejor opción para ti y tu bolsillo, es sólo hacer click para ser remitido hacia el sitio o app partner y entonces finalizar la compra.",
    contentMidia: "VAH en los medios",
    contentMidiaTextA:
      "Mira lo que ya dijeron sobre nosotros en Brasil y el mundo: ",
    contentMidiaTextB: "y otros.",

    //BUSCA URBANO
    buscaUrbanoTitle:
      "Aplicación de Taxi, Uber y más transportes: compara las mejores tarifas.",
    buscaUrbanoPartida: "MI LUGAR DE SALIDA",
    buscaUrbanoDestino: "MI DESTINO",
    buscaUrbanoBotao: "Comparar",
    buscaUrbanoPlace: "Introduce aquí la dirección",

    //FILTRO URBANO
    filtroUrbanoTitle: "Refina tu búsqueda con los filtros",
    filtroUrbanoOrdenar: "ORDENAR POR",
    filtroUrbanoPreco: "Precio",
    filtroUrbanoTempo: "Tiempo de llegada",
    filtroUrbanoEmpresas: "Empresas",
    filtroUrbanoBtn: "APLICAR FILTROS",

    //RESULT URBANO
    resultadoUrbanoBtn: "DETALLES",
    resultadoUrbanoDisctancia: "DISTANCIA",
    resultadoUrbanoChega: "Llega",
    resultadoUrbanoDuracao: "DURACIÓN",

    //BUSCA AEREO
    buscaAereoTitle: "Pasajes aéreos: compara las mejores tarifas.",
    buscaAereoFiltro1: "IDA Y VUELTA",
    buscaAereoFiltro2: "SÓLO IDA",
    buscaAereoDe: "SALIENDO DE",
    buscaAereoPara: "YENDO A",
    buscaAereoDataIda: "Ida",
    buscaAereoDataVolta: "Vuelta",
    buscaAereoPassageiro: "PASAJERO",
    buscaAereoPassageiroInfo:
      "Las políticas y límites de edad para viajar con niños pueden variar, por lo tanto, comprobar con la línea aérea antes de reservar",
    buscaAereoAtualizar: "atualizar",
    buscaAereoBotao: "Comparar",

    //FILTRO AEREO
    filtroAereoTitle: "Refina tu búsqueda con los filtros",
    filtroAereoEscalas: "ESCALAS",
    filtroAereoEmpresas: "AEROLÍNEAS",
    filtroAereoPreco: "PRECIO",
    filtroAereoIda: "Período ida",
    filtroAereoVolta: "Período vuelta",
    filtroAereoBtn: "Aplicar filtros",

    //RESULT AEREO
    resultadoAereoBtn: "DETALLES",
    resultadoAereoPassageiro: "PASAJERO",
    resultadoAereoPartida: "SALIDA",
    resultadoAereoChegada: "LLEGADA",

    //BUSCA RODOVIARIO
    buscaRodoviarioTitle: "Pasaje de autobús: compara las mejores tarifas.",
    buscaRodoviarioSaindo: "Saliendo de",
    buscaRodoviarioIndo: "Yendo a",
    buscaRodoviarioDataIda: "Ida",
    buscaRodoviarioBotao: "Comparar",

    //FILTRO RODOVIARIO
    filtroRodoviarioTitle: "Refina tu búsqueda con los filtros",
    filtroRodoviarioPeriodo: "PERÍODO",
    filtroRodoviarioEmpresas: "Empresas",
    filtroRodoviarioClasse: "Clase",
    filtroRodoviarioBtn: "Aplicar filtros",

    //RESULT RODOVIARIO
    resultadoRodoviarioBtn: "DETALLES",
    resultadoRodoviarioDuracao: "Duración del viaje",
    resultadoRodoviarioSaindo: "Saliendo de",
    resultadoRodoviarioIndo: "Yendo a",

    //BUSCA LOCAÇÃO
    buscaLocacaoTitle: "Alquiler de carros: compara las mejores tarifas.",
    buscaLocacaoFiltro1: "MISMO LUGAR",
    buscaLocacaoFiltro2: "OTRO LUGAR",
    buscaLocacaoRetirada: "RECOGIDA",
    buscaLocacaoDevolucao: "ENTREGA",
    buscaLocacaoDataIda: "FECHA",
    buscaLocacaoHorarioIda: "Horario",
    buscaLocacaoDataVolta: "RECOGIDA",
    buscaLocacaoHorarioVolta: "ENTREGA",
    buscaLocacaoBotao: "Comparar",

    //FILTRO LOCAÇÃO
    filtroLocacaoTitle: "Refina tu búsqueda con los filtros",
    filtroLocacaoCategoria: "Categoria",
    filtroLocacaoEmpresas: "Empresa",
    filtroLocacaoOpcionais: "Opcionales",
    filtroLocacaoBtn: "Aplicar filtros",

    //RESULT LOCAÇÃO
    resultadoLocacaoBtn: "Seleccionar",
    resultadoLocacaoPortas: "Puertos",
    resultadoLocacaoPassageiros: "pasajeros",

    //BUSCA CARONA
    buscaCaronaTitle:
      "Aventón: consulta las opciones de aventón intermunicipales y tarifas disponibles",
    buscaCaronaSaindo: "SALIENDO DE",
    buscaCaronaIndo: "YENDO A",
    buscaCaronaDataIda: "Ida",
    buscaCaronaSaida: "DESPUÉS DE",
    buscaCaronaLugares: "Lugares",
    buscaCaronaPlace: "Introduce aquí la dirección",
    buscaCaronaBotao: "Comparar",

    //FILTRO CARONA
    filtroCaronaTitle: "Refina tu búsqueda con los filtros",
    filtroCaronaPeriodo: "Período",
    filtroCaronaPreco: "PRECIO",
    filtroCaronaBtn: "Aplicar filtros",

    //RESULT CARONA
    resultadoCaronaBtn: "DETALLES",
    resultadoCaronaDuracao: "DuraCIÓN PROMEDIO",
    resultadoCaronaApartir: "Desde",

    //CONTATO
    contatoTitle1: "Hey, ¿necesitas ayuda?",
    contatoTitle2: "Entra en contacto con el equipo Vah:",
    contatoLegenda: "Todos los campos deben ser llenados",
    contatoNovidade: "recibir novedades",
    contatoMotivoOp1: "Sugerencia",
    contatoMotivoOp2: "Reclamación",
    contatoMotivoOp3: "Elogio",
    contatoMotivoOp4: "Duda",
    contatoMotivo: "Motivo del contacto",
    contatoBtn: "Enviar",

    //GERAL
    geralBtnVoltar: "Atrás",
    geralBtnRefazer: "REHACER BÚSQUEDA",
    geralBtnTransicao:
      "Nuestro algoritmo está buscando las mejores tarifas para ti.",
    geralBtnTransicao2:
      "¡Eso puede demorar un poquito, pues realizamos el mayor escaneo del mercado!"
  },
  "pt-BR": {
    //MENU HEADER
    menuUrbano: "Urbano",
    menuAereo: "Aéreo",
    menuRodoviario: "Rodoviário",
    menuLocacao: "Locação",
    menuCarona: "Carona",

    //FOOTER
    footerSlogan: "Economize tempo e dinheiro",
    footerSobre: "Sobre",
    footerTermos: "Termos de uso",
    footerContato: "Contato",
    footerBlog: "Blog",
    footerSiga: "siga-nos",

    //BANNER Download APP
    bannerTitulo: "Um aplicativo, ",
    bannerTituloB: "muitas possibilidades.",
    bannerDescricao: "Baixe agora o seu.",

    //CONTENT Home Pages
    contentTituloA: "Não importa aonde você",
    contentTituloB: ", vá com a gente:",
    contentOque: "O que é o VAH?",
    contentOqueText:
      "O Vah é um comparador de preços dos principais serviços de transporte. Aqui você pesquisa aplicativos de transporte, passagens aéreas, passagens de ônibus, aluguel de carros, além de carona compartilhada no Brasil e no exterior. Ficou fácil encontrar o transporte mais barato!",
    contentGratuito: "O VAH é 100% gratuito!",
    contentGratuitoText:
      "Você não paga nenhuma taxa extra para usar o comparador de transportes mais completo do Brasil! Após selecionar a melhor opção para você e seu bolso, é só clicar para ser redirecionado para o site ou app parceiro e então finalizar a compra.",
    contentMidia: "VAH na mídia",
    contentMidiaTextA:
      "Confira o que já falaram sobre a gente no Brasil e no mundo:",
    contentMidiaTextB: "e outros",

    //BUSCA URBANO
    buscaUrbanoTitle:
      "Aplicativo de Táxi, Uber e mais transportes: compare as melhores tarifas.",
    buscaUrbanoPartida: "MEU LOCAL DE PARTIDA",
    buscaUrbanoDestino: "MEU DESTINO",
    buscaUrbanoBotao: "Comparar",
    buscaUrbanoPlace: "Insira aqui o endereço",

    //FILTRO URBANO
    filtroUrbanoTitle: "Refine sua busca com os filtros",
    filtroUrbanoOrdenar: "ORDENAR POR",
    filtroUrbanoPreco: "Preço",
    filtroUrbanoTempo: "Tempo de chegada",
    filtroUrbanoEmpresas: "Empresas",
    filtroUrbanoBtn: "Aplicar filtros",

    //RESULT URBANO
    resultadoUrbanoBtn: "Ver detalhes",
    resultadoUrbanoDisctancia: "Distância",
    resultadoUrbanoChega: "Chega",
    resultadoUrbanoDuracao: "Duração",

    //BUSCA AEREO
    buscaAereoTitle:
      "Passagens aéreas: compare as melhores tarifas",
    buscaAereoFiltro1: "Ida e Volta",
    buscaAereoFiltro2: "Somente ida",
    buscaAereoDe: "de",
    buscaAereoPara: "para",
    buscaAereoDataIda: "Ida",
    buscaAereoDataVolta: "Volta",
    buscaAereoPassageiro: "Passageiro",
    buscaAereoPassageiroInfo:
      "As políticas e limites de idade para viagem com crianças podem variar, portanto, verifique com a linha aérea antes de reservar.",
    buscaAereoAtualizar: "atualizar",
    buscaAereoBotao: "Comparar",

    //
    //FILTRO AEREO
    filtroAereoTitle: "Refine sua busca com os filtros",
    filtroAereoEscalas: "ESCALAS",
    filtroAereoEmpresas: "Empresas",
    filtroAereoPreco: "Preço",
    filtroAereoIda: "Período ida",
    filtroAereoVolta: "Período volta",
    filtroAereoBtn: "Aplicar filtros",

    //RESULT AEREO
    resultadoAereoBtn: "Ver detalhes",
    resultadoAereoPassageiro: "Passageiro",
    resultadoAereoPartida: "Partida",
    resultadoAereoChegada: "Chegada",

    //BUSCA RODOVIARIO
    buscaRodoviarioTitle: "Passagem de ônibus: compare as melhores tarifas.",
    buscaRodoviarioSaindo: "Saindo de",
    buscaRodoviarioIndo: "Indo para",
    buscaRodoviarioDataIda: "Ida",
    buscaRodoviarioBotao: "Comparar",

    //FILTRO RODOVIARIO
    filtroRodoviarioTitle: "Refine sua busca com os filtros",
    filtroRodoviarioPeriodo: "PERÍODO",
    filtroRodoviarioEmpresas: "Empresas",
    filtroRodoviarioClasse: "Classe",
    filtroRodoviarioBtn: "Aplicar filtros",

    //RESULT RODOVIARIO
    resultadoRodoviarioBtn: "Ver detalhes",
    resultadoRodoviarioDuracao: "Duração da viagem",
    resultadoRodoviarioSaindo: "SAINDO DE",
    resultadoRodoviarioIndo: "INDO PARA",

    //BUSCA LOCAÇÃO
    buscaLocacaoTitle: "Aluguel de carros: compare as melhores tarifas..",
    buscaLocacaoFiltro1: "Mesmo Local",
    buscaLocacaoFiltro2: "Outro Local",
    buscaLocacaoRetirada: "Retirada",
    buscaLocacaoDevolucao: "Devolução",
    buscaLocacaoDataIda: "Data",
    buscaLocacaoHorarioIda: "Horário",
    buscaLocacaoDataVolta: "Data",
    buscaLocacaoHorarioVolta: "Horário",
    buscaLocacaoBotao: "Comparar",

    //FILTRO LOCAÇÃO
    filtroLocacaoTitle: "Refine sua busca com os filtros",
    filtroLocacaoCategoria: "Categoria",
    filtroLocacaoEmpresas: "Empresas",
    filtroLocacaoOpcionais: "Opcionais",
    filtroLocacaoBtn: "Aplicar filtros",

    //RESULT LOCAÇÃO
    resultadoLocacaoBtn: "Selecionar",
    resultadoLocacaoPortas: "Portas",
    resultadoLocacaoPassageiros: "Passageiros",

    //BUSCA CARONA
    buscaCaronaTitle:
      "Carona Compartilhada: veja as caronas e tarifas disponíveis",
    buscaCaronaSaindo: "Saindo de",
    buscaCaronaIndo: "Indo para",
    buscaCaronaDataIda: "Ida",
    buscaCaronaSaida: "Saída após",
    buscaCaronaPlace: "Digite aqui a local",
    buscaCaronaLugares: "Lugares",
    buscaCaronaBotao: "Comparar",

    //FILTRO CARONA
    filtroCaronaTitle: "Refine sua busca com os filtros",
    filtroCaronaPeriodo: "Período",
    filtroCaronaPreco: "Preço",
    filtroCaronaBtn: "Aplicar filtros",

    //RESULT CARONA
    resultadoCaronaBtn: "Ver detalhes",
    resultadoCaronaDuracao: "Duração média",
    resultadoCaronaApartir: "A partir de",

    //CONTATO
    contatoTitle1: "Ei, precisa de ajuda? ",
    contatoTitle2: "Entre em contato com a equipe Vah:",
    contatoLegenda: "Todos os campos precisam ser preenchidos.",
    contatoNovidade: "receber novidades",
    contatoMotivoOp1: "Sugestão",
    contatoMotivoOp2: "Reclamação",
    contatoMotivoOp3: "Elogio",
    contatoMotivoOp4: "Dúvidas",
    contatoMotivo: "Motivo do contato",
    contatoBtn: "Enviar",

    //GERAL
    geralBtnVoltar: "Voltar",
    geralBtnRefazer: "Refazer Busca",
    geralBtnTransicao:
      "Nosso algoritmo está pesquisando as melhores tarifas para você.",
    geralBtnTransicao2:
      "Isso pode levar um tempinho, pois realizamos a maior varredura do mercado!"
  }
};
