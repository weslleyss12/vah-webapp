import locale from "antd/lib/date-picker/locale/pt_BR";

const localeDatepPicker = {
  ...locale,
  lang: {
    ...locale.lang,
    monthFormat: "MMMM",
    monthBeforeYear: true
  }
};

export default localeDatepPicker;
