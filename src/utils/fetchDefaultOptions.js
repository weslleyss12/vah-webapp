export const fetchDefaults = {
  token:
    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImp0aSI6ImRzYTg3ZHNhOTgmRCUmXiZEUyVeJkFEJUFTJiVqaEdnamhhc2Rnamhkc2FqaGcifQ.eyJpc3MiOiJodHRwOlwvXC9hcHB2YWguY29tIiwiYXVkIjoiaHR0cDpcL1wvYXBwdmFoLmNvbSIsImp0aSI6ImRzYTg3ZHNhOTgmRCUmXiZEUyVeJkFEJUFTJiVqaEdnamhhc2Rnamhkc2FqaGciLCJpYXQiOjE1MzQyNjMyNjgsInVpZCI6MjExNzMxLCJwaWQiOjV9.k9swD2c6F5Shr5w-nDmlqlvm2G8Ri9oF_98_4_gjYRQ"
};

export function fetchDefaultOptions(
  method = "GET",
  data = null,
  token = true,
  cache = true
) {
  let basic = {
    method: method,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    }
  };

  if (token && fetchDefaults.token) {
    basic["headers"]["Authorization"] = `Bearer ${fetchDefaults.token}`;
  }

  if (data) {
    basic["body"] = JSON.stringify(data);
  }

  return basic;
}
