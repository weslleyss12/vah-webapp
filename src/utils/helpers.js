import moment from "moment";

export function getLocation(callback) {
  navigator.geolocation.getCurrentPosition(
    ({ coords }) => {
      callback(true, coords);
    },
    error => {
      callback(false);
    }
  );
}

export function getFromPlaceApiResult(result, node) {
  const addressNode = result.address_components.find(component => {
    return component.types[0] === node;
  });

  return addressNode ? addressNode.short_name : null;
}

export function formattingPlaceApiResult(result, type) {
  let terms = [].concat(result.terms);
  let addressNumber = null;
  let continueAt = 1;

  if (result.types.indexOf("establishment") !== -1) {
    if (terms.length === 7) {
      terms.splice(0, 2);
    } else if (terms.length === 6) {
      terms.splice(0, 1);
    }
  } else {
    if (terms.length < 5) {
      terms = [terms[0]].concat(terms);
    }
  }

  if (terms.length > 1 && !isNaN(terms[1].value)) {
    addressNumber = terms[1].value;
    terms.splice(1, 1);
  }
  return {
    id: result.placeId,
    street: terms[0].value + (addressNumber ? `, ${addressNumber}` : ""),
    neighborhood: terms.length > continueAt ? terms[continueAt].value : null,
    city: terms.length > continueAt + 1 ? terms[continueAt + 1].value : null,
    state: terms.length > continueAt + 2 ? terms[continueAt + 2].value : null,
    country: terms.length > continueAt + 3 ? terms[continueAt + 3].value : null,
    type
  };
}

export function formattingCurrentLocation(result) {
  let id = null;
  let street = "";
  let streetNumber = "";
  let neighborhood = "";
  let city = "";
  let state = "";
  let country = "";
  let zipcode = null;

  result.map(part => {
    if (part.types.indexOf("street_number") !== -1) {
      streetNumber = part.short_name;
    }
  });

  result.map(part => {
    if (part.types.indexOf("route") !== -1) {
      street = part.short_name + (streetNumber ? ", " + streetNumber : "");
    } else if (
      (!street && part.types.indexOf("establishment") !== -1) ||
      part.types.indexOf("point_of_interest") !== -1
    ) {
      street = part.short_name;
    } else if (
      part.types.indexOf("sublocality") !== -1 ||
      part.types.indexOf("sublocality_level_1") !== -1
    ) {
      neighborhood = part.short_name;
    } else if (
      part.types.indexOf("locality") !== -1 ||
      part.types.indexOf("administrative_area_level_2") !== -1
    ) {
      city = part.short_name;
    } else if (part.types.indexOf("administrative_area_level_1") !== -1) {
      state = part.short_name;
    } else if (part.types.indexOf("country") !== -1) {
      country = part.short_name;
    } else if (part.types.indexOf("postal_code") !== -1) {
      zipcode = part.short_name;
    }

    return part;
  });

  id = street.replace(/ +/g, "") + city.replace(/ +/g, "") + state;

  return { id, street, neighborhood, city, state, country, zipcode };
}

export const calendarLocale = {
  pt: {
    monthNames: [
      "JANEIRO",
      "FEVEREIRO",
      "MARÇO",
      "ABRIL",
      "MAIO",
      "JUNHO",
      "JULHO",
      "AGOSTO",
      "SETEMBRO",
      "OUTUBRO",
      "NOVEMBRO",
      "DEZEMBRO"
    ],
    monthNamesShort: [
      "JAN",
      "FEV",
      "MAR",
      "ABR",
      "MAI",
      "JUN",
      "JUL",
      "AGO",
      "SET",
      "OUT",
      "NOV",
      "DEZ"
    ],
    dayNames: [
      "DOMINGO",
      "SEGUNDA",
      "TERÇA",
      "QUARTA",
      "QUINTA",
      "SEXTA",
      "SÁBADO"
    ],
    dayNamesShort: ["DOM", "SEG", "TER", "QUA", "QUI", "SEX", "SAB"]
  },
  en: {
    monthNames: [
      "JANUARY",
      "FEBRUARY",
      "MARCH",
      "APRIL",
      "MAY",
      "JUNE",
      "JULY",
      "AUGUST",
      "SEPTEMBER",
      "OCTOBER",
      "NOVEMBER",
      "DECEMBER"
    ],
    monthNamesShort: [
      "JAN",
      "FEB",
      "MAR",
      "APR",
      "MAY",
      "JUN",
      "JUL",
      "AUG",
      "SEP",
      "OCT",
      "NOV",
      "DEC"
    ],
    dayNames: [
      "SUNDAY",
      "MONDAY",
      "TUESDAY",
      "WEDNESDAY",
      "THURSDAY",
      "FRIDAY",
      "SATURDAY"
    ],
    dayNamesShort: ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"]
  }
};

export function minutesInHours(duration) {
  const hours = Math.floor(duration / 60);
  const minutes = duration % 60;

  return minutes === 0 ? `${hours}H` : `${hours}H${minutes}`;
}

export function trim(text) {
  return text
    .toString()
    .replace(/^-+/, "")
    .replace(/-+$/, "");
}

export function slugify(str) {
  str = str.replace(/^\s+|\s+$/g, ""); // trim
  str = str.toLowerCase();

  // remove accents, swap ñ for n, etc
  var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
  var to = "aaaaaeeeeeiiiiooooouuuunc------";

  for (var i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
  }

  str = str
    .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
    .replace(/\s+/g, "-") // collapse whitespace and replace by -
    .replace(/-+/g, "-"); // collapse dashes

  return str;
}

export function timeMoment(date) {
  const hour = parseInt(moment(date, "YYYY-MM-DD hh:mm:ss").format("HH"));

  if (hour >= 0 && hour < 12) {
    return "morning";
  } else if (hour >= 12 && hour < 18) {
    return "afternoon";
  }

  return "night";
}
