import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import contact from '../views/contact/reducer';
import searchByAddress from '../views/searchByAddress/reducer';
import searchByRoad from '../views/searchByRoad/reducer';
import searchByRide from '../views/searchByRide/reducer';
import searchByAir from '../views/searchByAir/reducer';
import searchByRent from '../views/searchByRent/reducer';

export default combineReducers({
  contact,
  searchByAddress,
  searchByRoad,
  searchByRide,
  searchByAir,
  searchByRent,
  router: routerReducer
});
